import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';

import {Category} from '../models/category';
import {Template} from '../models/template';
import {CategoryService} from '../shared/category.service';

@Component({selector: 'my-template', templateUrl: './template.component.html', styleUrls: ['./template.component.css']})
export class TemplateComponent implements OnInit {
    category : Category;
    private sub : any;
    templates : Template[] = [];
    selectedTemplate : Template;

    constructor(private route : ActivatedRoute, private location : Location, private router : Router, private categoryService : CategoryService) {
        // Do stuff
    }

    ngOnInit() {
        //console.log(this.route.params['categoryId']);
        this.sub = this
            .route
            .params
            .subscribe(params => {
                //this.category = params['categoryId']; // (+) converts string 'id' to a number
                this.category = this
                    .categoryService
                    .getCategory(params['categoryId']);
                this.templates = this
                    .categoryService
                    .getTemplatesFromCategory(this.category.id);
                console.log(this.categoryService.getTemplatesFromCategory(this.category.id));
                // In a real app: dispatch action to load the details here.
            });

    }

    onSelect(template : Template) : void {
        this.selectedTemplate = template;
    }

    onNext() : void {
        //console.log('next');
        this
            .router
            .navigate(['/form', this.selectedTemplate.id]);
    }

    ngOnDestroy() {
        this
            .sub
            .unsubscribe();
    }

    goBack() : void {
        this
            .location
            .back();
    }

}
