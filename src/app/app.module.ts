import {NgModule, ApplicationRef} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpModule} from '@angular/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {DragulaModule} from 'ng2-dragula';

import {AppComponent} from './app.component';
import {HomeComponent} from './home/home.component';
import {AboutComponent} from './about/about.component';
import {CategoryComponent} from './category/category.component';
import {TemplateComponent} from './template/template.component';
import {FormComponent} from './form/form.component';
import {TestComponent} from './test/test.component';
import {ALL_DECLARATIONS} from './app.declarations';

import {HighlightDirective} from './directives/highlight.directive';
import {ThemeColorDirective} from './directives/themecolor.directive';

import {CategoryService} from './shared/category.service';
import {FormService} from './shared/form.service';
import {ControllerService} from './shared/controller.service';
import {Utility} from './shared/utility';
import {ApiService} from './shared';
import {routing} from './app.routing';

import {OrderBy} from './shared/orderby';
import {Setting} from './shared/setting';

import {removeNgStyles, createNewHosts} from '@angularclass/hmr';

import {CollapseModule} from 'ngx-bootstrap/collapse';
import {AlertModule} from 'ngx-bootstrap';
import {ModalModule} from 'ngx-bootstrap/modal';
import {TabsModule} from 'ngx-bootstrap/tabs';
import {MomentModule} from 'angular2-moment/moment.module';
import {SelectModule} from 'ng2-select';
import {BsDropdownModule} from 'ngx-bootstrap/dropdown';
import {InlineEditorModule} from 'ng2-inline-editor';
import {TooltipModule} from 'ngx-bootstrap/tooltip';
import {ToasterModule} from 'angular2-toaster';
import {ProgressbarModule} from 'ngx-bootstrap/progressbar';
import {Ng2SimplePageScrollModule} from 'ng2-simple-page-scroll';
import {CollapsibleModule} from 'angular2-collapsible';
import {PerfectScrollbarModule} from 'angular2-perfect-scrollbar';
import {PerfectScrollbarConfigInterface} from 'angular2-perfect-scrollbar';
import {IonRangeSliderModule} from "ng2-ion-range-slider";
import {TreeModule} from 'angular-tree-component';

const PERFECT_SCROLLBAR_CONFIG : PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

@NgModule({
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    routing,
    DragulaModule,
    CollapseModule.forRoot(),
    AlertModule.forRoot(),
    ModalModule.forRoot(),
    TabsModule.forRoot(),
    MomentModule,
    SelectModule,
    BsDropdownModule.forRoot(),
    InlineEditorModule,
    TooltipModule.forRoot(),
    BrowserAnimationsModule,
    ToasterModule,
    ProgressbarModule.forRoot(),
    Ng2SimplePageScrollModule.forRoot(),
    CollapsibleModule,
    PerfectScrollbarModule.forRoot(PERFECT_SCROLLBAR_CONFIG),
    IonRangeSliderModule,
    TreeModule
  ],
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    CategoryComponent,
    TemplateComponent,
    FormComponent,
    TestComponent,
    OrderBy,
    ...ALL_DECLARATIONS,
    HighlightDirective,
    ThemeColorDirective
  ],
  providers: [
    ApiService,
    CategoryService,
    FormService,
    Utility,
    ControllerService,
    Setting

  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(public appRef : ApplicationRef) {}
  hmrOnInit(store) {
    console.log('HMR store', store);
  }
  hmrOnDestroy(store) {
    let cmpLocation = this
      .appRef
      .components
      .map(cmp => cmp.location.nativeElement);
    // recreate elements
    store.disposeOldHosts = createNewHosts(cmpLocation);
    // remove styles
    removeNgStyles();
  }
  hmrAfterDestroy(store) {
    // display new elements
    store.disposeOldHosts();
    delete store.disposeOldHosts;
  }
}
