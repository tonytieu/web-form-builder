import {FormUI} from './form/form-ui/form-ui.component';
import {DBSFormUI} from './form/dbs-form-ui/dbs-form-ui.component';
import {UOBFormUI} from './form/uob-form-ui/uob-form-ui.component';
import {Workspace} from './workspace/workspace.component';
import {FormProperty} from './form/form-property/form-property.component';
import {FormPreview} from './form/form-preview/form-preview.component';
import {FieldUI} from './form/field-ui/field-ui.component';
import {Child} from './form/child.component';
import {Child2} from './form/child2.component';
import {SimpleTinymceComponent} from './simple-tinymce/simple-tinymce.component'
import {TempTinymceComponent} from './simple-tinymce/temp-tinymce/temp-tinymce.component'

export const ALL_DECLARATIONS = [
    FormUI,
    Child,
    Child2,
    FormProperty,
    FormPreview,
    Workspace,
    FieldUI,
    DBSFormUI,
    UOBFormUI,
    SimpleTinymceComponent,
    TempTinymceComponent
];