import {Component} from '@angular/core';

@Component({selector: 'form-child', template: `
  <h3>form child</h3>
  `})
export class Child {
    callFromParent() {
        console.log("form-child: callFromParent");
    }
}