import {
    Component,
    Input,
    Output,
    EventEmitter,
    OnInit,
    ChangeDetectionStrategy
} from '@angular/core';
import {Field} from '../../models/field';
import {ApiService} from '../../shared/api.service';
import {Setting} from '../../shared/setting';

@Component({selector: 'field-ui', templateUrl: './field-ui.component.html', styleUrls: ['./field-ui.component.css'], changeDetection: ChangeDetectionStrategy.OnPush})
export class FieldUI implements OnInit {
    @Input()showField : Field;
    items : Array < any > = [];
    @Input()index : number;
    @Output()onNext : EventEmitter < any > = new EventEmitter();
    isServerValidating : boolean = false;
    @Input()isNumberDisplayed : boolean = true;
    setting : Setting;

    constructor(csetting : Setting, private apiService : ApiService) {
        this.setting = csetting;
    }

    ngOnInit() {
        // console.log(this.field.name); console.log(this.itemIndex);
        // console.log(this.selectedField);
        if (this.showField.link) {
            this.apiService.getData < string[] > (this.showField.link).then(data => this.items = data);
        }
        if (this.showField.tabs) {
            console.log(this.showField.tabs);
            this.toggleTab(0);
        }
    }

    public selected(value : any) : void {
        console.log('Selected value is: ', value);
    }

    onSingleChoiceChange(op, e) {
        // clone the object for immutability this.showField.ans = Object.assign({},
        // this.showField.ans, op);
        this.showField.ans = op.value;
        this.btnNextClicked(e);
    }

    onMultiChoiceChange(op, isChecked) {
        console.log(isChecked);
        this.showField.ans = this.showField.ans
            ? this.showField.ans
            : [];
        if (isChecked) {
            this
                .showField
                .ans
                .push(op.value);
        } else {
            this
                .showField
                .ans
                .splice(this.showField.ans.indexOf(op.value), 1);
        }
    }

    btnNextClicked(e) {

        this
            .onNext
            .emit(e);
    }

    isEmailValidated(item) {
        return item
            .localValidations
            .find(x => x.type == 'email')
            ? true
            : false;
    }

    getFieldLength(item) {
        let lengthValidate = item
            .localValidations
            .find(x => x.type == 'length');
        if (lengthValidate) {
            return lengthValidate.length;
        }
        return 100;
    }

    onTabPressed(e) {
        e.stopPropagation();
        console.log(this.showField.serverValidations);
        if (this.showField.serverValidations && this.showField.serverValidations.length > 0) {
            this.isServerValidating = true;
            setTimeout(() => {
                this.showField.errors = [];
                this
                    .showField
                    .serverValidations
                    .forEach(x => {
                        console.log(this.showField.ans);
                        if (this.showField.ans.length <= 5 || !parseInt(this.showField.ans)) {
                            this
                                .showField
                                .errors
                                .push("'" + x.name + "' is invalid");
                        }
                    });
                this.isServerValidating = false;
            }, 2000);

        }
        this.btnNextClicked(e);
        console.log("ob tab pressed");
    }

    getActiveTabIndex() {
        return this
            .showField
            .tabs
            .findIndex(x => x.active);
    }

    onTabNextClicked() {
        let currentIndex : number = this.getActiveTabIndex();
        this.toggleTab(++currentIndex);
    }

    onTabPreviousCliked() {
        let currentIndex : number = this.getActiveTabIndex();
        this.toggleTab(--currentIndex);
    }

    toggleTab(index) {
        this
            .showField
            .tabs
            .forEach(x => {
                x.disabled = true;
                x.active = false;
            });
        if (this.showField.tabs.length > 0) {
            this.showField.tabs[index].disabled = false;
            this.showField.tabs[index].active = true;
        }
    }
}