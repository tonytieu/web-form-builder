import {Component, EventEmitter, Input, Output, OnInit} from '@angular/core';
import {Field, Tab} from '../../models/field';
import {ApiService} from '../../shared/api.service';
import {FormService} from '../../shared/form.service';
import {Utility} from '../../shared/utility';
import {UUID} from 'angular2-uuid';
// import * as _ from "lodash";

@Component({selector: 'form-ui', templateUrl: './form-ui.component.html', styleUrls: ['./form-ui.component.css']})
export class FormUI implements OnInit {
    @Input()field : Field;
    @Input()selectedField : Field; // used to compare chosen field with "this" field
    @Input()itemIndex : number;
    @Output()onItemRemoveClick = new EventEmitter < number > ();
    @Output()onItemSelectClick = new EventEmitter < Field > ();

    items : Array < any > = [];
    value : any = {};
    isMinimized : boolean = false;
    // public tabs : any[] = [     {         title: 'Dynamic Title 1', content: ` `,
    //         removable: true,         subFields: []     } ];

    constructor(private apiService : ApiService, private formService : FormService, private utility : Utility) {}

    ngOnInit() {
        // console.log(this.field.name); console.log(this.itemIndex);
        // console.log(this.selectedField);
        if (this.field.link) {
            this.apiService.getData < string[] > (this.field.link).then(data => this.items = data);
        }
        if (!this.field.id || this.field.id.startsWith('field')) {
            this.field.id = UUID.UUID();
        }
    }

    getFieldIcon(type : string) {
        return this
            .utility
            .getTypeIcon(type);
    }

    onItemRemoveClicked(e) {
        e.stopPropagation();
        this
            .onItemRemoveClick
            .emit(this.itemIndex);
        console.log("onItemRemoveClicked");
    }

    onCanvasFieldRemoved(index) {
        console.log(index);
        this
            .field
            .subFields
            .splice(index, 1);
    }

    onItemSelectClicked(item, event) {
        event.stopPropagation();
        console.log("onItemSelectClicked", item, event);
        // console.log(this.selectedField); this.selectedField = item; for (var k in
        // item)     this.selectedField[k] = item[k]; this.selectedField = item;

        this
            .onItemSelectClick
            .emit(item);

        this
            .formService
            .setSelectedField(item);
    }

    public selected(value : any) : void {
        console.log('Selected value is: ', value);
    }

    public addNewTab(e) : void {
        console.log(e);
        const newTabIndex = this.field.tabs.length + 1;
        let newTab: Tab = new Tab();
        newTab.title = `Dynamic Title ${newTabIndex}`;
        newTab.active = true;
        this
            .field
            .tabs
            .push(newTab);

    }

    public removeTabHandler(tab : any) : void {
        this
            .field
            .tabs
            .splice(this.field.tabs.indexOf(tab), 1);
        console.log('Remove Tab handler');
    }
}