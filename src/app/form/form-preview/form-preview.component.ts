import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {trigger, state, style, animate, transition} from '@angular/animations';

import {Field, Option, FieldTypes} from '../../models/field';
import {Form} from '../../models/field';
import {ActivatedRoute} from '@angular/router';
import {FormService} from '../../shared/form.service';
import {ModalDirective} from 'ngx-bootstrap/modal';
import {Setting} from '../../shared/setting';

import {ToasterService, ToasterConfig} from 'angular2-toaster';
import {SimplePageScrollService} from 'ng2-simple-page-scroll';

import * as _ from "lodash";

// import * as $ from 'jquery';
declare var $ : any;

@Component({
    selector: 'form-preview',
    templateUrl: './form-preview.component.html',
    styleUrls: ['./form-preview.component.scss'],
    host: {
        '[@routeAnimation]': 'true'
    },
    animations: [trigger('routeAnimation', [
            state('*', style({transform: 'translateX(0)', opacity: 1})),
            transition('void => *', [
                style({opacity: 0}),
                animate(300)
            ])
            // transition('* => void', [animate(100, style({opacity: 1}))])
        ])]
})
export class FormPreview implements OnInit {
    //@Input()canvasFields : Array < Field > = [];
    @Input()form : Form;
    showField : Field;
    index : number = -1;
    chosenOption : Option;
    filteredFields : Array < Field > = [];
    hiddenFilteredFields : Array < Field > = [];
    private sub : any;

    public max : number = 100;
    public dynamic : number = 1;

    public config1 : ToasterConfig = new ToasterConfig({positionClass: 'toast-top-right'});

    isSubmitted : boolean = false;
    isLoggedIn : boolean = false;
    isLogginInProgress : boolean = false;
    @ViewChild('smModal')loginModal : ModalDirective;
    enteredEmail : string;
    enteredPassword : string;

    setting : Setting;

    parameters = [];

    constructor(csetting : Setting, private toasterService : ToasterService, private simplePageScrollService : SimplePageScrollService, private route : ActivatedRoute, private formService : FormService) {
        console.log(this.simplePageScrollService);
        console.log(this.sub);
        this.setting = csetting;
        // console.log($.fn.jquery);
        this.sub = this
            .route
            .params
            .subscribe(params => {
                // this.title = params['templateId']; // (+) converts string 'id' to a number
                // this.template = this     .categoryService     .getTemplate(params['formId']);
                // console.log(this.template); this.title = this.template.name;
                console.log(params['formId']);
                // this.form = new Form(); this.form.createdOn = new Date(); this.form.fields =
                // this.canvasFields;
                this
                    .formService
                    .getForm(params['formId'])
                    .then(f => {
                        for (let field of f.fields) {
                            field.ans = null;
                            field.isShown = true;
                        }
                        this.form = f;
                        this.max = this.form.fields.length;
                        this.dynamic = 0;

                        // get linked ds
                        var idArr = [];
                        this
                            .form
                            .fields
                            .map(x => x.options.filter(y => y.link).map(y => y.link[0].id))
                            .forEach(arr => idArr = idArr.concat(arr));
                        //console.log(idArr);

                        this
                            .form
                            .fields
                            .filter(x => idArr.indexOf(x.id) >= 0)
                            .forEach(x => x.isShown = false);

                        // this.filteredFields = this     .form     .fields     .filter(x => x.isShown);

                        this.updateFilteredFields();

                        setInterval(() => {
                            this.dynamic = _.filter(this.form.fields, (f) => f.ans).length;
                        }, 1000);

                        this.start();
                    });
                //console.log(this.form);
            });
    }

    ngOnInit() {}

    start() {
        this.index = 0;
    }

    next(item : Field, e) {
        e.stopPropagation();
        console.log(item);
        if (this.index >= 0 && item.type == 'radio') {
            // hide linked fields
            let linkedOptionFieldIds = item
                .options
                .filter(x => x.link)
                .map(x => x.link[0].id);
            //console.log(linkedOptionFieldIds);
            this
                .form
                .fields
                .filter(x => linkedOptionFieldIds.indexOf(x.id) >= 0)
                .forEach(x => x.isShown = false);

            for (let op of item.options) {
                if (op.value == item.ans && op.link) {
                    // update filteredFields
                    let updatingField = this
                        .form
                        .fields
                        .find(x => x.id == op.link[0].id);
                    if (updatingField) {
                        updatingField.isShown = true;
                        this.updateFilteredFields();
                    }

                    // this.filteredFields = this     .form     .fields     .filter(x => x.isShown);

                    for (var i = 0; i < this.filteredFields.length; i++) {
                        if (op.link && this.filteredFields[i].id == op.link[0].id) {
                            this.index = --i;
                            break;
                        }
                    }
                }
            }
        }

        this.index++;

        if (this.index >= this.filteredFields.length) {
            this.index = -1;
        }

        if (this.filteredFields[this.index]) {
            //this.filteredFields[this.index].isShown = true;
            setTimeout(() => {
                $("html, body").animate({
                    scrollTop: $("#" + this.filteredFields[this.index].id)
                        .offset()
                        .top - 100
                });
            }, 300);

        }
        // $(window).scrollTop($("#" + this.form.fields[this.index].id).offset().top -
        // 100);

    }

    onCheckboxFieldOptionClicked(op) {
        this.chosenOption = op;
        console.log(op);
    }

    onQuestionClicked(i) {
        this.index = i;
        console.log("onQuestionClicked");
    }

    popToast(type, title, body) {
        this
            .toasterService
            .pop(type, title, body);
    }

    onFormSubmitClicked() {
        this.popToast('success', 'Your form is submitted', '');
        this.isSubmitted = true;
        console.log(this.form);
    }

    updateFilteredFields() {
        this.filteredFields = this
            .form
            .fields
            .filter(x => x.isShown && x.type != FieldTypes.HIDDEN);

        this.max = this.filteredFields.length;

        this.hiddenFilteredFields = this
            .form
            .fields
            .filter(x => x.type == FieldTypes.HIDDEN);
    }

    mockLogin() {
        this.isLogginInProgress = true
        setTimeout(() => {
            this
                .loginModal
                .hide();
            this.isLogginInProgress = false;
            this.isLoggedIn = true;
            this.prefillData();
        }, 2000);
    }

    private prefillData() {
        this
            .filteredFields
            .filter(x => x.origin == 'local_data')
            .forEach(x => x.ans = x.value);
    }

    addParameter() {
        this
            .parameters
            .push({key: "", value: ""});
    }

}