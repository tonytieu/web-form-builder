import {
    Component,
    OnInit,
    ElementRef,
    AfterViewInit,
    ViewChild,
    ViewEncapsulation

} from '@angular/core';
import {trigger, state, style, animate, transition} from '@angular/animations';

import {ActivatedRoute} from '@angular/router';
import {DragulaService} from 'ng2-dragula/ng2-dragula';
import {ToasterService, ToasterConfig} from 'angular2-toaster';
import {ModalDirective} from 'ngx-bootstrap/modal';

import {Form, Controller} from '../models/field';
import {Template} from '../models/template';
import {Field} from '../models/field';
import {Validation} from '../models/field';
import {CategoryService} from '../shared/category.service';
import {FormService} from '../shared/form.service';
import {ControllerService} from '../shared/controller.service';
import {Utility} from '../shared/utility';
import {Setting} from '../shared/setting';

import * as _ from "lodash";
import {UUID} from 'angular2-uuid';

import {Child2} from './child2.component';

declare var $ : any;
declare var renderjson : any;
declare var ace : any;

@Component({
    selector: 'my-form',
    templateUrl: './form.component.html',
    styleUrls: ['./form.component.css'],
    host: {
        '(document:click)': 'onDocumentClicked($event)',
        '[@routeAnimation]': 'true'
    },
    encapsulation: ViewEncapsulation.Emulated,
    animations: [
        trigger('routeAnimation', [
            state('*', style({transform: 'translateX(0)', opacity: 1})),
            transition('void => *', [
                style({opacity: 0}),
                animate(300)
            ])
            // transition('* => void', [animate(100, style({opacity: 1}))])
        ]),
        trigger('fadeInOut', [
            state('in', style({transform: 'scale(1)', opacity: 1})),
            transition('void => *', [
                style({opacity: 0}),
                animate(300)
            ]),
            transition('* => void', [animate(300, style({transform: 'scale(0)', opacity: 1}))])
        ])
    ]
})
export class FormComponent implements OnInit,
AfterViewInit {
    template : Template;
    private sub : any;
    //title : string;
    form : Form = new Form();
    selectedField : Field;

    draggableFields : Array < Field > = [];
    //canvasFields : Array < Field > = [];

    localFields : Array < Field > = [];

    serverFields : Array < Field > = [
        // new Field("field9", "Active Countries", "local_data", ""), new
        // Field("field10", "Branch Names", "server_data", ""), new Field("field11",
        // "Support Numbers", "server_data", "")
    ];
    localValidations : Validation[] = [
        new Validation("va1", "Length", "length", ""),
        new Validation("va2", "Email", "email", ""),
        new Validation("va3", "Number Range", "number_range", ""),
        new Validation("va4", "Alphabetic", "alphabetic", ""),
        new Validation("va5", "AlphaNumeric", "alphanumeric", ""),
        new Validation("va6", "Numeric", "numeric", ""),
        new Validation("va7", "AlphaNumeric+Symbols", "alphanumeric+symbols", ""),
        new Validation("va8", "Mobile/Office/Home SG", "telsg", "")
    ];
    serverValidations : Validation[] = [];

    @ViewChild('dropzone')dropZoneEle : ElementRef;
    // @ViewChild('btnChooseController')btnChooseController : ElementRef;
    @ViewChild('controllerModal')controllerModal : ModalDirective;
    @ViewChild(Child2)private formChild2 : Child2;
    public config1 : ToasterConfig = new ToasterConfig({positionClass: 'toast-top-right'});
    //private toasterService : ToasterService;
    controllerList : Array < Controller >;
    utility : Utility;
    setting : Setting;
    isAttributeSectionMinimized : boolean = false;
    isFormSetting : boolean = false;
    isDataElementSectionCollapsed : boolean = false;
    isFavoriteSectionCollapsed : boolean = false;
    isComponentSectionCollapsed : boolean = false;
    isTemplateSectionCollapsed : boolean = false;

    constructor(csetting : Setting, uti : Utility, private controllerService : ControllerService, private toasterService : ToasterService, private route : ActivatedRoute, private dragulaService : DragulaService, private categoryService : CategoryService, private formService : FormService) {
        console.log(this.categoryService);
        this.utility = uti;
        this.setting = csetting;
        let $this = this;
        //this.toasterService = toasterService;
        this
            .controllerService
            .getControllerList()
            .then(list => this.controllerList = list)
            .then(() => {
                for (let con of this.controllerList) {
                    this
                        .formService
                        .getLocalDataFromServer(con.link)
                        .then(fields => {
                            _
                                .forEach(fields, function (value) {
                                    value.id = UUID.UUID();
                                    value.localValidations = [];
                                    value.serverValidations = [];
                                    value.name = `\${${value.name}}`;
                                    value.shouldEdit = value.isEditable;
                                    //console.log(value);
                                });

                            con.fields = fields;
                        });
                }
            });
        this
            .formService
            ._itemSelect
            .subscribe(selected => this.selectedField = selected);
        this.sub = this
            .route
            .params
            .subscribe(params => {
                this
                    .formService
                    .getForm(params['formId'])
                    .then(f => {
                        this.form = f;
                        console.log(this.form);
                    })
                    .then(() => {
                        console.log(this.form);
                        if (this.form.controller) {
                            this
                                .formService
                                .getLocalDataFromServer(this.form.controller.link)
                                .then(fields => {
                                    _
                                        .forEach(fields, function (value) {
                                            value.id = UUID.UUID();
                                            value.localValidations = [];
                                            value.serverValidations = [];
                                            value.name = `\${${value.name}}`;
                                            value.shouldEdit = value.isEditable;
                                        })
                                    $this.localFields = fields;

                                });
                        }

                        if (!this.form.controller) {
                            // let event = new MouseEvent('click', {bubbles: true}); this
                            // .btnChooseController     .nativeElement     .dispatchEvent(event);
                            $this
                                .controllerModal
                                .show();
                        }
                    });
            });

        this
            .formService
            .getDraggableFields()
            .then(fields => {
                _
                    .forEach(fields, function (value) {
                        console.log(value);
                    });
                this.draggableFields = fields;
            });
        this
            .formService
            .getServerFields()
            .then(fields => {
                _
                    .forEach(fields, function (value) {
                        value.id = UUID.UUID();
                        value.localValidations = [];
                        value.serverValidations = [];
                    });
                this.serverFields = fields;
            });

        this
            .formService
            .getServerValidations()
            .then(vals => {
                this.serverValidations = vals;
                console.log(vals);
            });
    }

    ngOnInit() {
        const bag : any = this
            .dragulaService
            .find('another-bag');
        if (bag !== undefined) 
            this.dragulaService.destroy('another-bag');
        
        this
            .dragulaService
            .setOptions('another-bag', {
                //removeOnSpill: true,
                revertOnSpill: true,
                copy: function (el, source) {
                    // To copy only elements in left container, the right container can still be
                    // sorted
                    console.log(el);
                    console.log(source);
                    //return source.id === 'no-drop';
                    return source
                        .className
                        .indexOf('no-drop') > -1;
                },
                copySortSource: false,
                accepts: function (el, target, source, sibling) {
                    // To avoid draggin from right to left container
                    console.log({el, target, source, sibling});
                    return target.id !== 'no-drop';
                }
                // accepts: function (el, target, source, sibling) {     // var fn_debug = true;
                // var acceptAll = false; if (this._debug || fn_debug) {     //
                // console.log("accepts() start el, target, source, sibling");     if
                // (target.classList.contains('no-drop')) {         console.log({el, target,
                // source, sibling});         return false;     }     // if (sibling == null) {
                // return (target.children.length == 0); } var name :     // string =
                // el.innerText;     return true; }
            });
        this
            .dragulaService
            .drag
            .subscribe((value) => {

                console.log(`drag: ${value[0]}`);
                console.log(this.dropZoneEle);
                this.dropZoneEle.nativeElement.style.border = '1px solid lightgrey';
            });
        this
            .dragulaService
            .drop
            .subscribe((value) => {
                console.log(`drop: ${value[0]}`);
                this.dropZoneEle.nativeElement.style.border = '1px solid #FAFAFA';
            });
        this
            .dragulaService
            .over
            .subscribe((value) => {
                console.log(`over: ${value[0]}`);

            });
        this
            .dragulaService
            .out
            .subscribe((value) => {
                console.log(`out: ${value[0]}`);
                //this.dropZoneEle.nativeElement.style.border = 'none';

            })
        this
            .dragulaService
            .dropModel
            .subscribe((value) => {
                console.log(`dropModel: ${value[0]}`);
                console.log(value);
                this.onDropModel(value.slice(1));
            });
        this
            .dragulaService
            .removeModel
            .subscribe((value) => {
                console.log(`removeModel: ${value[0]}`);
                this.onRemoveModel(value.slice(1));
            });
        //this.selectedField = this.formService.selectedField;
    }

    ngAfterViewInit() {
        console.log(this.dropZoneEle.nativeElement);
        //console.debug(this.divs);
        this
            .formChild2
            .callFromParent2();
        // this     .jsoneditor     .setMode('tree');

    }

    ngOnDestroy() {
        this
            .sub
            .unsubscribe();
    }

    private onDropModel(args) {
        //        let [el] = args;
        console.log(args);
        console.log("on drop model");
    }

    private onRemoveModel(args) {
        // let [el,     source] = args; do something else
        console.log(args);
    }

    saveEditable(value) {
        console.log(value);
    }

    onCanvasFieldClicked(item, event) {
        event.stopPropagation();
        //console.log(item);
        this
            .formService
            .setSelectedField(item);
        //this.selectedField = item;
    }

    onCanvasFieldRemoved(index) {
        //console.log(index);
        this
            .form
            .fields
            .splice(index, 1);
        this.selectedField = null;
    }

    onDocumentClicked(event) {
        console.log(event);
        //this.selectedField = undefined;
    }

    onCanvasSurfaceClicked(event) {
        event.stopPropagation();
        this.selectedField = undefined;
        this.isFormSetting = false;

    }

    onFormSettingClicked(event) {
        event.stopPropagation();
        this.isFormSetting = true;
        this.selectedField = undefined;
        setTimeout(() => {
            var editor = ace.edit("editor");
            editor.setTheme("ace/theme/monokai");
            editor
                .getSession()
                .setMode("ace/mode/javascript");
        }, 1000);

    }

    onSubfieldSelectClicked(event) {
        //console.log(event);
        this.selectedField = event;
    }

    onLocalValidationChecked(item, checkValue) {
        //console.log(item); console.log(checkValue);
        if (checkValue) {
            this
                .selectedField
                .localValidations
                .push(_.clone(item));
        } else {
            _.remove(this.selectedField.localValidations, field => field.id == item.id);
        }
    }

    isLocalValidationSelected(validation : Validation) {
        if (_.find(this.selectedField.localValidations, f => f.id == validation.id)) 
            return true;
        else 
            return false;
        }
    onLengthChanged(field : Field, targetValidation : Validation, val) {
        let fieldValidation = field
            .localValidations
            .find(x => x.type == targetValidation.type);
        if (fieldValidation) {
            fieldValidation.length = val;
        }
    }
    getValidationLength(field : Field, targetValidation : Validation) {
        let fieldValidation = field
            .localValidations
            .find(x => x.type == targetValidation.type);
        if (fieldValidation) {
            return fieldValidation.length;
        }
    }
    onServerValidationChecked(item, checkValue) {
        //console.log(item); console.log(checkValue);
        if (checkValue) {
            this
                .selectedField
                .serverValidations
                .push(item);
        } else {
            _.remove(this.selectedField.serverValidations, field => field.id == item.id);
        }
    }

    isServerValidationSelected(validation : Validation) {
        if (_.find(this.selectedField.serverValidations, f => f.id == validation.id)) 
            return true;
        else 
            return false;
        }
    
    processForm() {
        //console.log("processing form"); this.form.fields = this.canvasFields;
        renderjson.set_show_to_level(4);
        $("#jsonModal").html(renderjson(this.form));

    }

    popToast(type, title, body) {
        this
            .toasterService
            .pop(type, title, body);
    }

    onControllerChooseClicked(controller, e) {
        e.stopPropagation();
        this.form.controller = controller;
        this
            .controllerModal
            .hide();
    }

    getNodesFromFrom() {
        let nodes : any = [
            {
                id: 1,
                name: 'Root',
                isExpanded: true,
                children: []
            }
        ];

        this
            .form
            .fields
            .forEach((item, index) => {
                let obj : any = {};
                obj.id = index;
                obj.name = item.type;
                obj.isExpanded = true;
                obj.children = [];
                obj.fieldId = item.id;

                if (item.subFields) {
                    item
                        .subFields
                        .forEach((f, i) => {
                            let sub : any = {};
                            sub.id = i;
                            sub.name = f.type;
                            sub.fieldId = f.id;
                            obj
                                .children
                                .push(sub);
                        });
                }
                nodes[0]
                    .children
                    .push(obj);

            });

        return nodes;
    }

    onTreeNodeActivated(e) {
        console.log(e);
        if (e.node.data.fieldId) {
            $(".canvas").animate({
                scrollTop: $("#" + e.node.data.fieldId)
                    .offset()
                    .top - 100
            });
        }
    }

}