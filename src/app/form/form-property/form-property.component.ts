import {Component, Input, OnInit} from '@angular/core';
import {Field, Option} from '../../models/field';
import {trigger, state, style, animate, transition} from '@angular/animations';

@Component({
    selector: 'form-property',
    templateUrl: './form-property.component.html',
    styleUrls: ['./form-property.component.css'],
    animations: [trigger('flyInOut', [
            state('in', style({transform: 'translateX(0)'})),
            transition('void => *', [
                style({transform: 'translateX(-100%)'}),
                animate(200)
            ]),
            transition('* => void', [animate(200, style({transform: 'translateX(100%)'}))])
        ])]
})
export class FormProperty implements OnInit {
    @Input()selectedField : Field;
    @Input()canvasFields : Array < Field > = [];

    constructor() {}

    ngOnInit() {}

    onAddFieldOptionClicked() {
        this
            .selectedField
            .options
            .push(new Option(null));
    }
    onFieldOptionRemoveClicked(index) {
        this
            .selectedField
            .options
            .splice(index, 1);
    }

    getLinkingQuestions() : Array < any > {
        // get linked ids
        var idArr = [];
        this
            .canvasFields
            .map(x => x.options.filter(y => y.link).map(y => y.link[0].id))
            .forEach(arr => idArr = idArr.concat(arr));

        // console.log(idArr) build select items
        let questions: Array < any > = [];
        this
            .canvasFields
            .filter(x => x.id != this.selectedField.id && idArr.indexOf(x.id) < 0)
            .forEach(x => questions.push({id: x.id, text: x.name}));
        // for (let f of this.canvasFields) {     if (f.id != this.selectedField.id) {
        // questions.push({id: f.id, text: f.name});     } }
        return questions;
    }

    onFieldOptionLinkSelected(op, value) {
        console.log(value, op);
        op.link = value.id;
    }

}