import {Component} from '@angular/core';

@Component({selector: 'form-child2', template: `
  <h3></h3>
  `})
export class Child2 {
    callFromParent2() {
        console.log("form-child2: callFromParent");
    }
}