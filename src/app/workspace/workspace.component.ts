import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {trigger, state, style, animate, transition} from '@angular/animations';

import {Form} from '../models/field';
import {Controller} from '../models/field';
import {FormService} from '../shared/form.service';
import {Setting} from '../shared/setting';
import {ControllerService} from '../shared/controller.service';
//import {UUID} from 'angular2-uuid'; import * as _ from "lodash";

@Component({
    selector: 'workspace',
    templateUrl: './workspace.component.html',
    styleUrls: ['./workspace.component.scss'],
    animations: [
        trigger('heroState', [
            state('inactive', style({backgroundColor: '#eee', transform: 'translateX(0) scale(1)'})),
            state('active', style({backgroundColor: '#cfd8dc', transform: 'translateX(0) scale(1.1)'})),
            transition('inactive => active', animate('100ms ease-in')),
            transition('active => inactive', animate('100ms ease-out')),
            transition('void => inactive', [
                style({transform: 'translateX(-100%) scale(1)'}),
                animate(100)
            ]),
            transition('inactive => void', [animate(300, style({transform: 'translateX(0%) scale(0)'}))]),
            transition('void => active', [
                style({transform: 'translateX(0) scale(0)'}),
                animate(200)
            ]),
            transition('active => void', [animate(200, style({transform: 'translateX(0) scale(0)'}))])
        ]),
        trigger('fadeInOut', [
            state('in', style({opacity: 1})),
            transition('void => *', [
                style({opacity: 0}),
                animate(300)
            ]),
            transition('* => void', [animate(300, style({opacity: 1}))])
        ])
    ]
})
export class Workspace implements OnInit {

    formList : Array < Form >;
    controllerList : Array < Controller >;
    setting : Setting;

    constructor(csetting : Setting, private router : Router, private formService : FormService, private controllerService : ControllerService) {
        this
            .formService
            .getFormList()
            .then(list => this.formList = list);
        this
            .controllerService
            .getControllerList()
            .then(list => this.controllerList = list);
        this.setting = csetting;
        // console.log(localStorage.getItem('formList')); if
        // (!localStorage.getItem('formList')) {     localStorage.setItem('formList',
        // '[]'); } else {}
    }

    ngOnInit() {}

    newForm() {
        this
            .formService
            .createNewForm();
    }

    deleteForm(index) {
        this
            .formService
            .deleteForm(index);
    }

    onControllerChosenChange(form : Form, controller : Controller, e) {
        e.stopPropagation();

        form.controller = controller;
    }

    onFormDoubleClicked(e) {
        e.stopPropagation();
        console.log("double click");
    }

    editForm(form) {
        this
            .router
            .navigate(['/form', form.id]);
    }

    onColorThemeChosen(colorname) {
        console.log(colorname);
        this.setting.colorTheme = colorname;
    }
}