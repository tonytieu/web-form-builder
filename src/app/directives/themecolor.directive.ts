import {Directive, ElementRef, Input, Renderer2} from '@angular/core';
// import $ from 'jquery'; declare var $ : any;
import * as $ from 'jquery/dist/jquery.js';
@Directive({selector: '[myThemeColor]'})
export class ThemeColorDirective {

    @Input('myThemeColor')highlightColor : string;
    @Input()setBackground : boolean = false;
    colorNames : Array < string > = ['color-green', 'color-red', 'color-blue'];
    bgColorNames : Array < string > = ['bg-green', 'bg-red', 'bg-blue'];

    constructor(private el : ElementRef, private renderder : Renderer2) {}

    ngOnInit() {
        //console.log(this.highlightColor);

        this.changeThemeColor(this.highlightColor);

        //this.el.nativeElement.style.backgroundColor = this.highlightColor;
    }
    ngAfterViewInit() {
        this.changeThemeColor(this.highlightColor);
    }

    ngOnChanges(changes) {
        // console.log(changes);
        this.changeThemeColor(changes.highlightColor.currentValue);
    }

    changeThemeColor(color) {
        let colorClassName;
        switch (color) {
            case 'green':
                colorClassName = 'color-green';
                if (this.setBackground) 
                    colorClassName = 'bg-green';
                break;
            case 'red':
                colorClassName = 'color-red';
                if (this.setBackground) 
                    colorClassName = 'bg-red';
                break;
            case 'blue':
                colorClassName = 'color-blue';
                if (this.setBackground) 
                    colorClassName = 'bg-blue';
                break;
        }
        this.removeAllColorClass(this.el.nativeElement);
        this
            .renderder
            .addClass(this.el.nativeElement, colorClassName);

        // change third-party styles
        $("a.c-inline-editor").addClass(colorClassName);
        //$("button.c-inline-editor").addClass(colorClassName);
    }

    private removeAllColorClass(ele) {
        for (let colorName of this.colorNames) {
            this
                .renderder
                .removeClass(ele, colorName);
            $("a.c-inline-editor").removeClass(colorName);
            //$("button.c-inline-editor").removeClass(colorName);
        }
        for (let colorName of this.bgColorNames) {
            this
                .renderder
                .removeClass(ele, colorName);
            $("a.c-inline-editor").removeClass(colorName);
            //$("button.c-inline-editor").removeClass(colorName);
        }
    }
}