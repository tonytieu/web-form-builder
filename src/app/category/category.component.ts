import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

import {Category} from '../models/category';
import {CategoryService} from '../shared/category.service';

@Component({selector: 'my-category', templateUrl: './category.component.html', styleUrls: ['./category.component.css']})
export class CategoryComponent implements OnInit {
    categories : Array < Category >;
    selectedCategory : Category;

    constructor(private router : Router, private categoryService : CategoryService) {}

    ngOnInit() {
        this
            .categoryService
            .getCategories()
            .then(cats => this.categories = cats);
    }

    onSelect(category : Category) : void {
        this.selectedCategory = category;
    }

    onNext() : void {
        //console.log('next');
        this
            .router
            .navigate(['/template', this.selectedCategory.id]);
    }

}
