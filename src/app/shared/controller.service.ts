import {Injectable} from '@angular/core';
//import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';
// import * as _ from "lodash";

import {Controller} from '../models/field';

@Injectable()
export class ControllerService {
    private controllers : Array < Controller > = [
        new Controller("DB Loggedin User", "controller1"),
        new Controller("DB Account Info", "controller2")
    ];

    getControllerList() : Promise < Controller[] > {
        return Promise.resolve(this.controllers);
    }
}