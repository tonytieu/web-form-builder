import {Injectable} from '@angular/core';
//import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';
import * as _ from "lodash";

import {Category} from '../models/category';
import {Template} from '../models/template';

@Injectable()
export class CategoryService {

    //private heroesUrl = 'api/heroes';  // URL to web api
    private categories : Array < Category > = [
        new Category("cat1", "profiles"),
        new Category("cat2", "details")
    ];

    private templates : Array < Template > = [
        new Template("temp1", "Template 1", "cat1"),
        new Template("temp2", "Template 2", "cat1"),
        new Template("temp3", "Template 3", "cat2"),
        new Template("temp4", "Template 4", "cat2")
    ];
    // private headers = new Headers({'Content-Type': 'application/json'})
    // constructor(private http: Http) { }

    getCategories() : Promise < Category[] > {
        return Promise.resolve(this.categories);
        // return this.http.get(this.heroesUrl)     .toPromise()     .then(response =>
        // response.json().data as Hero[])     .catch(this.handleError);
    }

    getCategory(categoryId : string) : Category {
        return _.find(this.categories, {id: categoryId});
    }

    getTemplates() : Promise < Template[] > {
        return Promise.resolve(this.templates);
        // return this.http.get(this.heroesUrl)     .toPromise()     .then(response =>
        // response.json().data as Hero[])     .catch(this.handleError);
    }

    getTemplate(templateId : string) : Template {
        return _.find(this.templates, {id: templateId});
    }

    getTemplatesFromCategory(categoryId : string) {
        console.log(_.filter(this.templates, {'categoryId': categoryId}));
        //return _.filter(this.templates, {'categoryId': categoryId});
        return this
            .templates
            .filter(template => template.categoryId == categoryId);
    }
    // private handleError(error: any): Promise<any> {     console.error('An error
    // occurred', error); // for demo purposes only     return
    // Promise.reject(error.message || error); }
}