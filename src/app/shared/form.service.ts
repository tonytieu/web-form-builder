import {Injectable} from '@angular/core';
import {Headers, Http} from '@angular/http';
import {EventEmitter} from '@angular/core';
import 'rxjs/add/operator/toPromise';
import * as _ from "lodash";
import mockedData from './mocked-data-2';

import {Form, Field, Validation, FieldTypes} from '../models/field';
import {Utility} from './utility';

@Injectable()
export class FormService {
    utility : Utility = new Utility();

    //private endpointPrefix = 'http://localhost:63849/api/';
    private endpointPrefix = 'http://demos.thesoftwarepractice.com/web-form-builder/api/';
    private draggableFields : Array < Field > = [
        new Field("field1", "Header", FieldTypes.LABEL, this.utility.getTypeIcon(FieldTypes.LABEL), 'e.g. Welcome to our house!'),
        new Field("field2", "Short Text", FieldTypes.SHORT_TEXT, this.utility.getTypeIcon(FieldTypes.SHORT_TEXT), 'e.g. What is your mother\'s name?'),
        new Field("field3", "Long Text", FieldTypes.LONG_TEXT, this.utility.getTypeIcon(FieldTypes.LONG_TEXT), 'e.g. Tell us a little about your family'),
        new Field("field4", "Number", FieldTypes.NUMBER, this.utility.getTypeIcon(FieldTypes.NUMBER), 'e.g. How many siblings do you have?'),
        new Field("field5", "MultiChoice", FieldTypes.MULTIPLE_CHOICE, this.utility.getTypeIcon(FieldTypes.MULTIPLE_CHOICE), 'e.g. Which of these dishes do you like?'),
        new Field("field6", "SingChoice", FieldTypes.SINGLE_CHOICE, this.utility.getTypeIcon(FieldTypes.SINGLE_CHOICE), 'e.g. What is your gender?'),
        new Field("field7", "Date", FieldTypes.DATE, this.utility.getTypeIcon(FieldTypes.DATE), 'e.g. When is your birthday?'),
        new Field("field8", "Group", FieldTypes.QUESTION_GROUP, this.utility.getTypeIcon(FieldTypes.QUESTION_GROUP), 'e.g. How would you rate the following services?'),
        new Field("field9", "Dropdown", FieldTypes.DROPDOWN, this.utility.getTypeIcon(FieldTypes.DROPDOWN), 'e.g. What is your title?'),
        new Field("field10", "Hidden", FieldTypes.HIDDEN, this.utility.getTypeIcon(FieldTypes.HIDDEN), 'e.g. What is your account id?'),
        new Field("field11", "Slider", FieldTypes.SLIDER, this.utility.getTypeIcon(FieldTypes.SLIDER), 'e.g. How much do you want to borrow?'),
        new Field("field12", "Tab", FieldTypes.TAB, this.utility.getTypeIcon(FieldTypes.TAB), 'e.g. Tabs?'),
        new Field("field13", "Rich Text", FieldTypes.RICH_TEXT, this.utility.getTypeIcon(FieldTypes.RICH_TEXT), 'e.g. Terms and conditions'),
        new Field("field13", "Submit Button", FieldTypes.SUBMIT_BUTTON, this.utility.getTypeIcon(FieldTypes.SUBMIT_BUTTON), 'e.g. Submit')
    ];
    private headers = new Headers({'Content-Type': 'application/json'})
    public _itemSelect : EventEmitter < Field >;
    public selectedField : Field;
    private formList : Array < Form > = [];

    constructor(private http : Http) {
        console.log(this.headers);
        this._itemSelect = new EventEmitter();
        //this.utility = uti; let mockedJson = ``;

        if (this.mockedJson) {
            let mockedForm : Form = this.mockedJson as Form;
            //mockedForm.controller = null;
            this
                .formList
                .push(mockedForm);
        }
    }

    setSelectedField(field) {
        this.selectedField = field;
        this
            ._itemSelect
            .emit(field);
    }

    getFormList() : Promise < Form[] > {

        return Promise.resolve(this.formList);
        // return this.http.get(this.heroesUrl)     .toPromise()     .then(response =>
        // response.json().data as Hero[])     .catch(this.handleError);
    }

    createNewForm() : Promise < Form > {
        let newForm = new Form("New Form");
        this
            .formList
            .push(newForm);
        return Promise.resolve(newForm);
    }

    deleteForm(index) : Promise < boolean > {
        let result = true;
        this
            .formList
            .splice(index, 1);
        return Promise.resolve(result);
    }

    getForm(id : string) : Promise < Form > {
        console.log(_.find(this.formList, f => f.id == id));
        return Promise.resolve(_.find(this.formList, f => f.id == id));
    }

    getDraggableFields() : Promise < Field[] > {
        return Promise.resolve(this.draggableFields);
        // return this.http.get(this.heroesUrl)     .toPromise()     .then(response =>
        // response.json().data as Hero[])     .catch(this.handleError);
    }

    getLocalDataFromServer(controllerName) : Promise < Field[] > {
        return this
            .http
            .get(this.endpointPrefix + '' + controllerName)
            .toPromise()
            .then(response => response.json()as Field[])
            .then(fields => {
                fields.forEach(x => {
                    x.options = [];
                    x.isShown = true;
                });
                return fields;
            })
            .catch(this.handleError);
    }

    getServerFields() : Promise < Field[] > {
        return this
            .http
            .get(this.endpointPrefix + 'shared')
            .toPromise()
            .then(response => response.json()as Field[])
            .then(fields => {
                fields.forEach(x => {
                    x.options = [];
                    x.isShown = true;
                });
                return fields;
            })
            .catch(this.handleError);
    }

    getServerValidations() : Promise < Validation[] > {
        return this
            .http
            .get(this.endpointPrefix + 'Validation')
            .toPromise()
            .then(response => response.json()as Validation[])
            .catch(this.handleError);
    }

    private handleError(error : any) : Promise < any > {
        console.error('An error occurred ', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }

    mockedJson : any = mockedData;
}