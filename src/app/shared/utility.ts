import {Injectable} from '@angular/core';
import * as _ from "lodash";
import {FieldTypes} from "../models/field";

@Injectable()
export class Utility {
    getDataObjectForLodashTemplate(variable, value) : Object {
        let obj = {};
        obj[variable] = value;
        return obj;
    }

    getStringFromLodashTemplate(link, data) : string {
        var compiled = _.template(decodeURIComponent(link));
        return compiled(data);
    }

    getTypeIcon(type : string) : string {
        switch(type) {
            case FieldTypes.LABEL:
                return `<span class="glyphicon glyphicon-font m-r-sm"></span>`;
            case FieldTypes.SHORT_TEXT:
                return `<span class ="glyphicon glyphicon-text-height m-r-sm"></span>`;
            case FieldTypes.LONG_TEXT:
                return `<span class ="fa fa-square-o m-r-sm"></span>`;
            case FieldTypes.MULTIPLE_CHOICE:
                return `<span class ="glyphicon glyphicon-list-alt m-r-sm"></span>`;
            case FieldTypes.SINGLE_CHOICE:
                return `<span class ="glyphicon glyphicon-collapse-down m-r-sm"></span>`;
            case FieldTypes.NUMBER:
                return `<span class="glyphicon glyphicon-glass m-r-sm"></span>`;
            case FieldTypes.QUESTION_GROUP:
                return `<span class="glyphicon glyphicon glyphicon-th-list m-r-sm"></span>`;
            case FieldTypes.DATE:
                return `<span class="glyphicon glyphicon-calendar m-r-sm"></span>`;
            case FieldTypes.DROPDOWN:
                return `<i class="fa fa-caret-square-o-down m-r-sm" aria-hidden="true"></i>`;
            case FieldTypes.IMAGE:
                return `<i class="fa fa-file-image-o m-r-sm" aria-hidden="true"></i>`;
            case FieldTypes.HIDDEN:
                return `<i class="fa fa-header m-r-sm" aria-hidden="true"></i>`;
            case FieldTypes.SLIDER:
                return `<i class="fa fa-sliders m-r-sm" aria-hidden="true"></i>`;
            case FieldTypes.TAB:
                return `<i class="fa fa-columns m-r-sm" aria-hidden="true"></i>`;
            case FieldTypes.RICH_TEXT:
                return `<i class="fa fa-object-group m-r-sm" aria-hidden="true"></i>`;
            case FieldTypes.SUBMIT_BUTTON:
                return `<i class="fa fa-hdd-o m-r-sm" aria-hidden="true"></i>`;
        }
    }
}