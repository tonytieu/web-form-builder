import {Injectable} from '@angular/core';
import {Http} from '@angular/http';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class ApiService {

  constructor(private http : Http) {}

  getData < T > (link) : Promise < T > {
    return this
      .http
      .get(link)
      .toPromise()
      .then(response => response.json()as T)
      .catch(this.handleError);
  }
  private handleError(error : any) : Promise < any > {
    console.error('An error occurred ', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
