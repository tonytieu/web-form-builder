import {
    Component,
    OnDestroy,
    AfterViewInit,
    EventEmitter,
    Input,
    Output
} from '@angular/core';

declare var tinymce : any;

@Component({selector: 'temp-tiny', template: `<textarea id="temp-tiny"></textarea>`})
export class TempTinymceComponent implements AfterViewInit,
OnDestroy {
    @Input()elementId : String;
    @Output()contentChange = new EventEmitter < any > ();
    @Input()content : string;
    editor;

    ngAfterViewInit() {
        console.log("temp-tinymce");
        tinymce.init({
            selector: '#temp-tiny',
            plugins: [
                //'link', 'paste', 'table'
            ],
            skin_url: 'plugins/tinymce/skins/lightgray',
            setup: editor => {
                this.editor = editor;
                editor.on('click', () => {
                    const content = editor.getContent();
                    //console.log(content);
                    this
                        .contentChange
                        .emit(content);
                });
                editor.on('KeyUp', () => {
                    const content = editor.getContent();

                    this
                        .contentChange
                        .emit(content);
                });
            },
            init_instance_callback: editor => {
                this.editor = editor;

                editor.setContent(this.content);
            }
        });
    }

    ngOnDestroy() {
        tinymce.remove(this.editor);
    }
}