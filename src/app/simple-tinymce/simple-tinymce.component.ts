import {
    Component,
    OnDestroy,
    AfterViewInit,
    EventEmitter,
    Input,
    Output,
    TemplateRef,
    ViewChild
} from '@angular/core';
import {BsModalService} from 'ngx-bootstrap/modal';
import {BsModalRef} from 'ngx-bootstrap/modal/modal-options.class';

declare var tinymce : any;

@Component({selector: 'simple-tiny', templateUrl: './simple-tinymce.component.html'})
export class SimpleTinymceComponent implements AfterViewInit,
OnDestroy {
    @Input()elementId : String;
    contentValue : string;
    @Output()contentChange = new EventEmitter();

    editor;

    @Input()get content() {

        return this.contentValue;
    }
    set content(val) {
        this.contentValue = val;
        this
            .contentChange
            .emit(this.contentValue);
        //console.log(this.contentValue);
    }

    public modalRef : BsModalRef;
    @ViewChild('template')
    template : TemplateRef < any >;

    constructor(private modalService : BsModalService) {}

    public openModal(template : TemplateRef < any >) {
        this.modalRef = this
            .modalService
            .show(template, Object.assign({}, null, {class: 'modal-lg'}));
    }

    ngAfterViewInit() {
        tinymce
            .PluginManager
            .add('toolbarplugin', (editor) => {
                editor.addButton('toolbarplugin', {
                    title: 'Expand',
                    icon: 'plus',
                    onclick: () => {
                        this.openModal(this.template);
                    }
                });
            });
        tinymce.init({
            selector: '#' + this.elementId,
            menubar: false,
            statusbar: false,
            toolbar1: 'undo redo | bold italic underline | alignleft aligncenter alignright | bullist n' +
                    'umlist | toolbarplugin',
            plugins: [//'link', 'paste', 'table'
                'toolbarplugin'],
            skin_url: 'plugins/tinymce/skins/lightgray',
            setup: editor => {
                this.editor = editor;
                editor.on('click', () => {
                    const content = editor.getContent();
                    //console.log(content);
                    this
                        .contentChange
                        .emit(content);
                });
                editor.on('KeyUp', () => {
                    const content = editor.getContent();

                    this
                        .contentChange
                        .emit(content);
                });

            },
            init_instance_callback: editor => {
                this.editor = editor;

                editor.setContent(this.contentValue);
            }
        });

    }

    ngOnDestroy() {
        tinymce.remove(this.editor);
    }

}