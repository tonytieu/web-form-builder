import {RouterModule, Routes} from '@angular/router';

//import {HomeComponent} from './home/home.component';
import {AboutComponent} from './about/about.component';
import {CategoryComponent} from './category/category.component';
import {TemplateComponent} from './template/template.component';
import {FormComponent} from './form/form.component';
import {Workspace} from './workspace/workspace.component';
import {FormPreview} from './form/form-preview/form-preview.component';
import {TestComponent} from './test/test.component';

const routes : Routes = [
  {
    path: '',
    redirectTo: '/workspace',
    pathMatch: 'full'
  }, {
    path: 'category',
    component: CategoryComponent
  }, {
    path: 'about',
    component: AboutComponent
  }, {
    path: 'template/:categoryId',
    component: TemplateComponent
  }, {
    path: 'workspace',
    component: Workspace
  }, {
    path: 'form/:formId',
    component: FormComponent
  }, {
    path: 'preview/:formId',
    component: FormPreview
  }, {
    path: 'test',
    component: TestComponent
  }
];

export const routing = RouterModule.forRoot(routes, {useHash: true});
