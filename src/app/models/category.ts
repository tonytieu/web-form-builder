export class Category {
    id : string;
    name : string;

    constructor(newid : string, newname : string) {
        this.id = newid;
        this.name = newname;
    }
}