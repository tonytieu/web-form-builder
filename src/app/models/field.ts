import {UUID} from 'angular2-uuid';

export class Controller {
    name : string;
    link : string;
    fields : Array < Field > = [];

    constructor(newName : string, newLink : string) {
        this.name = newName;
        this.link = newLink;
    }
}

export class Form {
    id : string;
    name : string;
    createdOn : Date;
    updatedOn : Date;
    fields : Array < Field >;
    controller : Controller;

    public state : string = 'inactive';
    showStyle : string = "default";
    customCss : string;

    constructor(newname
        ?
        : string) {
        this.name = newname;
        this.id = UUID.UUID();
        this.fields = new Array();
        this.createdOn = new Date();
    }

    toggleState() {
        this.state = (this.state === 'active'
            ? 'inactive'
            : 'active');
    }
}

export class Field {
    id : string;
    name : string;
    type : string;
    origin : string;
    templateId : string;
    formId : string;
    localValidations : Array < Validation >;
    serverValidations : Array < Validation >;
    link : string;
    value : string;
    isEditable : boolean;
    isContainer : boolean = false;
    subFields : Array < Field >;
    icon : string;
    tooltip : string;
    options : Array < Option >;
    ans : any;
    shouldEdit : boolean = false;
    isShown : boolean = true;
    min : number = 0;
    max : number = 100;
    step : number = 1;
    from : number = 0;
    tabs : Array < Tab >;
    isRowContinued : boolean = false;
    isRequired : boolean = false;
    public mouseOvered : boolean = false;
    errors : any;
    action : string; // form action url
    method : string; // form method: GET, POST, DELETE, PUT

    constructor(newid : string, newname : string, newtype : string, newicon : string, newtooltip : string) {
        this.id = newid
            ? newid
            : UUID.UUID();
        this.name = newname;
        this.type = newtype;
        this.icon = newicon;
        this.tooltip = newtooltip;
        this.localValidations = [];
        this.serverValidations = [];
        if (this.type == 'container') {
            this.isContainer = true;
        }
        this.subFields = [];
        this.options = [];
        this.tabs = [];
    }
}

export class Tab {
    title : string;
    content : string;
    disabled : boolean = false;
    removable : boolean = true;
    active : boolean;
    subFields : Array < Field >;

    constructor() {
        this.subFields = [];
    }
}

export class Option {
    id : string;
    value : string;
    link : any; // field id

    constructor(newvalue : string) {
        this.id = UUID.UUID();
        newvalue = newvalue
            ? newvalue
            : "option";
        this.value = newvalue;
    }
}

export class Validation {
    id : string;
    name : string;
    type : string;
    desc : string;
    link : string;
    variables : Array < string >;
    length : number;
    from : number;
    to : number;
    isRequired : true;

    constructor(newid : string, newname : string, newtype : string, newDesc : string) {
        this.id = newid;
        this.name = newname;
        this.type = newtype;
        this.desc = newDesc;
    }
}

export class FieldTypes {
    public static readonly DROPDOWN : string = "dropdown";
    public static readonly LABEL : string = "label";
    public static readonly SHORT_TEXT : string = "text";
    public static readonly LONG_TEXT : string = "textarea";
    public static readonly MULTIPLE_CHOICE : string = "checkbox";
    public static readonly SINGLE_CHOICE : string = "radio";
    public static readonly NUMBER : string = "number";
    public static readonly QUESTION_GROUP : string = "container";
    public static readonly DATE : string = "date";
    public static readonly IMAGE : string = "image";
    public static readonly HIDDEN : string = "hidden";
    public static readonly SLIDER : string = "slider";
    public static readonly TAB : string = "tab";
    public static readonly RICH_TEXT : string = "rich_text";
    public static readonly SUBMIT_BUTTON : string = "submit_button";
}

export class Validationypes {
    public static readonly LENGTH : string = "length";
    public static readonly EMAIL : string = "email";
    public static readonly REQUIRED : string = "required";
}