export class Template {
    id : string;
    name : string;
    categoryId : string;

    constructor(newid : string, newname : string, newCategoryId : string) {
        this.id = newid;
        this.name = newname;
        this.categoryId = newCategoryId;
    }
}