/* karthik 07-04*/ 
//Add forEach Array method support
'use strict';
if (!('forEach' in Array.prototype)) {
    Array.prototype.forEach= function(action, that /*opt*/) {
        for (var i= 0, n= this.length; i<n; i++)
            if (i in this)
                action.call(that, this[i], i, this);
    };
}
var BrowserDetect = { 

 init: function () {
	var isDeviceSize = window.innerWidth <= 800;	
	//var mq = window.matchMedia( "(min-width: 800px)" );
   //if(navigator.userAgent.match(/iPad|iPhone|Android|BlackBerry|webOS|Mobile/i)){ 
  // if (mq.matches) {
      if(navigator.userAgent.match(/iPad|iPhone|Android|BlackBerry|webOS|Mobile/i)){
          this.loadjscssfile("iwov-resources/styles/devices.css", "css", true);
      } else {
          this.loadjscssfile("iwov-resources/styles/desktop.css", "css", false);
          this.loadjscssfile("iwov-resources/styles/desktop_continue.css", "css", false);
      }
  },
 loadjscssfile: function (filename, filetype, ifMetaTag) {
    var metaTagHand = document.createElement("meta");
   metaTagHand.setAttribute("name", "HandheldFriendly");   
   metaTagHand.setAttribute("content", "True");      
 
   var metaTag = document.createElement("meta");
   metaTag.setAttribute("name", "viewport");   
   metaTag.setAttribute("content", "width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"); 
    
   var fileref=document.createElement("link");
   fileref.setAttribute("rel", "stylesheet");
   fileref.setAttribute("type", "text/css");
   fileref.setAttribute("href", filename);
   //fileref.setAttribute("media", "screen");
	 
     if (typeof fileref!="undefined"){
     if (ifMetaTag){
    document.getElementsByTagName("head")[0].appendChild(metaTagHand);
    document.getElementsByTagName("head")[0].appendChild(metaTag);
  }    
    document.getElementsByTagName("head")[0].appendChild(fileref);
    } 
 } 
};
BrowserDetect.init();
 
/*Header BR Tag Remove 11 Jun 2013*/
$(document).ready(function() {
 $('ul#bu-dropdown li.list-heading h1').each(function(){
    var heading = $(this).text();
  heading = heading.replace("<br/>"," ");
  $(this).text(heading);
 });
 $('div#language-country-dropdown li.list-heading h1').each(function(){
  var heading = $(this).text();
  heading = heading.replace("<br/>"," ");
  $(this).text(heading); 
 });
 $('div#segment-dropdown li.list-heading h1').each(function(){
  var heading = $(this).text();
  heading = heading.replace("<br/>"," ");
  $(this).text(heading); 
 });
 /*************** Cookie Value updation statrs here ******************************/
 function setCookie(c_name,value,exdays,urlPath)
        {
            var exdate=new Date();
            exdate.setDate(exdate.getDate() + exdays);
            var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString()) + ";path=" +urlPath;
            document.cookie=c_name + "=" + c_value;
        }
    
        setCookie("segmentValue", "", "-1", "/");
        var pageURL = window.location.href;
      
        var pageSegments = pageURL.split('/');
        var pageSegment = "";
        if(pageSegments != null && pageSegments != "")
        {
   
            if(pageSegments[3] == "in")
                 pageSegment = pageSegments[4];
            else if(pageSegments[3] == "id")
                 pageSegment = pageSegments[4];
            else
                 pageSegment = pageSegments[3];
        }
        //var arrValues = [ "treasures", "personal", "sme", "corporate" ];
        var arrValues = [ "treasures", "sme", "corporate", "treasures-private-client", "private-banking" ];
  arrValues.forEach(function(entry) {
     if(pageSegment==entry)
     {
      "console" in window && console.log("cookie passed here is --->"+pageSegment);
       setCookie("segmentValue", pageSegment, 99999,"/");
     }
  });
       /* if((pageSegment!=null || pageSegment!='')&&((pageSegment != "index")&&(pageSegment != "index-sc")&&(pageSegment != "index-zh")&&(pageSegment != "index-id")))
        {
   "console" in window && console.log("cookie passed here is --->"+pageSegment);
            setCookie("segmentValue", pageSegment, 99999,"/");
        }*/
 /*************** Cookie Value updation ends here ******************************/
});
