/**
 * REVISION HISTORY
 * ------------------------------------------------------------------------------------
 * Date         Author                  Details
 * ------------------------------------------------------------------------------------
 * 2013-06-25   DBS-Kevin               Load maps on click.
 * ------------------------------------------------------------------------------------
 */
function log(x,y) {
 // console.trace();
  if(y==void 0) y=""; 
  if(window.console&&window.console.log){window.console.log("footer.js(log): " + x,y);}
  
}
  
function getRegion() {
  var region = "sg"; //default is SG

  try{log("PARAM(country):",country);}catch(e){log("ERROR:",e);}
  
  try{
	var country = $("#country").val();
    country = $.trim(country.toLowerCase());
    //Hong Kong
    if(country == 'hk' || country.search('hong')!=-1){
      region = 'hk';
    //Singapore
    }else if(country == 'sg' || country.search('sing')!=-1){
      region = 'sg';
    //India
    }else if(country == 'in' || country.search('india')!=-1){
      region = 'in';
    //China
    }else if(country == 'cn' || country.search('china')!=-1){
      region = 'cn';
    //Taiwan
    }else if(country == 'tw' || country.search('taiwan')!=-1){
      region = 'tw';
    //Indonesia
    }else if(country == 'id' || country.search('indo')!=-1){
      region = 'id';
    }
  }catch(e){
    log("ERROR:",e);
  }
  log("region:",region);
  return region;
}

function initializeFooter() {
  log('initializeFooter()');
  /**Google Map Place API**/
  var geocodeInputFooter = document.getElementById('geocodeInputFooter');
  
  var autocomplete = new google.maps.places.Autocomplete(geocodeInputFooter, {
    types: ["geocode"],
    componentRestrictions: {country: getRegion()}
  });
}

$(function(){
  /**ACTION: Go to Find Branch Page**/
  var goToFindBranchPage = function(e) {
    if($('#geocodeInputFooter').val()!=""){
      var url = $('#goBranchfinder').attr('href');
      url = url + (/\?$/.test(url) ? "" : "?") + "q=" + $('#geocodeInputFooter').val();
      window.open(url);
    }
  };
  
  /**EVENT: On Search Click**/
  $('#goBranchfinder').click(goToFindBranchPage);
  
  /**EVENT: On Enter Press**/
  $('#geocodeInputFooter').keypress(function(e){
      var code = (e.keyCode ? e.keyCode : e.which);
      if (code == 13) { goToFindBranchPage()}
  });
});