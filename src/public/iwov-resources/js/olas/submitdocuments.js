$(document).ready(function() {
    var submitDocumentObj = new submitDocument();
    submitDocumentObj.init();

    $('.buttonHolder button').click(function(e) {
        var href = $(this).attr('href');
        OLASSubmitForm($('.form-horizontal'), href);
    });
});
var submitDocument = (function() {
    $("input[type=file]").attr("accept", "application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint, text/plain, application/pdf, image/*");

    var popupEditorObj = new popupEditor();
    this.init = function() {
        //listener for submit block
        $('.form-horizontal .control-group input[name="submitDetails"]').change(function() {
            toggleSubmitBlock();
        });
        toggleSubmitBlock();
        //enable the upload icon once user selects the file
        $('.upload').change(function() {
            $(this).parents('.controls').find('.input-file').val($(this).val());
            if ($(this).val() != '')
                $(this).parents('.controls').find('.btn-file-upload').removeAttr('disabled');
        });
        //upload and show the progress bar once the user upload the file
        $('.btn-file-upload').click(function() {
            var pathname = $(this).parents('.controls').find('.input-file').val();
            if (pathname == '') return;
            var file = pathname.split('\\').pop().split('/').pop(); //split the filename from path name

            var uploadBar = $('.upload_Bar').clone(); //get the cloned upload bar
            var controlObj = $(this).parents('.controls');
            uploadBar.addClass('cloned');
            uploadBar.find('.UB_progress-bar').css('width', '0px');
            $(this).after(uploadBar);

            //DO AJAX HERE

            //CALL BACK FOR AJAX
            var progressTotal = 1;
            progressTotal *= 100;
            var animationLength = 2500;
            //animating
            controlObj.find('.UB_progress-bar').animate({
                width: progressTotal + '%'
            }, {
                duration: animationLength,
                progress: function(obj, progress, remaining) {
                    controlObj.find('.percent').html(Math.round((progress * 100)) + '%');
                }
            }).promise().done(function() {
                uploadBar.remove();
                if (controlObj.find('.upload_list').length == 0)
                    controlObj.find('.btn-file-upload').after('<ul class="upload_list"></ul>');

                controlObj.find('.upload_list').append('<li><p><span class="noMTop removeElement_icon  mr5" rel="remove"></span>' + file + '</p></li>');
                OLASClear(controlObj);
                controlObj.find('.input-file').val('');
                controlObj.find('.btn-file-upload').attr('disabled', 'true');
            });
        });
        //show the input box while editing
        $('#emailLabel_DIV button').click(function() {
            $('#emailEdit_DIV').removeClass('hide');
            $('#emailLabel_DIV').addClass('hide');
            $('#emailEdit_DIV input[type="text"]').val($('#mailID_Label').html());
        });
        //validate and update the area while updating and hide the box
        $('#emailEdit_DIV button').click(function() {
            var validationObj = new validation();
            var DBSvalidationObj = new DBSvalidation();
            DBSvalidationObj.validateObj(validationObj, $('#mailID'));
            if (validationObj.isValid()) {
                $('#emailLabel_DIV').removeClass('hide');
                $('#emailEdit_DIV').addClass('hide');
                $('#mailID_Label').html($('#emailEdit_DIV input[type="text"]').val());
            }
        });

        //delete the files uploaded
        $('.form-horizontal').delegate('.upload_list .removeElement_icon', 'click', function(e) {
            var popUpObj = $('#modalWindow_Delete');
            var th = $(this);
            popupEditorObj.setPopUpObj(popUpObj);
            popupEditorObj.show();

            $(popUpObj).undelegate('.btn-panel-close', 'click').delegate('.btn-panel-close', 'click', function() {
                popupEditorObj.hide();
            });

            $(popUpObj).undelegate('.modal-footer button', 'click').delegate('.modal-footer button', 'click', function(e) {
                popupEditorObj.hide();
                if ($(this).hasClass('cancel')) return;
                th.parents('li').remove();
            });
        });
        //toggling the mail documents block
        $('#mail_DIV .control-group input[name="mailDocs"]').change(function() {
            toggleSendDocBlock();
        });
        toggleSendDocBlock();



        //validating the comments field
        $('#upload_DIV').delegate('.comments', 'change', function() {
            var validationObj = new validation();
            if (!validationObj.isNonEmpty($(this).val()) || !validationObj.isAlphaNumeric($(this).val()))
                $(this).addClass('error').removeClass('success');
            else
                $(this).addClass('success').removeClass('error');
        });
        //validation
        $('.form-horizontal .buttonHolder button').click(function() {
            var href = $(this).attr('href');
            if ($(this).hasClass('prev-btn')) OLASSubmitForm($('.form-horizontal'), href);
            if (validateSubmitDocuments($(this)))
                OLASSubmitForm($('.form-horizontal'), href);
            else
                OLASErrorForm($('.form-horizontal'));
        });

        var DBSvalidationObj = new DBSvalidation();
        DBSvalidationObj.init();
        //calendar functionality
        if ($('#meetingSlot').length > 0) {
            $('#meetingSlot').datepick({
                minDate: "0",
                dateFormat: "dd - mm - yyyy",
                onSelect: function(dateText, inst) {
                    var DBSvalidationObj = new DBSvalidation();
                    var validationObj = new validation();
                    DBSvalidationObj.validateObj(validationObj, $(this));
                }
            });
        };

        $("#meetingSlotAccord").smk_Accordion({ closeAble: true });
    };

    //toggler for submit block
    var toggleSubmitBlock = function() {
        $('.form-horizontal .submitBlock').addClass('hide');
        var block = $('.form-horizontal .control-group input[name="submitDetails"]:checked').attr('id');
        if (block != undefined) {
            $('#' + block + '_DIV').removeClass('hide');
            if (block != 'upload') {
                OLASClear($('.form-horizontal .submitBlock:not(#' + block + '_DIV')); //clear the block datas
                $('.upload_list').remove(); //clear the upload list
            } else if (block != 'email') {
                $('#emailLabel_DIV').removeClass('hide'); //toggle the blocks
                $('#emailEdit_DIV').addClass('hide');
            }
        }
    };

    //toggler for send document block
    var toggleSendDocBlock = function() {
        $('#mail_DIV .sendDocBlock').addClass('hide');
        var block = $('#mail_DIV .control-group input[name="mailDocs"]:checked').attr('id');
        if (block != undefined)
            $('#' + block + '_DIV').removeClass('hide');
    };




    //validating the page
    var validateSubmitDocuments = function(obj) {
        var validationObj = new validation();
        var DBSvalidationObj = new DBSvalidation();
        if (!obj.hasClass('next-btn')) DBSvalidationObj.setCompulsory(false);

        //validating the file uploader
        if (obj.hasClass('next-btn')) {
            //validating the necessary file upload fields
            if ($('#upload_DIV .upload:visible').length > 0) {
                $('#upload_DIV .upload[data-dbsvalidate]').each(function() {
                    if ($(this).attr('data-nonrequired') == 'true') return;
                    if (!validationObj.hasCond($(this).parents('.controls').find('.upload_list li').length) > 0)
                        $(this).parents('.control-group').addClass('error').removeClass('success');
                    else
                        $(this).parents('.control-group').addClass('success').removeClass('error');
                });
            }
            //validating the comment box

            if ($('#upload_DIV .comments:visible').length > 0) //there is no comment box in branchappointment
            {
                var commentObj = $('#upload_DIV .comments');
                if (!validationObj.isNonEmpty(commentObj.val()) || !validationObj.isAlphaNumeric(commentObj.val()))
                    commentObj.addClass('error').removeClass('success');
                else
                    commentObj.addClass('success').removeClass('error');
            }
        }
        //validating the remaining fields
        DBSvalidationObj.validate($('.form-horizontal'), validationObj);
        return validationObj.isValid();
    };

});