$(document).ready(function() {
    var olasThankYouObj = new olasThankYou();
    olasThankYouObj.init();

    $('.buttonHolder button').click(function(e) {
        if (!validatePreDetails()) {
            return;
        }

        var href = $(this).attr('href');
        console.log(href);
        location.href = href;
        //OLASSubmitForm($('.form-horizontal'), href);
    });


});

//validate the form in the page
var validatePreDetails = function() {

    var validationObj = new validation();
    var DBSvalidationObj = new DBSvalidation();

    DBSvalidationObj.validate($('.form-horizontal'), validationObj);
    return validationObj.isValid();
};


var olasThankYou = (function() {
    this.init = function() {

        var cloneObj = new cloner();
        cloneObj.init();

        var DBSvalidationObj = new DBSvalidation();
        DBSvalidationObj.init();

        //cloning process
        // var cloneRemove = $('<span class="removeElement_icon"></span>');
        // var cloneAdd = $('<span class="addElement_icon"></span>');
        // //clone the send application to another email block
        // $('.form-horizontal').undelegate('.emailcloneparent .controls .addElement_icon', 'click').delegate('.emailcloneparent .controls .addElement_icon', 'click', function() {
        //     var cloneObj = $(this).parents('.emailcloneparent').clone();
        //     cloneObj.find('label').html(''); //remove content in the label
        //     cloneObj.find('input').val('');
        //     cloneObj.removeClass('success error'); //remove unwanted classes

        //     if (cloneObj.find('.removeElement_icon').length == 0) cloneObj.find('.addElement_icon').before(cloneRemove); // add the remove icon if not exists
        //     $('.emailcloneparent .addElement_icon').remove(); // remove the add icon from last one
        //     $('.emailcloneparent').last().after(cloneObj);
        //     if ($('.emailcloneparent').length >= 5) $('.emailcloneparent .addElement_icon').remove(); // remove the add icon if the length reaches 5

        //     iterateEmailIdentifiers();
        // });

        // //removing process
        // $('.form-horizontal').undelegate('.emailcloneparent .controls .removeElement_icon', 'click').delegate('.emailcloneparent .controls .removeElement_icon', 'click', function() {
        //     $(this).parents('.emailcloneparent').remove(); //remove the requesting one

        //     var appendObj = $('.emailcloneparent').last(); //get the last obj

        //     if (appendObj.find('.addElement_icon').length == 0)
        //         appendObj.find('.errorMessage').before(cloneAdd); //add the add icon

        //     iterateEmailIdentifiers();
        //     if ($('.emailcloneparent .controls').length <= 1) $('.emailcloneparent .controls input').trigger('focusout'); //trigger while the total elements is 1
        // });
        // //validating the fields on focus out
        // $('.form-horizontal').undelegate('.emailcloneparent input', 'focusout').delegate('.emailcloneparent input', 'focusout', function() {
        //     var val = $(this).val();
        //     //if there is only one email box no need of nonempty validation
        //     if ($('.emailcloneparent .controls').length == 1 && $(this).val() == '') {
        //         $(this).parents('.emailcloneparent').removeClass('success error');
        //         return;
        //     };
        //     var validationObj = new validation();
        //     if (!validationObj.isNonEmpty(val) || !validationObj.isEmail(val))
        //         $(this).parents('.emailcloneparent').addClass('error').removeClass('success');
        //     else
        //         $(this).parents('.emailcloneparent').addClass('success').removeClass('error');
        // });
        // //validation the form on submission
        // $(".form-horizontal .next-btn").click(function(event) {
        //     var validationObj = new validation();
        //     var val = '';

        //     //if there is only one email box no need of nonempty validation
        //     if ($('.emailcloneparent .controls').length == 1 && $('.emailcloneparent .controls input').val() == '') {
        //         window.location.href = 'http://www.dbs.com.sg';
        //         return;
        //     };

        //     $('.emailcloneparent .controls').each(function() {
        //         val = $(this).find('input').val();
        //         if (!validationObj.isNonEmpty(val) || !validationObj.isEmail(val))
        //             $(this).parents('.emailcloneparent').addClass('error').removeClass('success');
        //         else
        //             $(this).parents('.emailcloneparent').addClass('success').removeClass('error');
        //     });

        //     if (validationObj.isValid())
        //         window.location.href = $(this).attr('href');
        // });
    };
    //iterate the email text box to maintain the unique id
    var iterateEmailIdentifiers = function() {
        var id_count = 0;
        $('.emailcloneparent .controls').each(function() {
            id_count++;
            $(this).find('input').attr('id', 'inputEmailAdd' + id_count);
        });
    };
});