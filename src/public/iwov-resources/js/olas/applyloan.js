$(document).ready(function() {
    var applyLoanObj = new applyLoan();
    applyLoanObj.init();
});

var applyLoan = (function() {
    var popupEditorObj = new popupEditor();

    //flag to check whether the status is checked or not
    var statusChecked = 0;
    this.init = function() {
            var block = $('.control-group input[type="radio"]:checked').attr('id');
            toggleBlock(block);

            $("#saved_OTPValue").on('focusout', function() {

                var $ele = $(this);
                setTimeout(function() {
                    console.log("onchange" + $ele.hasClass("success"));
                    if ($ele.hasClass("success")) {
                        $("#saved_sendNumber ~ a:contains('Resend OTP')").removeClass("btn-disabled");
                        $("#doneSavedApplication").removeAttr("disabled");
                    } else {
                        $("#saved_sendNumber ~ a:contains('Resend OTP')").addClass("btn-disabled");
                        $("#doneSavedApplication").attr("disabled", "disabled");
                    }
                }, 200);

            });

            //listener for toggler
            $('.form-horizontal .control-group.applyType  input[type="radio"]').change(function() {
                toggleBlock($(this).attr('id'));
            });

            //validating while requesting for otp
            $('#modalWindow_savedApplication .sendotp,#modalWindow_statusApplication .sendotp').click(function() {
                var validationObj = new validation();
                var DBSvalidationObj = new DBSvalidation();

                var controlObj = $(this).parents('.control-group');
                //validate the object and get the status
                if (!DBSvalidationObj.validateObj(validationObj, controlObj.find('select'), true)) {
                    controlObj.addClass('error').removeClass('success');
                    controlObj.find('select').addClass('error').removeClass('success');
                } else {
                    controlObj.addClass('success').removeClass('error');
                    controlObj.find('select').addClass('success').removeClass('error');
                }
            });

            //submission
            $('.form-horizontal .buttonSection button,.form-horizontal .buttonHolder button').click(function() {
                var href = $(this).attr('href');
                if (!validateApplyLoan($(this))) return;

                //if user requests for new application and the device is tablet show the popup and submit the form
                if ($(this).parents('.NewApplication_DIV').length > 0) {
                    if (navigator.userAgent.match(/iPad|iPhone|Android|BlackBerry|webOS|Mobile|IEMobile/i)) {
                        var popUpObj = $('#modalWindow-confirmation');
                    } else {
                        OLASSubmitForm($('.form-horizontal'), href);
                        return;
                    }
                }
                //else show the otp requesting popup
                else if ($(this).parents('.ContinueSavedApplication_DIV').length > 0)
                    var popUpObj = $('#modalWindow_savedApplication');
                else if ($(this).parents('.CheckApplication_DIV').length > 0)
                    var popUpObj = $('#modalWindow_statusApplication');
                else
                    return;
                popupEditorObj.setPopUpObj(popUpObj); //setting otp requesting popup
                popupEditorObj.show();

                var id = popUpObj.attr('id')
                    //listener to close the popup
                $(popUpObj).undelegate('.btn-panel-close', 'click').delegate('.btn-panel-close', 'click', function() {
                    popupEditorObj.hide();
                    OLASClear($('#' + id));
                });
                //validating the appropriate otp popup
                $(popUpObj).undelegate('.modal-footer button', 'click').delegate('.modal-footer button', 'click', function() {
                    if ($(this).hasClass('cancel')) {
                        popupEditorObj.hide();
                        return;
                    }
                    if (id == 'modalWindow_statusApplication') //status popup
                    {
                        if (validateStatusApplPopUp()) //validating the status application popup
                        {
                            popupEditorObj.hide();
                            toggleBlock('CheckApplicationStatus'); //toggle the block
                            statusChecked = 1;
                        }
                        return;
                    } else if (id == 'modalWindow_savedApplication') //saved application popup
                    {
                        if (validateSavedApplPopUp()) //validating the saved application popup
                            popupEditorObj.hide();
                        else
                            return;
                    }
                    OLASSubmitForm($('.form-horizontal'), href); //submit the form
                });
            });
            //initiating the dbs validation 
            var DBSvalidationObj = new DBSvalidation();
            DBSvalidationObj.init();
            DBSvalidationObj.setCompulsory(true);
        }
        //toggler function for user requesting application block
    var toggleBlock = function(block) {
        $('.form-horizontal .control-group.applyBlock').addClass('hide');
        OLASClear($('.form-horizontal .control-group.applyBlock'));
        //twist for if application status is already checked
        if (block == 'CheckApplication' && statusChecked == 1) block = 'CheckApplicationStatus';
        if (block != undefined)
            $('.form-horizontal .control-group.' + block + '_DIV').removeClass('hide');
    };

    //validating saved application popup
    var validateSavedApplPopUp = function() {
        var validationObj = new validation();
        var DBSvalidationObj = new DBSvalidation();

        DBSvalidationObj.validate($('#modalWindow_savedApplication'), validationObj);
        return validationObj.isValid();
    };

    //validating the status application popup
    var validateStatusApplPopUp = function() {
        var validationObj = new validation();
        var DBSvalidationObj = new DBSvalidation();

        DBSvalidationObj.validate($('#modalWindow_statusApplication'), validationObj);
        return validationObj.isValid();
    };

    //validate the page
    var validateApplyLoan = function(obj) {
        var validationObj = new validation();
        var DBSvalidationObj = new DBSvalidation();

        DBSvalidationObj.validate($('.form-horizontal'), validationObj);
        return validationObj.isValid();
    };
});