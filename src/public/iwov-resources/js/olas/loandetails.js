$(document).ready(function() {
    var loanDetailObj = new loanDetail();
    loanDetailObj.init();

    $("input[type=checkbox]").on("click", function(e) {
        // var $inputs = $(this).parents("li").find("input");
        // if ($inputs.length > 0) {
        //     $inputs.on('focusout', function() {
        //         //console.log("focusout");
        //         if ($("input[type=text].error").length == 0) {
        //             $('.next-btn').removeAttr("disabled");
        //         } else {
        //             $('.next-btn').attr("disabled", "disabled");
        //         }

        //         setTimeout(function() {
        //             if ($("input[type=text].error").length == 0) {
        //                 $('.next-btn').removeAttr("disabled");
        //             } else {
        //                 $('.next-btn').attr("disabled", "disabled");
        //             }
        //         }, 500);
        //     });

        //     $inputs.trigger("focusout");
        // } else {
        //     if ($("input[type=text].error").length == 0) {
        //         $('.next-btn').removeAttr("disabled");
        //     } else {
        //         $('.next-btn').attr("disabled", "disabled");
        //     }
        // }
    });

    $("#microLoanCheck").on("click", function() {
        var $ele = $(this);
        if ($ele.is(":checked")) {
            $("input[type=checkbox]").not($("#microLoanCheck")).attr("disabled", "disabled");
            $(".input-normal").val("");
            $(".input-mini").val("");
        } else {
            $("input[type=checkbox]").not($("#microLoanCheck")).removeAttr("disabled");
        }
    });

    $(".addElement_icon").on("click", function() {
        setTimeout(function() {
            //console.log("loaded");
            $('[data-cloneitem="AssetFinPropLoan"]:last')
                .find('.btn-compute')
                .unbind("click")
                .on("click", onBtnComputeClicked);
        }, 300);
    });

    $("input[type=checkbox]").filter(function(i, el) { return $(el).is(":checked"); })
        .each(function(index) {
            //console.log($(this).parents(".splitter-tile"));
            $(this).parents(".splitter-tile").find(".splitter-tile-overlay").css("opacity", "0");
            $(this).parents(".splitter-tile").find(".splitter-tile-overlay").css("display", "none");
            $(this).parents(".splitter-tile").find(".splitter-tile-overlay").addClass("inactive");
        });

    $("input[type=text]").filter(function(i, el) { return $(el).val().length > 0; }).each(function(index) {
        //console.log($(this).parents(".splitter-tile"));
        $(this).parents(".splitter-tile").find(".splitter-tile-overlay").css("opacity", "0");
        $(this).parents(".splitter-tile").find(".splitter-tile-overlay").css("display", "none");
        $(this).parents(".splitter-tile").find(".splitter-tile-overlay").addClass("inactive");
    });

});

function onBtnComputeClicked() {
    alert("compute");
}

function onCloneAdderClicked() {
    console.log("onCloneAdderClicked");
}

var loanDetail = (function() {
    var popupEditorObj = new popupEditor();
    this.init = function() {
        toggleBlock();
        organiseCollateral();
        toggleSubmitBtn();

        $('#microLoanCheck').on("click", function(event) {
            var ele = $(this);
            if ($(ele).is(":checked")) {
                var loanCheckboxes = $(".loanCheckbox:checked");

                $.each(loanCheckboxes, function(index) {
                    var checkboxEle = loanCheckboxes[index];
                    //if($(checkboxEle).is(':checked')) 
                    $(checkboxEle).trigger('click');
                });

                if (loanCheckboxes.length > 0) {
                    $(ele).trigger('click');
                }

                $(".buttonHolder .other").removeClass("hide");
                $(".buttonHolder .loanAll").addClass("hide");
            } else {
                $(".buttonHolder .other").addClass("hide");
                $(".buttonHolder .loanAll").removeClass("hide");
            }

        });

        var cloneObj = new cloner();
        cloneObj.init();

        //toggle when checkbox clicked
        $('.form-horizontal .splitter-panel input.loanCheckbox[type="checkbox"]').change(function() {
            if ($(this).is(':checked')) {
                $(this).parents('li').find('.defaultView').addClass('hide');
                $(this).parents('li').find('.selectedView,.selectedViewHld').removeClass('hide');
            } else {
                $(this).parents('li').find('.defaultView,.adderLink').removeClass('hide');
                $(this).parents('li').find('.selectedView,.selectedViewHld').addClass('hide');
                // OLASClear($(this).parents('li').find('.selectedView,.selectedViewHld'));

            }

            //display and hide the overlay
            if ($(this).parents('.splitter-tile').find('input.loanCheckbox[type="checkbox"]:checked').length == 0)
                $(this).parents('.splitter-tile').find('.splitter-tile-overlay').removeClass('inactive');
            else
                $(this).parents('.splitter-tile').find('.splitter-tile-overlay').addClass('inactive');


            //console.log($(this).parents('li').find('.btn-compute'));
            $(this).parents('li').find('.btn-compute').unbind("click").on("click", onBtnComputeClicked);

            toggleBlock();
            organiseCollateral();
        });

        //toggle when checkbox clicked
        $('.form-horizontal .splitter-panel input.loanCheckbox[type="checkbox"]').change(function() {
            toggleSubmitBtn();
        });
        //toggle when collateral checked
        $('.form-horizontal input[name="collateral"]').click(function(e) {
            toggleSubmitBtn();
        });

        //if property loan is checked prevent the change of property collateral
        $('#Property').click(function(e) {
            if ($('#propertyLoan:checked').length)
                e.preventDefault();
        });

        //toggle when checkbox clicked
        $('.form-horizontal .collateral input[type="checkbox"]').change(function() {
            if (!$(this).is(':checked')) return;
            if ($(this).attr('id') == 'noCollateral')
                $(this).parents('.collateral').find('input[type="checkbox"]').not($(this)).attr('checked', false);
            else
                $('#noCollateral').attr('checked', false);
        });

        //validation listeners
        $('.form-horizontal').delegate('.selectedView input:not([type="file"]):not([type="radio"]):not([type="checkbox"])[data-dbsvalidate]', 'focusout', function() {
            // var DBSvalidationObj = new DBSvalidation();
            // var validationObj = new validation();
            // validateObj($(this), DBSvalidationObj.validateObj(validationObj, $(this), true));
        });

        $('.form-horizontal').delegate('.selectedView select[data-dbsvalidate]', 'change', function() {
            // var DBSvalidationObj = new DBSvalidation();
            // var validationObj = new validation();
            // validateObj($(this), DBSvalidationObj.validateObj(validationObj, $(this), true));
        });

        //validation
        $('.form-horizontal .buttonHolder button').click(function(e) {




            var href = $(this).attr('href');
            var otherhref = $(this).attr("data-otherhref");

            if (!$(this).hasClass("saveandexit")) {
                if (!validateLoanDetails()) return;
            }





            //if bankers guarantee is checked show the popup 
            if ($('#BankersGuarantee').is(':checked') && $(this).hasClass('loanBankGuarantor-FD')) {
                var popUpObj = $('#modalWindow-SalesCheck');
                popupEditorObj.setPopUpObj(popUpObj);
                popupEditorObj.show();

                $(popUpObj).undelegate('.btn-panel-close', 'click').delegate('.btn-panel-close', 'click', function() {
                    popupEditorObj.hide();
                });

                $(popUpObj).undelegate('.modal-footer button', 'click').delegate('.modal-footer button', 'click', function(e) {
                    popupEditorObj.hide();
                    OLASSubmitForm($('.form-horizontal'), href);
                });
            } else {
                if ($("#microLoanCheck").is(":checked")) {
                    OLASSubmitForm($('.form-horizontal'), otherhref);
                } else {
                    OLASSubmitForm($('.form-horizontal'), href);
                }
            }

        });

        var DBSvalidationObj = new DBSvalidation();
        DBSvalidationObj.init();
    };

    //hide or show the collateral block and microloan block based on the type of loan selection
    var toggleBlock = function() {
        if ($('.form-horizontal .splitter-panel input[data-loantype="type1"]:checked').length == 0)
            hideBlock('type1');
        else
            showBlock('type1');

        if ($('.form-horizontal .splitter-panel input[data-loantype="type2"]:checked').length == 0)
            hideBlock('type2');
        else
            showBlock('type2');
    };
    //show the collateral section which matches the loantype
    var showBlock = function(loanType) {
        $('.form-horizontal .control-group[data-loanTypeBlock="' + loanType + '"]').removeClass('hide');
    };
    //hide the collateral section which matches the loantype
    var hideBlock = function(loanType) {
        $('.form-horizontal .control-group[data-loanTypeBlock="' + loanType + '"]').addClass('hide');
        //bala
        $('.form-horizontal .control-group[data-loanTypeBlock="' + loanType + '"] input[type=checkbox]').prop("checked", false)
        $('.form-horizontal .control-group[data-loanTypeBlock="' + loanType + '"] input[type=checkbox]').closest('.control-group').removeClass("success")
    };

    //organise the sections in the collateral block
    var organiseCollateral = function() {
        //if property or banker guarantee is checked show the no collateral 
        if ($('#propertyLoan:checked,#BankersGuarantee:checked').length == 0)
            $('#noCollateral').parent('label').removeClass('hide');
        else
            $('#noCollateral').parent('label').addClass('hide');
        //if property loan always checked the c
        if ($('#propertyLoan:checked').length)
            $('#Property').attr('checked', true);
    };

    //toggler for submit button
    var toggleSubmitBtn = function() {
        $('.form-horizontal .buttonHolder .loanBankGuarantor-FD,.form-horizontal .buttonHolder .loanAll,.form-horizontal .buttonHolder .loanMicro').addClass('hide');

        //only micro loan
        if ($('#microLoan').is(':checked') &&
            $('.form-horizontal .loanCheckbox:checked').length == 1 && $('.form-horizontal input:visible[name="collateral"]:checked').length == 0)
            $('.form-horizontal .buttonHolder .loanMicro').removeClass('hide');
        //only fixed deposit and bankers guarantee
        else if ($('#FixedDeposit:visible').is(':checked') && $('#BankersGuarantee').is(':checked') &&
            $('.form-horizontal .loanCheckbox:checked').length == 1 && $('.form-horizontal input:visible[name="collateral"]:checked').length == 1)
            $('.form-horizontal .buttonHolder .loanBankGuarantor-FD').removeClass('hide');
        else
            $('.form-horizontal .buttonHolder .loanAll').removeClass('hide');
    };

    //validate the form
    var validateLoanDetails = function() {


        var DBSvalidationObj = new DBSvalidation();
        var validationObj = new validation();

        //validating the loan type checkbox
        if ($("#microLoanCheck").is(":checked")) {
            return true;
        }

        if (!DBSvalidationObj.validateObj(validationObj, $('.form-horizontal input[name="loanType"][data-dbsvalidate]'), true)) {
            console.log("error");
            $('#ErrorDiv').css('display', 'block');
            return;
        } else
            $('#ErrorDiv').css('display', 'none');

        //normal global validation
        DBSvalidationObj.validate($('.form-horizontal'), validationObj);

        //special case validations
        $('.selectedView:visible input:not([type="file"]):not([type="radio"]):not([type="checkbox"])[data-dbsvalidate]').each(function() {
            validateObj($(this), DBSvalidationObj.validateObj(validationObj, $(this), true));
        });

        $('.selectedView:visible select[data-dbsvalidate]').each(function() {
            validateObj($(this), DBSvalidationObj.validateObj(validationObj, $(this), true));
        });
        return validationObj.isValid();
    };
    //validate the object and add or remove the classes based on the validation statu
    var validateObj = function(obj, status) {
        if (!status) {
            if (obj.attr('type') != 'radio' && obj.attr('type') != 'checkbox') {
                obj.addClass('error').removeClass('success');
                if (obj.is('select'))
                    obj.parent('.select-wrapper').addClass('error').removeClass('success');
            }
            obj.parents('.selectedView').addClass('error').removeClass('success');
            return;
        }

        if (obj.attr('type') != 'radio' && obj.attr('type') != 'checkbox') {
            obj.addClass('success').removeClass('error');
            if (obj.is('select'))
                obj.parent('.select-wrapper').addClass('success').removeClass('error');
        }
        var adjacentStatus = checkAdjcentStatus(obj);
        if (adjacentStatus === false) //if there is any boxes with error
            obj.parents('.selectedView').addClass('error').removeClass('success');
        if (adjacentStatus === true) //if there is any boxes with success
            obj.parents('.selectedView').addClass('success').removeClass('error');
        else if (adjacentStatus === null) //if there is no status boxes
            obj.parents('.selectedView').removeClass('success error');
    };

    //check the adjacent element validation status
    var checkAdjcentStatus = function(obj) {
        //TODO: by now skip this for radio and checkboxes
        if (obj.attr('type') == 'radio' || obj.attr('type') == 'checkbox') return true;
        //if there is no adjacent element just return true;	
        if (obj.parents('.selectedView:visible').find('input[data-dbsvalidate]:not([type="radio"]):not([type="file"]):not([type="checkbox"]),select[data-dbsvalidate]').not(obj).length == 0)
            return true; //check whether there is no other elements
        if (obj.parents('.selectedView:visible').find('input[data-dbsvalidate]:not([type="radio"]):not([type="file"]):not([type="checkbox"]).error,select[data-dbsvalidate].error').not(obj).length > 0)
            return false;
        else if (obj.parents('.selectedView:visible').find('input[data-dbsvalidate]:not([type="radio"]):not([type="file"]):not([type="checkbox"]).success,select[data-dbsvalidate].success').not(obj).length > 0)
            return true;
        else
            return null;
    };
});