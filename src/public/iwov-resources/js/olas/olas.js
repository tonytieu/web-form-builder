$(document).ready(function(){
	var olasObj = new olas();
	olasObj.init();

	if (!String.prototype.startsWith) {
	  String.prototype.startsWith = function(searchString, position) {
	    position = position || 0;
	    return this.indexOf(searchString, position) === position;
	  };
	}

	if(!String.prototype.replaceAll) {
		String.prototype.replaceAll = function(search, replacement) {
		    var target = this;
		    return target.replace(new RegExp(search, 'g'), replacement);
		};
	}
});
var olas = (function(){
	this.init = function(){
		this.triggerBtnCheck();
		//prevents the default action of submitting the form
		$('.form-horizontal').delegate('button','click',function(e){
			e.preventDefault();
		});
	};

	//KEEP IT TEMPORARILY
	//parse the query params from url and convert it to the array map
	this.getQueryArr = function(checkUrl){
		var url = document.createElement('a');
		url.href = checkUrl;
		var queryString = url.search;
		var queryListMap = new Array();
		if(queryString != '')
		{
			var queryArr = new Array();
			var queryMap = new Array();
			queryString = queryString.substring(0,1) == '?' ? queryString.substring(1,queryString.length) : queryString;
			queryArr = queryString.split('&');
			for (var key in queryArr)
		  	{
		    	queryMap = queryArr[key].split('=');
		    	queryListMap[queryMap[0]] = queryMap[1] != undefined ? queryMap[1] : '';
		  	}
		}	
		return queryListMap;	
	};
	//hide or show the required progress bar based on the query params
	this.triggerBtnCheck = function(){
		//if there is only one bread crumb, no need to check
		if($('#progress-bar ul').length == 1) return;

		var queryArr = this.getQueryArr(document.URL);
		if(queryArr['microLoan']!= undefined && queryArr['microLoan'] == 1)
		{
			$('.btn.loanMicro').removeClass('hide');
			$('.btn.loanAll').addClass('hide');
			$('#progress-bar ul.five-steps').removeClass('hide');
			$('#progress-bar ul:not(.five-steps)').addClass('hide');
		}
		else
		{
			$('.btn.loanMicro').addClass('hide');
			$('.btn.loanAll').removeClass('hide');
			$('#progress-bar ul.five-steps').addClass('hide');
			$('#progress-bar ul:not(.five-steps)').removeClass('hide');
		}
	};
});

//submit the form
function OLASSubmitForm(form,href){
	form.attr('method','POST');
	form.attr('action',href);
	form.submit();
};

//clear the input elements and reset the default values
function OLASClear(obj){
	obj.find('input:not([type="radio"]):not([type="checkbox"]):not([readonly]):not([disabled]):not(.placeholder),textarea').val('');
	obj.find('.success,.error').removeClass('success error');
	obj.find('select').each(function(){
		OLASClearSelect($(this));
	});

	obj.find('input[type="radio"]:not([data-default]),input[type="checkbox"]:not([data-default])').attr('checked',false);
	obj.find('input[type="radio"][data-default],input[type="checkbox"][data-default]').attr('checked',true);
	obj.removeClass('success error');
};

//reset the default values for select box
function OLASClearSelect(obj){
	var defValue = obj.find('option[data-default]').attr('value');
	if(defValue == undefined)
	{	
		// //TODO: temporary fix until the data-default added
		// if(obj.find('option[value="DBS"]').length > 0)
		// 	defValue = obj.find('option[value="DBS"]').attr('value');
		// else if(obj.find('option[value="Singapore"]').length > 0)
		// 	defValue = obj.find('option[value="Singapore"]').attr('value');
		// //TODO: upto here
		// else
			defValue = obj.find('option:first').attr('value');
	}

	obj.val(defValue);
	obj.siblings('.holder').html(obj.find('option:selected').html());
};

//show error message
function OLASErrorForm(form){
	form.find('#ErrorDiv').css('display','block');
};

//the same code which is in placeholdermodernizer
//it works on the placeholder which is not supported in ie versions
function OLASplaceholderModernizr(obj){
	
	if (!Modernizr.input.placeholder) {
		obj.find('input:not([type="radio"]):not([type="checkbox"])[placeholder],textarea[placeholder]').each(function(){				
			if ($(this).val() === '') {
           	    $(this).val($(this).attr('placeholder')).addClass('placeholder');
            }
		});
	}
	return obj;
}

function DBSConsoleLog(val){
	//console.log(val);
}

//thousand separator script
(function($){$.fn.priceFormat=function(options){var defaults={prefix:'US$ ',suffix:'',centsSeparator:'.',thousandsSeparator:',',limit:false,centsLimit:2,clearPrefix:false,clearSufix:false,allowNegative:false,insertPlusSign:false};var options=$.extend(defaults,options);return this.each(function(){var obj=$(this);var is_number=/[0-9]/;var prefix=options.prefix;var suffix=options.suffix;var centsSeparator=options.centsSeparator;var thousandsSeparator=options.thousandsSeparator;var limit=options.limit;var centsLimit=options.centsLimit;var clearPrefix=options.clearPrefix;var clearSuffix=options.clearSuffix;var allowNegative=options.allowNegative;var insertPlusSign=options.insertPlusSign;if(insertPlusSign)allowNegative=true;function to_numbers(str){var formatted='';for(var i=0;i<(str.length);i++){char_=str.charAt(i);if(formatted.length==0&&char_==0)char_=false;if(char_&&char_.match(is_number)){if(limit){if(formatted.length<limit)formatted=formatted+char_}else{formatted=formatted+char_}}}return formatted}function fill_with_zeroes(str){while(str.length<(centsLimit+1))str='0'+str;return str}function price_format(str){var formatted=fill_with_zeroes(to_numbers(str));var thousandsFormatted='';var thousandsCount=0;if(centsLimit==0){centsSeparator="";centsVal=""}var centsVal=formatted.substr(formatted.length-centsLimit,centsLimit);var integerVal=formatted.substr(0,formatted.length-centsLimit);formatted=(centsLimit==0)?integerVal:integerVal+centsSeparator+centsVal;if(thousandsSeparator||$.trim(thousandsSeparator)!=""){for(var j=integerVal.length;j>0;j--){char_=integerVal.substr(j-1,1);thousandsCount++;if(thousandsCount%3==0)char_=thousandsSeparator+char_;thousandsFormatted=char_+thousandsFormatted}if(thousandsFormatted.substr(0,1)==thousandsSeparator)thousandsFormatted=thousandsFormatted.substring(1,thousandsFormatted.length);formatted=(centsLimit==0)?thousandsFormatted:thousandsFormatted+centsSeparator+centsVal}if(allowNegative&&(integerVal!=0||centsVal!=0)){if(str.indexOf('-')!=-1&&str.indexOf('+')<str.indexOf('-')){formatted='-'+formatted}else{if(!insertPlusSign)formatted=''+formatted;else formatted='+'+formatted}}if(prefix)formatted=prefix+formatted;if(suffix)formatted=formatted+suffix;return formatted}function key_check(e){var code=(e.keyCode?e.keyCode:e.which);var typed=String.fromCharCode(code);var functional=false;var str=obj.val();var newValue=price_format(str+typed);if((code>=48&&code<=57)||(code>=96&&code<=105))functional=true;if(code==8)functional=true;if(code==9)functional=true;if(code==13)functional=true;if(code==46)functional=true;if(code==37)functional=true;if(code==39)functional=true;if(allowNegative&&(code==189||code==109))functional=true;if(insertPlusSign&&(code==187||code==107))functional=true;if(!functional){e.preventDefault();e.stopPropagation();if(str!=newValue)obj.val(newValue)}}function price_it(){var str=obj.val();var price=price_format(str);if(str!=price)obj.val(price)}function add_prefix(){var val=obj.val();obj.val(prefix+val)}function add_suffix(){var val=obj.val();obj.val(val+suffix)}function clear_prefix(){if($.trim(prefix)!=''&&clearPrefix){var array=obj.val().split(prefix);obj.val(array[1])}}function clear_suffix(){if($.trim(suffix)!=''&&clearSuffix){var array=obj.val().split(suffix);obj.val(array[0])}}$(this).bind('keydown.price_format',key_check);$(this).bind('keyup.price_format',price_it);$(this).bind('focusout.price_format',price_it);if(clearPrefix){$(this).bind('focusout.price_format',function(){clear_prefix()});$(this).bind('focusin.price_format',function(){add_prefix()})}if(clearSuffix){$(this).bind('focusout.price_format',function(){clear_suffix()});$(this).bind('focusin.price_format',function(){add_suffix()})}if($(this).val().length>0){price_it();clear_prefix();clear_suffix()}})};$.fn.unpriceFormat=function(){return $(this).unbind(".price_format")};$.fn.unmask=function(){var field=$(this).val();var result="";for(var f in field){if(!isNaN(field[f])||field[f]=="-")result+=field[f]}return result}})(jQuery);


//tooltip script
//modified version of the tooltip they used
$(document).ready(function(){
	var txt='';
    var targets = $('.help_icon'),
	target = false,
	tooltip = false,
	title = false;
	targetStartEvent="";
	targetEndEvent=""
	if(navigator.userAgent.match(/iPad|iPhone|Windows Phone/i)){ 
		//alert('ipad-windows')
	   targetStartEvent ="click";
	   targetEndEvent ="click";
	}
	else{
	   targetStartEvent ="mouseenter";
	   targetEndEvent = "mouseleave";
	}

		$('body').delegate('.help_icon',targetStartEvent,function(e){
		// targets.on(targetStartEvent, function (e) {
			e.stopPropagation();
            target = $(this);
            var tip = $(this).attr('rel');
            tooltip = $('<div id="tooltip"></div>');

            if (!tip || tip == '')
                return false;

            target.removeAttr('title');
            tooltip.css('opacity', 0)
               .html(tip)
               .appendTo('body');

            var init_tooltip = function () {
                if ($(window).width() < tooltip.outerWidth() * 1.5)
                    tooltip.css('max-width', $(window).width() / 2);
                else
                    tooltip.css('max-width', 340);

                var pos_left = target.offset().left + (target.outerWidth() / 2) - (tooltip.outerWidth() / 2),
                pos_top = target.offset().top - tooltip.outerHeight() - 20;

                if (pos_left < 0) {
                    pos_left = target.offset().left + target.outerWidth() / 2 - 20;
                    tooltip.addClass('left');
                }
                else
                    tooltip.removeClass('left');

                if (pos_left + tooltip.outerWidth() > $(window).width()) {
                    pos_left = target.offset().left - tooltip.outerWidth() + target.outerWidth() / 2 + 20;
                    tooltip.addClass('right');
                }
                else
                    tooltip.removeClass('right');

                if (pos_top < 0) {
                    var pos_top = target.offset().top + target.outerHeight();
                    tooltip.addClass('top');
                }
                else
                    tooltip.removeClass('top');

                tooltip.css({ left: pos_left, top: pos_top })
                   .animate({ top: '+=10', opacity: 1 }, 50);
            };

            init_tooltip();
            $(window).resize(init_tooltip);
			
			var remove_tooltip = function () {
				tooltip.animate({
					top: '-=10',
					opacity: 0
				}, 50, function () {
					$(this).remove();
				});

			};
			if(navigator.userAgent.match(/iPad|iPhone/i)){
				document.getElementById('tooltip').addEventListener('touchend', function(e){
				 e.stopPropagation();
				 }, false)
				 
				document.body.addEventListener('touchend', function(e){
				 remove_tooltip() 
				 }, false)
				
			}
				
			else if(navigator.userAgent.match(/Windows Phone/i)){
				tooltip.on(targetStartEvent, function (e) { e.stopPropagation();});
				bdy.on(targetEndEvent, remove_tooltip);
			
			}

			else{
				target.on(targetEndEvent, remove_tooltip);		   
			} 	
        });
});

/**
 * SMK Accordion jQuery Plugin v1.3
 * ----------------------------------------------------
 * Author: Smartik
 * Author URL: http://smartik.ws/
 * License: MIT
 */
;(function ( $ ) {

	$.fn.smk_Accordion = function( options ) {
		
		if (this.length > 1){
			this.each(function() { 
				$(this).smk_Accordion(options);
			});
			return this;
		}
		
		// Defaults
		var settings = $.extend({
			animation:  true,
			showIcon:   true,
			closeAble:  false,
			closeOther: true,
			slideSpeed: 150,
			activeIndex: false
		}, options );

		if( $(this).data('close-able') )    settings.closeAble = $(this).data('close-able');
		if( $(this).data('animation') )     settings.animation = $(this).data('animation');
		if( $(this).data('show-icon') )     settings.showIcon = $(this).data('show-icon');
		if( $(this).data('close-other') )   settings.closeOther = $(this).data('close-other');
		if( $(this).data('slide-speed') )   settings.slideSpeed = $(this).data('slide-speed');
		if( $(this).data('active-index') )  settings.activeIndex = $(this).data('active-index');

		// Cache current instance
		// To avoid scope issues, use 'plugin' instead of 'this'
		// to reference this class from internal events and functions.
		var plugin = this;

		//"Constructor"
		var init = function() {
			plugin.createStructure();
			plugin.clickHead();
		}

		// Add .smk_accordion class
		this.createStructure = function() {

			//Add Class
			plugin.addClass('smk_accordion');
			if( settings.showIcon ){
				plugin.addClass('acc_with_icon');
			}

			//Create sections if they were not created already
			if( plugin.find('.accordion_in').length < 1 ){
				plugin.children().addClass('accordion_in');
			}

			//Add classes to accordion head and content for each section
			plugin.find('.accordion_in').each(function(index, elem){
				var childs = $(elem).children();
				$(childs[0]).addClass('acc_head');
				$(childs[1]).addClass('acc_content');
			});
			
			//Append icon
			if( settings.showIcon ){
				plugin.find('.acc_head').prepend('<div class="acc_icon_expand"></div>');
			}

			//Hide inactive
			plugin.find('.accordion_in .acc_content').not('.acc_active .acc_content').hide();

			//Active index
			if( settings.activeIndex === parseInt(settings.activeIndex) ){
				if(settings.activeIndex === 0){
					plugin.find('.accordion_in').addClass('acc_active').show();
					plugin.find('.accordion_in .acc_content').addClass('acc_active').show();
				}
				else{
					plugin.find('.accordion_in').eq(settings.activeIndex - 1).addClass('acc_active').show();
					plugin.find('.accordion_in .acc_content').eq(settings.activeIndex - 1).addClass('acc_active').show();
				}
			}
			
		}

		// Action when the user click accordion head
		this.clickHead = function() {

			plugin.on('click', '.acc_head', function(){
				
				var s_parent = $(this).parent();
				
				if( s_parent.hasClass('acc_active') == false ){
					if( settings.closeOther ){
						plugin.find('.acc_content').slideUp(settings.slideSpeed);
						plugin.find('.accordion_in').removeClass('acc_active');
					}	
				}

				if( s_parent.hasClass('acc_active') ){
					if( false !== settings.closeAble ){
						s_parent.children('.acc_content').slideUp(settings.slideSpeed);
						s_parent.removeClass('acc_active');
					}
				}
				else{
					$(this).next('.acc_content').slideDown(settings.slideSpeed);
					s_parent.addClass('acc_active');
				}

			});

		}

		//"Constructor" init
		init();
		return this;

	};


}( jQuery ));

/* --------------- SMK Accordion jQuery Plugin v1.3 --------------- */