var cloner = (function() {
    this.init = function() {
        //toggle the clone items display on loading
        $('.form-horizontal [data-clone]').each(function() {
            toggleClonerDisplay($(this));
        });
        //show the clone element first always
        $('.form-horizontal').undelegate('[data-clone] .cloneadder', 'click').delegate('[data-clone] .cloneadder', 'click', function(e) {
            var parentObj = $(this).parents('[data-clone]'); //clone parent
            var cloneItems = parentObj.find('[data-cloneitem]'); //clone obj
            var cloneObj = cloneItems.first().clone();
            var firstPropertyTypeRadio = $("input[name='bean.propertyDetails[0].propertyType']:checked");
            var firstEquipmentDetailTypeRadio = $("input[name='bean.equipmentDetails[0].ED_Type']:checked");
            var firstVehicleDetailTypeRadio = $("input[name='bean.vehicleDetails[0].vdtype']:checked");
            var firstFixedDepositTypeRadio = $("input[name='bean.fixedDeposit[0].fdtype']:checked");
            cleanCloneObj(cloneObj);
            cloneItems.last().after(cloneObj);

            //cloneObj.find('input[data-dbsvalidate="price"]').val(1000);
            //cloneObj.find('input[data-dbsvalidate="price"]').trigger('focusout');
            cloneObj.find('[data-id="existingProperty"]').trigger('click');
            cloneObj.find('[value="Used"]').trigger('click');



            //call the necessary callback which is registerd in the corresponding page js
            var callbackObj = new clonerCallBack();
            var fn = parentObj.attr('data-clone');
            if (typeof callbackObj[fn] == 'function')
                callbackObj[fn](cloneObj);
            // cloneObj.find('input[type="radio"],input[type="checkbox"],select').trigger('change');
            // cloneObj.find('.error,.success').removeClass('success error');

            toggleClonerDisplay(parentObj);
            cloneObj.find("#ED_Category_Others_DIV").addClass("hide");

            console.log(cloneObj);
            var showOtherBox = function() {
                if ($(this).val() == "Others") {
                    //console.log($(this).parents("[data-cloneitem=fd_fixeddeposit]").find("#ED_Category_Others_DIV"));
                    $(this).parents("[data-cloneitem=fd_fixeddeposit]").find(".otherDivClass").removeClass("hide");
                } else {
                    $(this).parents("[data-cloneitem=fd_fixeddeposit]").find(".otherDivClass").addClass("hide");
                }
            };
            $("select[name=ED_Category]").on("change", showOtherBox);


            firstPropertyTypeRadio.trigger('click');
            firstEquipmentDetailTypeRadio.trigger('click');
            firstVehicleDetailTypeRadio.trigger('click');
            firstFixedDepositTypeRadio.trigger('click');

            //
            var DBSvalidationObj = new DBSvalidation();
            DBSvalidationObj.init();

            e.preventDefault();
        });

        $('.form-horizontal').undelegate('[data-clone] .cloneremover', 'click').delegate('[data-clone] .cloneremover', 'click', function(e) {
            var parentObj = $(this).parents('[data-clone]'); //clone parent
            var cloneObj = $(this).parents('[data-cloneitem]').remove(); //clone obj
            toggleClonerDisplay(parentObj);

            e.preventDefault();
        });
    };

    var cleanCloneObj = function(cloneObj) {
        OLASClear(cloneObj);
        //FIX:in order to unchecking of inputbox or checkbox
        cloneObj.find('input[type="radio"],input[type="checkbox"]').each(function() {
            splitArr = $(this).attr('name').split('_');
            $(this).attr('name', splitArr[0]);
        });
        OLASplaceholderModernizr(cloneObj);

        //add listener for price formate
        cloneObj.find('input[data-dbsvalidate="price"]').priceFormat({
            prefix: '',
            centsSeparator: '',
            centsLimit: -1,
            clearPrefix: true,
            clearSufix: true
        });

        cloneObj.find('input[data-dbsvalidate="pricedecimal"]').priceFormat({
            prefix: '',
            centsLimit: 2,
            clearPrefix: true,
            clearSufix: true
        });
    };

    //show the adder and remove linker as reqd
    var toggleClonerDisplay = function(parentObj) {
        var cloneName = parentObj.attr('data-clone');
        var maxCount = parentObj.attr('data-clonemax'); //get the maximum value
        var cloneItems = parentObj.find('[data-cloneitem="' + cloneName + '"]');
        parentObj.find('.cloneadder,.cloneadderParent').addClass('hide'); //hide the adder and adder parent
        //if length not reaches the maximum show the adder and adder parent
        if (parentObj.find('[data-cloneitem="' + cloneName + '"]').length < maxCount) {
            if (cloneItems.find('.cloneadder').length > 0)
                cloneItems.last().find('.cloneadder,.cloneadderParent').removeClass('hide');
            else
                parentObj.find('.cloneadder,.cloneadderParent').removeClass('hide');
        }

        cloneItems.find('.cloneremover').removeClass('hide'); //show the remover
        if (parentObj.attr('data-clonemin') != 0) //if the minimum is non zero hide the remover for first one
            cloneItems.first().find('.cloneremover').addClass('hide');
        iterateCloner(parentObj);
    };

    //iterate and set the ids and name for the form elements appropriately 
    var iterateCloner = function(parentObj) {
        var cloneName = parentObj.attr('data-clone')
        var eleId = 1;
        var splitArr = new Array();
        parentObj.find('[data-cloneitem="' + cloneName + '"]').each(function() {
            $(this).find('input[id],select[id]').each(function() {
                var id = $(this).attr('id');
                var newId = generateBeanCompatibleName(id, eleId);
                if (newId == null) {
                    splitArr = $(this).attr('id').split('_');
                    newId = splitArr[0] + '_' + eleId;
                }
                $(this).attr('id', newId);

                //if(!$(this).hasAttr('name')) return;
                if ($(this).is(':radio') || $(this).is(':checkbox')) {
                    var name = $(this).attr('name');
                    var sectionName = $(this).attr('data-sectionName');
                    console.log(sectionName);
                    var newId = generateBeanCompatibleName(name, eleId, sectionName);
                    if (newId == null) {
                        splitArr = $(this).attr('name').split('_');
                        newId = splitArr[0] + '_' + '[' + eleId + ']';
                    }
                    $(this).attr('name', newId);
                }
            });
            $(this).find('label[for]').each(function() {
                var forx = $(this).attr('for');
                var newId = generateBeanCompatibleName(forx, eleId);
                if (newId == null) {
                    splitArr = $(this).attr('for').split('_');
                    newId = splitArr[0] + '_' + eleId;
                }
                $(this).attr('for', newId);
            });
            eleId++;

        });
    };
});

var generateBeanCompatibleName = function(id, eleId, sectionName) {

    var specialChar = 'C';
    if (typeof id == "undefined")
        return null;
    var dotCount = (id.match(/\./g) || []).length;
    var newId = null;

    if (sectionName) {
        var splits = id.split(".");
        newId = splits[0] + '.' + sectionName + '[' + (eleId - 1) + ']' + '.' + splits[2];
        console.log(eleId);
        return newId;
    }

    if (dotCount >= 2) {
        newId = '';
        var splits = id.split(".");
        for (var i = 0; i < splits.length - 2; i++) {
            newId += splits[i] + '.';
        }
        newId += (specialChar + eleId) + '.';
        newId += splits[splits.length - 1];
        return newId;
    }
    return newId;
}

var clonerCallBack = (function() {

});