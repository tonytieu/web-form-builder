var pattern = /(\d{2})\/(\d{2})\/(\d{4})/;

var picker = null;
$(document).ready(function() {

    var now = new Date();
    for (var i = 0; i < 5; i++) {
        var year = now.getFullYear() - i;
        $("#latestyear").append('<option value="' + year + '">' + year + '</option>');
    }

    var businessDetailObj = new businessDetail();
    businessDetailObj.init();

    $("input").on("focusout", enableNextBtn);
    $("select").on("click", enableNextBtn);

    if (navigator.userAgent.match(/iPad|iPhone|iPod/i)) {
        if ($(window).width() >= 700 && $(window).width() < 1024) {
            console.log($(window).width());
            $(".select-wrapper").attr("style", "width:91% !important");
        }
        if ($(window).width() >= 700 && $(window).width() <= 1024) {
            console.log($(window).width());
            $("#modalWindow_companyBorrow .select-wrapper,#modalWindow_shareholder .select-wrapper")
                .attr("style", "width:60% !important");
        }
    }


    // $("#incorporation").on("change", function(){
    //     console.log($(this).val());
    //     var dt = new Date($(this).val().replace(pattern,'$3-$2-$1'));
    //     var now = new Date();
    //     if(now >= dt) {
    //         $("#incorporation ~ p").addClass("hide");
    //         $("#incorporation ~ p").html("Please check your entry");
    //         $("#incorporation ~ p").css("display", "none");
    //     } else {
    //         $("#incorporation ~ p").removeClass("hide");
    //         $("#incorporation ~ p").html("Can't choose future date");
    //         $("#incorporation ~ p").css("display", "block");
    //         $("#incorporation").val(now.getDate() + "/" + (now.getMonth() + 1) + "/" + now.getFullYear());
    //         $("#incorporation").trigger("change");
    //     }
    // });

    // $("#incorporation").val(now.getDate() + "/" + (now.getMonth() + 1) + "/" + now.getFullYear());

    // $("#facilityLoanType0").on("change", onFacilityChanged);

    // $("#collateralLoanType0").on("change", onCollateralChanged);
});

function onFacilityChanged(ele) {
    var val = $(ele).val();
    var others = "Others";
    if (val.substring(0, others.length) === others) {
        $(ele).parent().siblings(".faclityOther").removeClass("hide");
    } else {
        $(ele).parent().siblings(".faclityOther").addClass("hide");
        $(ele).parent().siblings(".faclityOther").val("nothing");
    }
}

function onCollateralChanged(ele) {
    var val = $(ele).val();
    var others = "Others";
    if (val.substring(0, others.length) === others) {
        $(ele).parent().siblings(".collateralOther").removeClass("hide");

    } else {
        $(ele).parent().siblings(".collateralOther").addClass("hide");
        $(ele).parent().siblings(".collateralOther").val("nothing");
    }
}

function enableNextBtn() {
    // if ($("select.success").length >= 1 && $("input.success").length >= 8 &&
    //     $("select.error").length == 0 && $("input.error").length <= 1) {
    //     $(".next-btn").removeAttr("disabled");
    // } else {
    //     $(".next-btn").attr("disabled", "disabled");
    // }

    // setTimeout(function() {
    //     if ($("select.success").length >= 1 && $("input.success").length >= 8 &&
    //         $("select.error").length == 0 && $("input.error").length <= 1) {
    //         $(".next-btn").removeAttr("disabled");
    //     } else {
    //         $(".next-btn").attr("disabled", "disabled");
    //     }
    // }, 500);
}


function cloneFacilityDiv(ele) {
    //$('#companyBorrow-AddOn-sample .facility').filter(function(){return $(this).attr('data-index') > 0}).remove();



    var cloned = $(facilityCloned).clone();
    var showCloned = $(facilityShowCloned).clone();
    var lastDiv = $(".facility_div:last");
    var lastDivIndex = $(lastDiv).attr("data-index");

    if (parseInt($(lastDiv).attr("data-index")) >= 4) {
        $(ele).addClass("hide");
        return;
    }

    if (typeof(lastDivIndex) == 'undefined') {
        lastDivIndex = -1;
    }

    var index = parseInt(lastDivIndex) + 1;
    $(cloned).find("input").val("");
    $(cloned).find("select").val("");

    for (var i = 0; i < facilitychangedEles.length; i++) {
        var name = facilitychangedEles[i];
        var currentName = name + '0';
        var newName = name + index;

        //console.log( currentName);
        $(cloned).find(".control-group[data-relate=" + currentName + "]").attr("data-relate", newName);
        $(cloned).find(".control-label[for=" + currentName + "]").attr("for", newName);
        $(cloned).find("#" + currentName).attr("id", newName);

        $(showCloned).find(".control-label[for=" + currentName + "]").attr("for", newName);
        $(showCloned).find(".controls[data-relate=" + currentName + "]").attr("data-relate", newName);
    }
    $(cloned).attr("data-index", index);
    $(showCloned).attr("data-index", index);
    //console.log($(cloned).find(".control-group[data-relate="+"facilityLoanType"+"]").attr("data-relate", "facilityLoanType0"));


    $("#parent_facility_div").append(cloned);
    $("#parent_facility_show_div").append(showCloned);

    $('input[data-dbsvalidate="price"]').priceFormat({
        prefix: '',
        centsSeparator: '',
        centsLimit: -1,
        clearPrefix: true,
        clearSufix: true
    });
}

function removeFacilityDiv(ele) {
    $(ele).parents(".facility_div").remove();
    $('#btnAddFacilityDiv').removeClass("hide");
}

function cloneCollateralDiv(ele) {
    var cloned = $(collateralCloned).clone();
    var showCloned = $(collateralShowCloned).clone();
    var lastDiv = $(".collateral_div:last");
    var lastDivIndex = $(lastDiv).attr("data-index");

    if (parseInt($(lastDiv).attr("data-index")) >= 4) {
        $(ele).addClass("hide");
        return;
    }


    if (typeof(lastDivIndex) == 'undefined') {
        lastDivIndex = -1;
    }
    var index = parseInt(lastDivIndex) + 1;
    $(cloned).find("input").val("");
    $(cloned).find("select").val("");

    for (var i = 0; i < collateralchangedEles.length; i++) {
        var name = collateralchangedEles[i];
        var currentName = name + '0';
        var newName = name + index;

        //console.log( currentName);
        $(cloned).find(".control-group[data-relate=" + currentName + "]").attr("data-relate", newName);
        $(cloned).find(".control-label[for=" + currentName + "]").attr("for", newName);
        $(cloned).find("#" + currentName).attr("id", newName);

        $(showCloned).find(".control-label[for=" + currentName + "]").attr("for", newName);
        $(showCloned).find(".controls[data-relate=" + currentName + "]").attr("data-relate", newName);
    }
    $(cloned).attr("data-index", index);
    $(showCloned).attr("data-index", index);

    //$('.collateral').filter(function(){return $(this).attr('data-index') > 0}).remove();

    console.log(cloned);
    $("#parent_collateral_div").append(cloned);
    $("#parent_collateral_show_div").append(showCloned);

    $('input[data-dbsvalidate="price"]').priceFormat({
        prefix: '',
        centsSeparator: '',
        centsLimit: -1,
        clearPrefix: true,
        clearSufix: true
    });
}

function removeCollateralDiv(ele) {
    $(ele).parents(".collateral_div").remove();
    $('#btnAddCollateralDiv').removeClass("hide");
}

function onCompanyTypeSelect(ele) {
    //console.log("chage");
    var val = $(ele).val();
    $("#br").attr("placeholder", val).trigger("focusout");
    $("#ci").trigger("focusout");

    if (val == "Limited Company") {
        $("#dateOfIncorporationDiv").removeClass("hide");
    } else {
        $("#dateOfIncorporationDiv").addClass("hide");
    }

    if (val == "Sole Proprietorship" || val == "Partnership") {
        $("#dateOfBusinessCommencedDiv").removeClass("hide");
    } else {
        $("#dateOfBusinessCommencedDiv").addClass("hide");
    }
}

var businessDetail = (function() {
    var popupEditorObj = new popupEditor();
    this.init = function() {
        var cloneObj = new cloner();
        cloneObj.init();
        //mailing address toggling
        $('#mailingAddress').change(function() {
            toggleAddressBlock();
        });
        toggleAddressBlock();

        //others text box toggling in mainOperatingAccount 
        $('#mainOperatingAccount').change(function() {
            toggleMainOperatingBlock();
        });
        toggleMainOperatingBlock();

        //popup for addCompanyBorrowings
        $('#addCompanyBorrowings').click(function(e) {
            //console.log("addCompanyBorroings");
            var popUpObj = $('#modalWindow_companyBorrow');
            popUpObj.find('.modal-footer button:not(#adduserSave)').removeClass('hide');
            popupEditorObj.setPopUpObj(popUpObj);
            //popupEditorObj.setIndex('companyBorrow');
            popupEditorObj.setIndex('olafBean.businessDetailsHk.companyBorrowings');

            popupEditorObj.show();

            $(".faclityOther").addClass('hide');
            $(".collateralOther").addClass('hide');

            $(popUpObj).undelegate('.btn-panel-close', 'click').delegate('.btn-panel-close', 'click', function() {
                popupEditorObj.hide();
                resetBorrowingPopup();
            });

            $(popUpObj).undelegate('.modal-footer button', 'click').delegate('.modal-footer button', 'click', function(e) {
                if (!validateBorrowingPopUp()) { return; }



                var cntObj = $('#companyBorrow-AddOn-sample').clone();

                //
                //console.log($(this).parents("#modalWindow_companyBorrow").find(".otherDIV").hasClass("hide"));
                var otherDivIfVisible = $(this).parents("#modalWindow_companyBorrow").find(".otherDIV");
                if (!otherDivIfVisible.hasClass("hide")) {
                    console.log(otherDivIfVisible.find("#otherInstitutionInputBox").val());
                    cntObj.attr("data-otherBankName", otherDivIfVisible.find("#otherInstitutionInputBox").val())
                    cntObj.attr("data-otherBankCode", otherDivIfVisible.find("#otherInstitutionBankCodeInputBox").val())
                }
                //console.log("bank add");
                cntObj.attr("data-bankCode", $(this).parents("#modalWindow_companyBorrow").find("#finInstitution").val());
                cntObj.attr("data-bankName", $(this).parents("#modalWindow_companyBorrow").find("#finInstitution option:selected").text());

                $.each($(this).parents("#modalWindow_companyBorrow").find(".facility_div"), function() {

                    //console.log($(this).attr("data-index"));
                    // console.log(cntObj.find(".facility[data-index="+ $(this).attr("data-index") +"]"));
                    // console.log($(this).find("select option:selected").val());
                    cntObj.find(".facility[data-index=" + $(this).attr("data-index") + "]").attr("data-facilitycode", $(this).find("select option:selected").val());
                    //console.log($(this).find("select option:selected").val());
                });

                $.each($(this).parents("#modalWindow_companyBorrow").find(".collateral_div"), function() {
                    cntObj.find(".collateral[data-index=" + $(this).attr("data-index") + "]").attr("data-collateralcode", $(this).find("select option:selected").val());
                });

                //console.log($(this).parents("#modalWindow_companyBorrow").find(".facility_div"));

                var borrowingIndexArr = $("div[data-borrowingindex]");
                var newBorrowingIndex = parseInt($(borrowingIndexArr[borrowingIndexArr.length - 1]).attr("data-borrowingindex")) + 1;
                cntObj.attr("data-borrowingindex", newBorrowingIndex);

                // remove duplicate divs
                var lastDiv = $(".facility_div:last");
                var lastDivIndex = $(lastDiv).attr("data-index");
                for (var i = 0; i <= lastDivIndex; i++) {
                    //console.log(i);
                    var divs = cntObj.find(".facility[data-index=" + i + "]");
                    if (divs.length > 0) {
                        divs.eq(1).remove();
                        //console.log("dupliate");
                    }
                }

                var lastDiv = $(".collateral_div:last");
                var lastDivIndex = $(lastDiv).attr("data-index");
                for (var i = 0; i <= lastDivIndex; i++) {
                    //console.log(i);
                    var divs = cntObj.find(".collateral[data-index=" + i + "]");
                    if (divs.length > 0) {
                        divs.eq(1).remove();
                        //console.log("dupliate");
                    }
                }

                //console.log(cntObj);


                cntObj = popupEditorObj.addCnt(cntObj);
                $('#companyBorrow-AddOn').append(cntObj);
                if ($(this).attr('id') == 'adduserSave') {
                    popupEditorObj.hide();
                }
                resetBorrowingPopup();
            });
        });
        resetBorrowingPopup();



        $('#addshareholder').click(function(e) {
            resetshareholderPopup();

            var popUpObj = $('#modalWindow_shareholder');
            popUpObj.find('.modal-footer button:not(#adduserSave)').removeClass('hide');
            popupEditorObj.setPopUpObj(popUpObj);
            popupEditorObj.setIndex('shareholding');
            popupEditorObj.show();

            $(popUpObj).undelegate('.btn-panel-close', 'click').delegate('.btn-panel-close', 'click', function() {
                popupEditorObj.hide();
                resetshareholderPopup();
            });

            $(popUpObj).undelegate('.modal-footer button', 'click').delegate('.modal-footer button', 'click', function(e) {
                console.log(validateshareholderPopUp());
                if (!validateshareholderPopUp()) { return; }

                var cntObj = $('#shareholder-AddOn-sample').clone();

                var shareholdingIndexArr = $("div[data-shareholdingindex]");
                var newshareholdingIndex = parseInt($(shareholdingIndexArr[shareholdingIndexArr.length - 1]).attr("data-shareholdingindex")) + 1;

                cntObj.attr("data-shareholdingindex", newshareholdingIndex);

                var positionCode = $("#position").val();

                cntObj.attr("data-positionCode", positionCode);

                //console.log("positionCode " + positionCode);

                cntObj = popupEditorObj.addCnt(cntObj);
                //console.log(cntObj);
                $('#shareholder-AddOn').append(cntObj);

                console.log($(this).attr('id'));
                if ($(this).attr('id') == 'adduserSave') {
                    popupEditorObj.hide();
                }

                resetshareholderPopup();

                if ($(".shareholder-AddOn").filter(function() { return $(this).attr("data-shareholdingindex") >= 0 }).length == 5) {
                    $("#addshareholder").hide();
                }
                console.log(newshareholdingIndex);
            });
        });
        resetshareholderPopup();

        //listeners for borrowing popup
        $('#finInstitution').change(function() {
            toggleBorrowingBankBlock();
        });
        $('#LoanType').change(function() {
            toggleBorrowingLoanBlock();
        });

        //toggle for none selector in Security Provided
        $('.loanTypeHld .control-group[data-relate="security"] input[type="checkbox"]').change(function() {
            if ($(this).hasClass('chkBoxNone')) {
                if ($(this).is(':checked')) {
                    $(this).parents('.controls').find('input[type="checkbox"]:not(.chkBoxNone)').attr('checked', false);
                }
            } else
                $(this).parents('.controls').find('input[type="checkbox"].chkBoxNone').attr('checked', false);
        });

        //popup edit for addPersonalBorrowings
        $('#companyBorrow-AddOn').delegate('.edit', 'click', function(e) {
            var popUpObj = $('#modalWindow_companyBorrow');
            popUpObj.find('.modal-footer button:not(#adduserSave)').addClass('hide');
            var editObj = $(this).parents('.companyBorrow-AddOn');

            var facilities = editObj.find(".facility");
            var collaterals = editObj.find(".collateral");


            var emptyFacilities = facilities.filter(function() { return $(this).children().length == 0; });
            emptyFacilities.remove();

            var emptyCollateralss = collaterals.filter(function() { return $(this).children().length == 0; });
            emptyCollateralss.remove();

            popUpObj.find('.facility_div').filter(function() { return $(this).attr('data-index') > 0 }).remove();
            for (var i = 0; i < facilities.length - 1; i++) {
                popUpObj.find('#btnAddFacilityDiv').trigger('click');
            }


            popUpObj.find('.collateral_div').filter(function() { return $(this).attr('data-index') > 0 }).remove();
            for (var i = 0; i < collaterals.length - 1; i++) {
                popUpObj.find('#btnAddCollateralDiv').trigger('click');
            }

            popupEditorObj.setPopUpObj(popUpObj);
            popupEditorObj.setIndex('companyBorrow');
            popupEditorObj.setEditCallBack(function() {
                toggleBorrowingBankBlock();
                toggleBorrowingLoanBlock();
            });
            popupEditorObj.show();
            popupEditorObj.setEditObj(editObj);
            popupEditorObj.editCnt();
            //validateBorrowingPopUp();

            $(popUpObj).undelegate('.btn-panel-close', 'click').delegate('.btn-panel-close', 'click', function() {
                popupEditorObj.hide();
                resetBorrowingPopup();
            });

            $(popUpObj).undelegate('.modal-footer button', 'click').delegate('.modal-footer button', 'click', function(e) {
                if (!validateBorrowingPopUp()) { return; }


                // get unique
                //var sampleFacilities = $('#companyBorrow-AddOn-sample .facility');
                //sampleFacilities.filter(function(){return $(this).attr('data-index') > 0}).remove();
                for (var i = 0; i < facilities.length; i++) {
                    //$(sampleFacilities[sampleFacilities.length - 1 - i]).remove();
                    do {
                        var deletedFacilities = $('#companyBorrow-AddOn-sample .facility').filter(function() { return $(this).attr("data-index") == $(facilities[i]).attr("data-index") });

                        if (deletedFacilities.length > 1) {
                            deletedFacilities.last().remove();
                        }
                    } while (deletedFacilities.length > 1);
                }

                //var sampleCollaterals = $('#companyBorrow-AddOn-sample .collateral');
                //sampleFacilities.filter(function(){return $(this).attr('data-index') > 0}).remove();
                for (var i = 0; i < collaterals.length; i++) {
                    //$(sampleFacilities[sampleFacilities.length - 1 - i]).remove();
                    do {
                        var deletedCollaterals = $('#companyBorrow-AddOn-sample .collateral').filter(function() { return $(this).attr("data-index") == $(collaterals[i]).attr("data-index") });

                        if (deletedCollaterals.length > 1) {
                            deletedCollaterals.last().remove();
                        }
                    } while (deletedCollaterals.length > 1);
                }

                var cntObj = $('#companyBorrow-AddOn-sample').clone();

                //console.log($(this).parents("#modalWindow_companyBorrow").find(".otherDIV").hasClass("hide"));
                var otherDivIfVisible = $(this).parents("#modalWindow_companyBorrow").find(".otherDIV");
                if (!otherDivIfVisible.hasClass("hide")) {
                    //console.log(otherDivIfVisible.find("#otherInstitutionInputBox").val());
                    cntObj.attr("data-otherBankName", otherDivIfVisible.find("#otherInstitutionInputBox").val())
                    cntObj.attr("data-otherBankCode", otherDivIfVisible.find("#otherInstitutionBankCodeInputBox").val())
                }

                //console.log("bank edit");
                cntObj.attr("data-bankCode", $(this).parents("#modalWindow_companyBorrow").find("#finInstitution").val());
                cntObj.attr("data-bankName", $(this).parents("#modalWindow_companyBorrow").find("#finInstitution option:selected").text());

                $.each($(this).parents("#modalWindow_companyBorrow").find(".facility_div"), function() {
                    cntObj.find(".facility[data-index=" + $(this).attr("data-index") + "]").attr("data-facilitycode", $(this).find("select option:selected").val());
                });

                $.each($(this).parents("#modalWindow_companyBorrow").find(".collateral_div"), function() {
                    cntObj.find(".collateral[data-index=" + $(this).attr("data-index") + "]").attr("data-collateralcode", $(this).find("select option:selected").val());
                });

                cntObj.attr("data-borrowingindex", editObj.attr("data-borrowingindex"));

                cntObj = popupEditorObj.addCnt(cntObj);
                editObj.replaceWith(cntObj);
                if ($(this).attr('id') == 'adduserSave') {
                    popupEditorObj.hide();
                }
                resetBorrowingPopup();
            });
        });

        $('#shareholder-AddOn').delegate('.edit', 'click', function(e) {
            //console.log("edit");
            var popUpObj = $('#modalWindow_shareholder');
            popUpObj.find('.modal-footer button:not(#adduserSave)').addClass('hide');
            var editObj = $(this).parents('.shareholder-AddOn');
            popupEditorObj.setPopUpObj(popUpObj);
            popupEditorObj.setIndex('companyBorrow');
            popupEditorObj.setEditCallBack(function() {
                toggleBorrowingBankBlock();
                toggleBorrowingLoanBlock();
            });
            popupEditorObj.show();
            popupEditorObj.setEditObj(editObj);
            popupEditorObj.editCnt();
            validateBorrowingPopUp();

            $(popUpObj).undelegate('.btn-panel-close', 'click').delegate('.btn-panel-close', 'click', function() {
                popupEditorObj.hide();
                resetBorrowingPopup();
            });

            $(popUpObj).undelegate('.modal-footer button', 'click').delegate('.modal-footer button', 'click', function(e) {
                if (!validateBorrowingPopUp()) { return; }
                var cntObj = $('#shareholder-AddOn-sample').clone();
                cntObj.attr("data-shareholdingindex", editObj.attr("data-shareholdingindex"));

                var positionCode = $("#position").val();
                cntObj.attr("data-positionCode", positionCode);

                cntObj = popupEditorObj.addCnt(cntObj);

                editObj.replaceWith(cntObj);

                if ($(this).attr('id') == 'adduserSave') {
                    popupEditorObj.hide();
                    //console.log("adduserSave");
                }
                resetBorrowingPopup();


            });
        });





        //delete personalborrowings
        $('#companyBorrow-AddOn').delegate('.delete', 'click', function(e) {
            var popUpObj = $('#modalWindow_Delete');
            var deleteObj = $(this).parents('.companyBorrow-AddOn');
            popupEditorObj.setPopUpObj(popUpObj);
            popupEditorObj.show();

            $(popUpObj).undelegate('.btn-panel-close', 'click').delegate('.btn-panel-close', 'click', function() {
                popupEditorObj.hide();
            });

            $(popUpObj).undelegate('.modal-footer button', 'click').delegate('.modal-footer button', 'click', function(e) {
                popupEditorObj.hide();
                if ($(this).attr('id') == 'deleteCompanyBorrow') {
                    deleteObj.remove();
                    toggleBorrowingBtn();

                    // reset shareholding div index
                    var currentcompanyBorrowAddonDivs = $(".companyBorrow-AddOn").not(".hide");
                    $.each(currentcompanyBorrowAddonDivs, function(i) {
                        var ele = currentcompanyBorrowAddonDivs[i];

                        $(ele).attr("data-borrowingindex", i);

                        var hiddenInputs = $(ele).find("input[type=hidden]");


                        $.each(hiddenInputs, function(j) {
                            var inputEle = hiddenInputs[j];
                            var relatedName = $(inputEle).attr("data-relatedname");

                            $(inputEle).attr("name", "companyBorrow[" + relatedName + "][" + i + "]");
                        });

                        console.log("done reseting companyBorrow divs");
                    });
                }
            });
        });

        $('#shareholder-AddOn').delegate('.delete', 'click', function(e) {
            var popUpObj = $('#modalWindow_Delete');
            var deleteObj = $(this).parents('.shareholder-AddOn');
            popupEditorObj.setPopUpObj(popUpObj);
            popupEditorObj.show();

            $(popUpObj).undelegate('.btn-panel-close', 'click').delegate('.btn-panel-close', 'click', function() {
                popupEditorObj.hide();
            });

            $(popUpObj).undelegate('.modal-footer button', 'click').delegate('.modal-footer button', 'click', function(e) {
                popupEditorObj.hide();
                if ($(this).attr('id') == 'deleteCompanyBorrow') {
                    deleteObj.remove();
                    toggleBorrowingBtn();

                    // reset shareholding div index
                    var currentShareHolderAddonDivs = $(".shareholder-AddOn").not(".hide");
                    $.each(currentShareHolderAddonDivs, function(i) {
                        var ele = currentShareHolderAddonDivs[i];

                        $(ele).attr("data-shareholdingindex", i);

                        var hiddenInputs = $(ele).find("input[type=hidden]");


                        $.each(hiddenInputs, function(j) {
                            var inputEle = hiddenInputs[j];
                            var relatedName = $(inputEle).attr("data-relatedname");

                            $(inputEle).attr("name", "shareholding[" + relatedName + "][" + i + "]");
                        });

                        //console.log("done reseting shareholder divs");
                    });

                    $("#addshareholder").show();

                }
            });


        });

        //share holding popup
        $('#shareholding').delegate('table .shareHoldType select', 'change', function() {
            toggleShareHldBlock($(this));
            if ($(this).val() != 'Company') { return; }
            var rowObj = $(this).parents('tr');
            var popUpObj = $('#modalWindow_shareholding');
            popUpObj.find('.modal-header h3 .name').html(rowObj.find('td .name').html()); //set the name for the popup by shareholder name
            popupEditorObj.setPopUpObj(popUpObj);
            popupEditorObj.show();

            popUpObj.find('.shareHoldType select').each(function() {
                toggleSharePopUpBlock($(this));
            });

            $(popUpObj).undelegate('.btn-panel-close', 'click').delegate('.btn-panel-close', 'click', function() {
                popupEditorObj.hide();
                resetShareHldPopup();
            });

            $(popUpObj).undelegate('.modal-footer button', 'click').delegate('.modal-footer button', 'click', function(e) {
                console.log("adduserSave");
                if (!validateShareHldPopUp()) { return; }
                appendShareHldData(rowObj); //append the user added data in the post method format
                popupEditorObj.hide();
                resetShareHldPopup();
            });
        });

        //shareholding edit popup
        $('#shareholding').delegate('table .shareHldPopLink', 'click', function() {
            var rowObj = $(this).parents('tr');
            var popUpObj = $('#modalWindow_shareholding');
            var uniqueName = rowObj.find('td .name').data('name');
            popUpObj.find('.modal-header h3 .name').html(rowObj.find('td .name').html()); //set the name for the popup by shareholder name
            var existingCount = rowObj.find('.shareHld .shareUnit').length;
            var remainingCount = existingCount - popUpObj.find('tbody tr').length;
            for (var ele = 1; ele <= remainingCount; ele++) //add the rows based on the existing records
                popUpObj.find('tbody tr:first .cloneadder').trigger('click');

            var ele = 0;
            var name;
            var obj;
            rowObj.find('.shareHld .shareUnit').each(function() { //add the existing records to the rows
                $(this).find('input').each(function() {
                    name = $(this).attr('name');
                    name = name.substring(0, name.indexOf('['));
                    obj = popUpObj.find('tbody tr:eq(' + ele + ') [name="' + name + '[]"]');
                    obj.val($(this).val());
                    if (obj.is('select')) {
                        obj.siblings('.holder').html(obj.val()); //adding the html for span holder
                    }
                });
                ele++;
            });

            popupEditorObj.setPopUpObj(popUpObj);
            popupEditorObj.show();

            popUpObj.find('.shareHoldType select').each(function() { //hide or show the respective blocks in each rows of the popup
                toggleSharePopUpBlock($(this));
            });

            validateShareHldPopUp();

            $(popUpObj).undelegate('.btn-panel-close', 'click').delegate('.btn-panel-close', 'click', function() {
                popupEditorObj.hide();
                resetShareHldPopup();
            });

            $(popUpObj).undelegate('.modal-footer button', 'click').delegate('.modal-footer button', 'click', function(e) {
                if (!validateShareHldPopUp()) { return; }
                appendShareHldData(rowObj);
                popupEditorObj.hide();
                resetShareHldPopup();
            });
        });

        $('#shareholding table .shareHoldType select').each(function() { //hide or show the block in the shareholding table
            toggleShareHldBlock($(this));
        });
        //listener to hide or show the block in the share holding popup block
        $('#modalWindow_shareholding').delegate('table .shareHoldType select', 'change', function(e) {
            if ($('#modalWindow_shareholding .shareHoldType select option:selected[data-unique]').length > 1) {
                var siblingObj = $(this).find('option[data-unique]').siblings('option');
                $(this).val(siblingObj.val());
                $(this).siblings('.holder').html(siblingObj.html());
            }
            toggleSharePopUpBlock($(this));
        });

        //validation listeners for share hold popup
        $('#modalWindow_shareholding').delegate('tr input[data-dbsvalidate]', 'focusout', function() {
            var DBSvalidationObj = new DBSvalidation();
            var validationObj = new validation();
            if (DBSvalidationObj.validateObj(validationObj, $(this), true))
                $(this).addClass('success').removeClass('error');
            else
                $(this).addClass('error').removeClass('success');

        });
        $('#modalWindow_shareholding').delegate('tr select[data-dbsvalidate]', 'change', function() {
            var DBSvalidationObj = new DBSvalidation();
            var validationObj = new validation();
            if (DBSvalidationObj.validateObj(validationObj, $(this), true)) {
                $(this).addClass('success').removeClass('error');
                $(this).parent('.select-wrapper').addClass('success').removeClass('error');
            } else {
                $(this).addClass('error').removeClass('success');
                $(this).parent('.select-wrapper').addClass('error').removeClass('success');
            }
        });

        //validation
        $('.form-horizontal .buttonHolder button').click(function() {
            var href = $(this).attr('href');

            if (!$("#incorporation ~ p").hasClass("hide"))
                return;

            if ($(this).hasClass('prev-btn')) OLASSubmitForm($('.form-horizontal'), href);

            if (validateBusinessDetails($(this))) {
                OLASSubmitForm($('.form-horizontal'), href);
            } else {
                OLASErrorForm($('.form-horizontal'));
            }
        });

        var DBSvalidationObj = new DBSvalidation();
        DBSvalidationObj.init();
    };

    //show or hide the mailing addressblock
    var toggleAddressBlock = function() {
        if ($('#mailingAddress').is(':checked')) {
            $('#mailingAddressCnt').slideUp('slow', function() {
                $('#mailingAddressCnt .control-group').addClass('hide');
                OLASClear($('#mailingAddressCnt'));
            });
        } else {
            $('#mailingAddressCnt .control-group').removeClass('hide');
            $('#mailingAddressCnt').slideDown('slow');
        }
    };

    //show or hide the main operating block
    var toggleMainOperatingBlock = function() {
        if ($('#mainOperatingAccount').val() == 'Others') {
            $('.control-group.otherDIV').removeClass('hide');
        } else {
            $('.control-group.otherDIV').addClass('hide');
            OLASClear($('.control-group.otherDIV'));
        }
    };

    //toggle popUpBank block
    var toggleBorrowingBankBlock = function() {
        if ($('#finInstitution').val() == 'Others') {
            $('#finInstitution').parents('.control-group').siblings('.otherDIV').removeClass('hide');
        } else {
            $('#finInstitution').parents('.control-group').siblings('.otherDIV').addClass('hide');
            OLASClear($('#finInstitution').parents('.control-group').siblings('.otherDIV'));
        }
    };

    //toggle popUpLoan block
    var toggleBorrowingLoanBlock = function() {
        var val = $('#LoanType').val();
        $('#modalWindow_companyBorrow .loanTypeHld').addClass('hide');
        $('#' + val + '_DIV').removeClass('hide');
        try {
            OLASClear($('#modalWindow_companyBorrow .loanTypeHld:not(#' + val + '_DIV'));
        } catch (e) {}
    };

    //reset the borrowing popup
    var resetBorrowingPopup = function() {
        OLASClear($('#modalWindow_companyBorrow'));
        toggleBorrowingBankBlock();
        toggleBorrowingLoanBlock();
        toggleBorrowingBtn();
    };


    var resetshareholderPopup = function() {
        OLASClear($('#modalWindow_shareholder'));


    };
    //hide or show the add more button in borrowings poup
    var toggleBorrowingBtn = function() {
            if ($('.companyBorrow-AddOn:not(#companyBorrow-AddOn-sample)').length >= 5) {
                $('#addCompanyBorrowings').addClass('hide');
            } else {
                $('#addCompanyBorrowings').removeClass('hide');
            }
        }
        //hide or show the blocks in the share holding table
    var toggleShareHldBlock = function(obj) {
        var val = obj.val();
        var dataObj = obj.parents('tr').find('.shareHoldStaff');
        dataObj.children().addClass('hide');
        if (val == 'Company') {
            dataObj.find('input').removeClass('hide');
        } else {
            dataObj.find('p').removeClass('hide');
            obj.parents('tr').find('.shareHldPopLink').addClass('hide');
        }
    };

    //hide or show the blocks in the share holding popup
    var toggleSharePopUpBlock = function(obj) {
        var val = obj.val();
        var rowObj = obj.parents('tr');
        rowObj.find('.shareHldStaff,.shareHldNationality').find('p,input,select,span').addClass('hide');
        if (val == 'Company') {
            rowObj.find('.shareHldStaff input').removeClass('hide');
            rowObj.find('.shareHldNationality p').removeClass('hide');
        } else if (val == 'Individual') {
            rowObj.find('.shareHldStaff p').removeClass('hide');
            rowObj.find('.shareHldNationality select,.shareHldNationality span').removeClass('hide');
        } else
            rowObj.find('.shareHldStaff p,.shareHldNationality p').removeClass('hide');
    };

    //resetting share hlding popup contents
    var resetShareHldPopup = function() {
        $('#modalWindow_shareholding tbody tr:not(:first)').remove();
        OLASClear($('#modalWindow_shareholding'));
    };

    //appending corresponding data to share hold
    var appendShareHldData = function(rowObj) {
        console.log(rowObj);
        //collect the data
        var html = '';
        var name;
        var val;
        var uniqueName = rowObj.find('td .name').data('name');
        var id = 0;
        $('#modalWindow_shareholding tbody tr').each(function() {
            html += '<span class="shareUnit">';
            $(this).find('input:visible,select:visible').each(function() {
                name = $(this).attr('name').replace("[]", "");

                val = $(this).val();
                html += '<input type="hidden" name="' + name + '[' + uniqueName + '][' + id + ']" value="' + val + '"/>'; //collect the user datas for server
            });
            html += '</span>';
            id++;
        });

        html = '<span class="shareHld hide">' + html + '</span>';
        rowObj.find('.shareHld').remove();
        rowObj.find('td:first').append(html);

        rowObj.find('.shareHldPopLink').removeClass('hide');
    };

    //validating shareHld popup
    var validateShareHldPopUp = function() {
        var validationObj = new validation();
        var DBSvalidationObj = new DBSvalidation();
        console.log("asdf");
        //validation input boxes
        $('#modalWindow_shareholder input[data-dbsvalidate]:visible').each(function() {
            if (DBSvalidationObj.validate($(this), validationObj)) {
                $(this).addClass('success').removeClass('error');
            } else {
                $(this).addClass('error').removeClass('success');
            }
        });

        //validating select boxes
        $('#modalWindow_shareholder select[data-dbsvalidate]:visible').each(function() {
            if (DBSvalidationObj.validate($(this), validationObj)) {
                $(this).addClass('success').removeClass('error');
                $(this).parent('.select-wrapper').addClass('success').removeClass('error');
            } else {
                $(this).addClass('error').removeClass('success');
                $(this).parent('.select-wrapper').addClass('error').removeClass('success');
            }
        });
        return validationObj.isValid();
    };


    //validating borrowing popup
    var validateBorrowingPopUp = function() {
        var validationObj = new validation();
        var DBSvalidationObj = new DBSvalidation();

        DBSvalidationObj.validate($('#modalWindow_companyBorrow'), validationObj);
        return validationObj.isValid();
    };
    //validate the page
    var validateBusinessDetails = function(obj) {
        var validationObj = new validation();

        var DBSvalidationObj = new DBSvalidation();
        if (!obj.hasClass('next-btn')) { DBSvalidationObj.setCompulsory(false); }

        DBSvalidationObj.validate($('.form-horizontal'), validationObj);
        return validationObj.isValid();
    };

    this.wrap_toggleSharePopUpBlock = function(obj) {
        toggleSharePopUpBlock(obj);
    };



    var validateshareholderPopUp = function() {
        var validationObj = new validation();
        var DBSvalidationObj = new DBSvalidation();

        DBSvalidationObj.validate($('#modalWindow_shareholder'), validationObj);
        return validationObj.isValid();
    };


});




//extending the cloner callback and add the callback registers
clonerCallBack.prototype.shareHolder = function(obj) {
    var businessDetailObj = new businessDetail();
    businessDetailObj.wrap_toggleSharePopUpBlock(obj.find('select'));
};




$(document).ready(function() {
    $("#ci-no").hide();
    $("#br-no").hide();

    $("#companytype").change(function() {
        $(this).find("option:selected").each(function() {
            if ($(this).attr("value") == "Partnership") {
                $("#ci-no").hide();
                $("#br-no").show();
            } else if ($(this).attr("value") == "Limited Company") {
                $("#ci-no").show();
                $("#br-no").hide();
            } else if ($(this).attr("value") == "Sole Proprietorship") {
                $("#ci-no").hide();
                $("#br-no").show();
            } else {
                $("#ci-no").hide();
                $("#br-no").hide();
            }
        });
    });
    var now = new Date();

    picker = $('#incorporationCalendar').pickadate({
        format: 'dd/mm/yyyy',
        max: new Date(),
        selectYears: (new Date()).getFullYear() - 1900,
        //selectMonths: true,
        onClose: function() {
            $("#incorporation").val($("#incorporationCalendar").val());
            $("#incorporation").trigger("focusout");
        },
        onStart: function() {

        }
    });

    $('#dateOfBusinessCommencedCalendar').pickadate({
        format: 'dd/mm/yyyy',
        max: new Date(),
        selectYears: (new Date()).getFullYear() - 1900,
        onClose: function() {
            $("#dateOfBusinessCommenced").val($("#dateOfBusinessCommencedCalendar").val());
            $("#dateOfBusinessCommenced").trigger("focusout");
        },
        onStart: function() {

        }
    });
    // pick = picker.pickadate('picker');
    //     pick.on({set: function(thingSet) {
    //     console.log('Set stuff:', thingSet)
    //     }}
    // );
    $("#incorporation").val(now.getDate() + "/" + (now.getMonth() + 1) + "/" + now.getFullYear());
    $("#dateOfBusinessCommenced").val(now.getDate() + "/" + (now.getMonth() + 1) + "/" + now.getFullYear());
    // if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    //     $(".LoanRequirements .control-group .span6").css("line-height", "2.6em");
    //     $(".confirmation-label").css("text-align", "left");
    //     console.log("mobile");
    // }
});