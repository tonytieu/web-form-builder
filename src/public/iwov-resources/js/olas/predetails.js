$(document).ready(function() {
    var preDetailObj = new preDetail();
    preDetailObj.init();

    $('.name-title').on('classChange', function() {

        var isErrorTitle = $("[name=title]").hasClass("error");
        var isErrorName = $("[name=Name]").hasClass("error");

        if (isErrorName && isErrorTitle) {
            $('.bothInvalid').show();

            $('.nameInvalid').hide();
            $('.titleInvalid').hide();

        } else if (isErrorName) {
            $('.nameInvalid').show();

            $('.bothInvalid').hide();
            $('.titleInvalid').hide();

        } else if (isErrorTitle) {
            $('.titleInvalid').show();

            $('.bothInvalid').hide();
            $('.nameInvalid').hide();

        } else {
            $('.bothInvalid').hide();
            $('.nameInvalid').hide();
            $('.titleInvalid').hide();
        }

    });

    $("#countryCode").on("focusout", function() {
        //console.log($(this).val());
        if ($(this).val() == 852 || $(this).val() == 65) {
            $("#officeNumber").attr("data-requiredlength", "8");
            $("#officeNumber").trigger('focusout');
        } else {
            $("#officeNumber").attr("data-requiredlength", "1");
            $("#officeNumber").trigger('focusout');
        }
    });

    // $("#pin").on("focusout", function() {
    //     console.log($(this).val());
    //     if ($(this).val() == 852 || $(this).val() == 65) {
    //         $("#mobileNumber").attr("data-dbsvalidate", "phone");
    //     } else {
    //         $("#mobileNumber").attr("data-dbsvalidate", "number");
    //     }
    // });


    // 	var states = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California',
    //   'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii',
    //   'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana',
    //   'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota',
    //   'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire',
    //   'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota',
    //   'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island',
    //   'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont',
    //   'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'
    // ];

    // 	// constructs the suggestion engine
    // var states = new Bloodhound({
    //   datumTokenizer: Bloodhound.tokenizers.whitespace,
    //   queryTokenizer: Bloodhound.tokenizers.whitespace,
    //   // `states` is an array of state names defined in "The Basics"
    //   local: states
    // });

    // $('#bloodhound .typeahead').typeahead({
    //   hint: true,
    //   highlight: true,
    //   minLength: 1
    // },
    // {
    //   name: 'states',
    //   source: states
    // });

    if (navigator.userAgent.match(/iPad|iPhone|iPod/i)) {
        if ($(window).width() >= 700 && $(window).width() < 1024) {
            console.log($(window).width());
            $("#sales-turn .select-wrapper").attr("style", "width:91% !important");
        }
    }

});

function onOfficeAreaCodeChanged(event) {
    console.log($(event.target).val());
    var val = event.target.value;
    if (val.startsWith("65") || val.startsWith("+65") || val.startsWith("852")) {
        // $("#officeNumber").attr("maxlength", 8);
        // $("#officeNumber").attr("data-requiredlength", 8);
        $("#areaCode").addClass("hide");
        $("#officeNumber").css("width", "72%");
    } else if (val.length > 0) {
        // $("#officeNumber").attr("maxlength", 12);
        $("#areaCode").removeClass("hide");

        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            $("#officeNumber").css("width", "46%");
        } else {
            $("#officeNumber").css("width", "46%");
        }
        // $("#officeNumber").attr("data-requiredlength", "");
    } else {
        $("#areaCode").addClass("hide");
        $("#officeNumber").css("width", "72%");
    }
}

function onMobileCountryCodeChanged(event) {
    console.log($(event.target).val());
    var val = event.target.value;

    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        if (val.startsWith("65") || val.startsWith("+65") || val.startsWith("852")) {
            $("#mobileAreaCode").addClass("hide");
            $("#mobileNumber").css("width", "72%");
        } else if (val.length > 0) {
            $("#mobileAreaCode").removeClass("hide");
            $("#mobileNumber").css("width", "47%");
        } else {
            $("#mobileAreaCode").addClass("hide");
            $("#mobileNumber").css("width", "72%");
        }
    } else {
        if (val.startsWith("65") || val.startsWith("+65") || val.startsWith("852")) {
            $("#mobileAreaCode").addClass("hide");
            $("#mobileNumber").css("width", "62%");
        } else if (val.length > 0) {
            $("#mobileAreaCode").removeClass("hide");
            $("#mobileNumber").css("width", "42%");
        } else {
            $("#mobileAreaCode").addClass("hide");
            $("#mobileNumber").css("width", "62%");
        }
    }


}

function enableNextBtn() {
    // if ($("select.success").length >= 2 && $("input.success").length >= 4 &&
    //     $("select.error").length == 0 && $("input.error").length == 0 && $("#ConfirmCheck").is(":checked")) {
    //     $(".next-btn").removeAttr("disabled");
    // } else {
    //     $(".next-btn").attr("disabled", "disabled");
    // }
}

var preDetail = (function() {
    var popupEditorObj = new popupEditor();
    this.init = function() {
            //$('.form-horizontal .buttonHolder .next-btn').attr('disabled','true');

            $('.form-horizontal input[name="prefContact"]').change(function() {
                toggleMobileBlock();
            });
            toggleMobileBlock();
            //search brn
            $('#searchByBRN').click(function(e) {
                var validationObj = new validation();
                var DBSvalidationObj = new DBSvalidation();

                DBSvalidationObj.validateObj(validationObj, $('#BRNumber'));
                if (!validationObj.isValid()) return;

                var popUpObj = $('#modalWindow_confirmBRN');
                popupEditorObj.setPopUpObj(popUpObj);
                popupEditorObj.show();

                $(popUpObj).undelegate('.btn-panel-close', 'click').delegate('.btn-panel-close', 'click', function() {
                    popupEditorObj.hide();
                });

                $(popUpObj).undelegate('.modal-footer button', 'click').delegate('.modal-footer button', 'click', function(e) {
                    popupEditorObj.hide();
                    if ($(this).attr('id') == 'adduserSave')
                        $('.form-horizontal .buttonHolder .next-btn').removeAttr('disabled');
                    else {
                        $('.form-horizontal .buttonHolder .next-btn').attr('disabled', 'true');
                        OLASClear($('#BRNumber').parents('.control-group'));
                    }
                });
            });
            //listener for changes in BR Number
            $('#BRNumber').change(function() {
                $('.form-horizontal .buttonHolder .next-btn').attr('disabled', true);
            });

            //validation
            $('.form-horizontal .buttonHolder .next-btn').click(function() {
                if (validatePreDetails()) {
                    if ($("#turnover").val() == "HK2") {
                        OLASSubmitForm($('.form-horizontal'), "09-HK-OLAF-OtherPage.html");
                    } else {
                        OLASSubmitForm($('.form-horizontal'), $(this).attr('href'));
                    }
                } else {
                    OLASErrorForm($('.form-horizontal'));
                }
            })

            $("input").on("focusout", enableNextBtn);
            $("input").on("change", enableNextBtn);
            $("input").on("keyup", enableNextBtn);

            $("select").on("click", enableNextBtn);

            $("#countryCode").on("focusout", onOfficeAreaCodeChanged);
            $("#pin").on("focusout", onMobileCountryCodeChanged);

            var DBSvalidationObj = new DBSvalidation();
            DBSvalidationObj.init();
        }
        //toggle the office number as compulsory or not when the user changes the preferred contact number
    var toggleMobileBlock = function() {
        var checked = $('.form-horizontal input[name="prefContact"]:checked').attr('id');
        if (checked == 'prefContactOffice') {
            $('#officeNumber').removeAttr('data-nonrequired');
            $("#officeNumberTimes").removeClass("hidden");
        } else {
            $('#officeNumber').attr('data-nonrequired', 'true');
            $("#officeNumberTimes").addClass("hidden");
            //revoke the validation part
            if ($('#officeNumber').val() == '' || $('#officeNumber').val() == $('#officeNumber').attr('placeholder')) {
                $('#officeNumber').removeClass('success error');
                $('#officeNumber').parents('.control-group').removeClass('success error');
            }
        }
    };
    //validate the form in the page
    var validatePreDetails = function() {

        var validationObj = new validation();
        var DBSvalidationObj = new DBSvalidation();

        DBSvalidationObj.validate($('.form-horizontal'), validationObj);

        var isErrorTitle = $("[name=title]").hasClass("error");
        var isErrorName = $("[name=Name]").hasClass("error");

        if (isErrorName && isErrorTitle) {
            $('.bothInvalid').show();

            $('.nameInvalid').hide();
            $('.titleInvalid').hide();

        } else if (isErrorName) {
            $('.nameInvalid').show();

            $('.bothInvalid').hide();
            $('.titleInvalid').hide();

        } else if (isErrorTitle) {
            $('.titleInvalid').show();

            $('.bothInvalid').hide();
            $('.nameInvalid').hide();

        } else {
            $('.bothInvalid').hide();
            $('.nameInvalid').hide();
            $('.titleInvalid').hide();
        }

        return validationObj.isValid();
    };
});