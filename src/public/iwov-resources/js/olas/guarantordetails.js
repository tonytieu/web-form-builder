$(document).ready(function(){
	var guarantorDetailObj = new guarantorDetail();
	guarantorDetailObj.init();
});
var guarantorDetail = (function(){
	var popupEditorObj = new popupEditor();
	this.init = function(){
		//listener for guarantor checkbox
		//add the promoter
		$('.form-horizontal input[name="Guarantors"]').change(function(){
			toggleGuarantorBlock($(this));	
			buildMainPromoter();		
			iterateGuarantorBlock();
		});
		$('.form-horizontal input[name="Guarantors"]').each(function(){
			toggleGuarantorBlock($(this));
		});
		buildMainPromoter();
		iterateGuarantorBlock();

		$('#assignGurantor').change(function(){
			toggleGuarantorDelete();
		});
		toggleGuarantorDelete();

		//remvoe the guarantors
		$('.form-horizontal').delegate('.assignGurantorHld .deleteButton','click', function(e){
			var popUpObj = $('#modalWindow_personalBorrowGuarantorDelete');
			var name = $(this).parents('.assignGurantorHld').attr('data-guarantor');
			popupEditorObj.setPopUpObj(popUpObj);
			popupEditorObj.show();

			$(popUpObj).undelegate('.btn-panel-close','click').delegate('.btn-panel-close','click',function(){
				popupEditorObj.hide();
				resetGuarantorDeletePopup();
			});

			$(popUpObj).undelegate('.modal-footer button','click').delegate('.modal-footer button','click',function(e){
				if($(this).attr('id') == 'reasonToDelete_Done')
				{
					var validationObj = new validation();
					var DBSvalidationObj = new DBSvalidation();

					DBSvalidationObj.validate($('#modalWindow_personalBorrowGuarantorDelete'),validationObj);
					if(!validationObj.isValid()) return;
				
					var relatedCheckBox = $('.form-horizontal input[name="Guarantors"][value="'+name+'"]');
					relatedCheckBox.attr('checked',false)
					toggleGuarantorBlock(relatedCheckBox);	
					buildMainPromoter();		
					iterateGuarantorBlock();
				}
				popupEditorObj.hide();
				resetGuarantorDeletePopup();
			});
			resetGuarantorDeletePopup();
		});

		$('#modalWindow_personalBorrowGuarantorDelete #reason3').change(function(){
			toggleGuarantorDeletePopUp();
		});
			
		//popup for addPersonalBorrowings
		$('.form-horizontal').delegate('.AddPersonalBorrowings','click',function(e){
			var popUpObj = $('#modalWindow_personalBorrow');
			var th = $(this);
			popUpObj.find('.modal-footer button:not(#adduserSave)').removeClass('hide');
			popupEditorObj.setPopUpObj(popUpObj);
			var index = 'personalborrowings['+$(this).parents('.assignGurantorHld').attr('data-guarantor')+']';
			popupEditorObj.setIndex(index);
			popupEditorObj.show();

			$(popUpObj).undelegate('.btn-panel-close','click').delegate('.btn-panel-close','click',function(){
				popupEditorObj.hide();
				resetBorrowingPopup();
			});

			$(popUpObj).undelegate('.modal-footer button','click').delegate('.modal-footer button','click',function(e){
				if(!validateBorrowingPopUp()) return;
				var cntObj = $('#personalBorrow-AddOn-sample').clone();
				cntObj = popupEditorObj.addCnt(cntObj);
				th.siblings('.personalBorrowings').append(cntObj);
				if($(this).attr('id') == 'adduserSave')
					popupEditorObj.hide();

				resetBorrowingPopup();
			});
		});
		resetBorrowingPopup();

		//listeners for borrowing popup
		$('#finInstitution').change(function(){
			toggleBorrowingBankBlock();
		});
		$('#LoanType').change(function(){
			toggleBorrowingLoanBlock();
		});

		//toggle for none selector
		$('.loanTypeHld .control-group[data-relate="security"] input[type="checkbox"]').change(function(){
			if($(this).hasClass('chkBoxNone'))
			{
				if($(this).is(':checked'))
					$(this).parents('.controls').find('input[type="checkbox"]:not(.chkBoxNone)').attr('checked',false);
			}
			else
				$(this).parents('.controls').find('input[type="checkbox"].chkBoxNone').attr('checked',false);
		});

		//popup edit for addPersonalBorrowings
		$('.form-horizontal').delegate('.personalBorrowings .edit','click',function(e){
			var popUpObj = $('#modalWindow_personalBorrow');
			popUpObj.find('.modal-footer button:not(#adduserSave)').addClass('hide');
			var editObj = $(this).parents('.personalBorrow-AddOn');
			popupEditorObj.setPopUpObj(popUpObj);
			var index = 'personalborrowings['+$(this).parents('.assignGurantorHld').attr('data-guarantor')+']';
			popupEditorObj.setIndex(index);
			popupEditorObj.setEditCallBack(function(){
				toggleBorrowingBankBlock();
				toggleBorrowingLoanBlock();
			});
			popupEditorObj.show();
			popupEditorObj.setEditObj(editObj);
			popupEditorObj.editCnt();
			validateBorrowingPopUp();

			$(popUpObj).undelegate('.btn-panel-close','click').delegate('.btn-panel-close','click',function(){
				popupEditorObj.hide();
				resetBorrowingPopup();
			});

			$(popUpObj).undelegate('.modal-footer button','click').delegate('.modal-footer button','click',function(e){
				if(!validateBorrowingPopUp()) return;
				var cntObj = $('#personalBorrow-AddOn-sample').clone();
				cntObj = popupEditorObj.addCnt(cntObj);
				editObj.replaceWith(cntObj);
				if($(this).attr('id') == 'adduserSave')
					popupEditorObj.hide();
				resetBorrowingPopup();
			});
		});

		//delete personalborrowings
		$('.form-horizontal').delegate('.personalBorrowings .delete','click',function(e){
			var popUpObj = $('#modalWindow_Delete');
			var deleteObj = $(this).parents('.personalBorrow-AddOn');
			popupEditorObj.setPopUpObj(popUpObj);
			popupEditorObj.show();

			$(popUpObj).undelegate('.btn-panel-close','click').delegate('.btn-panel-close','click',function(){
				popupEditorObj.hide();
			});

			$(popUpObj).undelegate('.modal-footer button','click').delegate('.modal-footer button','click',function(e){
				popupEditorObj.hide();
				if($(this).hasClass('cancel')) return;
				deleteObj.remove();		
			});
		});

		//popup for addGuarantor
		$('#addGuarantor').click(function(e){
			var popUpObj = $('#modalWindow_guarantor');
			popUpObj.find('.modal-footer button:not(#addGuarantorSave)').removeClass('hide');
			popupEditorObj.setPopUpObj(popUpObj);
			popupEditorObj.setIndex('additionalGuarantor');
			popupEditorObj.show();

			$(popUpObj).undelegate('.btn-panel-close','click').delegate('.btn-panel-close','click',function(){
				popupEditorObj.hide();
				resetGuarantorPopup();
			});

			$(popUpObj).undelegate('.modal-footer button','click').delegate('.modal-footer button','click',function(e){
				if($(this).attr('id') == 'addGuarantorSave')
				{
					if(!validateGuarantorPopUp()) return;
					var cntObj = $('#additionalGuarantor-sample').clone();
					cntObj = popupEditorObj.addCnt(cntObj);
					$('#additionalGuarantor').append(cntObj);
				}
				popupEditorObj.hide();
				resetGuarantorPopup();
			});
		});
		resetGuarantorPopup();

		//listener for guarantor popup
		$('#modalWindow_guarantor input[name="guarantorType"]').change(function(){
			toggleGuarantorPopUpBlock();
		});

		//popup for editGuarantor
		$('#additionalGuarantor').delegate('.edit','click',function(e){
			var popUpObj = $('#modalWindow_guarantor');
			popUpObj.find('.modal-footer button:not(#addGuarantorSave)').removeClass('hide');
			var editObj = $(this).parents('.additionalGuarantor');
			popupEditorObj.setPopUpObj(popUpObj);
			popupEditorObj.setIndex('additionalGuarantor');
			popupEditorObj.setEditCallBack(function(){
				toggleGuarantorPopUpBlock();
			});
			popupEditorObj.show();
			popupEditorObj.setEditObj(editObj);
			popupEditorObj.editCnt();
			validateGuarantorPopUp();

			$(popUpObj).undelegate('.btn-panel-close','click').delegate('.btn-panel-close','click',function(){
				popupEditorObj.hide();
				resetGuarantorPopup();
			});

			$(popUpObj).undelegate('.modal-footer button','click').delegate('.modal-footer button','click',function(e){
				if(!validateGuarantorPopUp()) return;
				if($(this).attr('id') == 'addGuarantorSave')
				{
					var cntObj = $('#additionalGuarantor-sample').clone();
					cntObj = popupEditorObj.addCnt(cntObj);
					editObj.replaceWith(cntObj);
				}
				popupEditorObj.hide();
				resetGuarantorPopup();
			});
		});
		//confirm popup for guarantor delete
		$('#additionalGuarantor').delegate('.delete','click',function(e){
			//delete personalborrowings
			var popUpObj = $('#modalWindow_Delete');
			var deleteObj = $(this).parents('.additionalGuarantor');
			popupEditorObj.setPopUpObj(popUpObj);
			popupEditorObj.show();

			$(popUpObj).undelegate('.btn-panel-close','click').delegate('.btn-panel-close','click',function(){
				popupEditorObj.hide();
			});

			$(popUpObj).undelegate('.modal-footer button','click').delegate('.modal-footer button','click',function(e){
				popupEditorObj.hide();
				if($(this).hasClass('cancel')) return;
				deleteObj.remove();	
				toggleGuarantorBtn();
			});
		});

		//validation
		$('.form-horizontal .buttonHolder button').click(function(){
			var href = $(this).attr('href');
			if($(this).hasClass('prev-btn'))
				OLASSubmitForm($('.form-horizontal'),href);
			else if(validateGuarantorDetails($(this)))
				OLASSubmitForm($('.form-horizontal'),href);
			else
				OLASErrorForm($('.form-horizontal'));
		})

		var DBSvalidationObj = new DBSvalidation();
		DBSvalidationObj.init();
	};
	//hide or show the respective guarantor block if exists else build the block using the sample content
	var toggleGuarantorBlock = function(obj){
		var guarantor = obj.val();
		var name = obj.parent('label').text();
		if(!obj.is(':checked')) {
			$('.form-horizontal .assignGurantorHld[data-guarantor="'+guarantor+'"]').addClass('hide');
			return;
		}

		if($('.form-horizontal .assignGurantorHld[data-guarantor="'+guarantor+'"]').length > 0)
			$('.form-horizontal .assignGurantorHld[data-guarantor="'+guarantor+'"]').removeClass('hide');
		else
		{
			var sampleObj = $('#guarantorBlock_sample').clone();
			sampleObj.find('.guarantorName').html(name);
			sampleObj.removeAttr('id');
			sampleObj.removeClass('hide');
			sampleObj.addClass('assignGurantorHld');
			sampleObj.attr('data-guarantor',guarantor);
			sampleObj.append('<input type="hidden" name="guarantorName[]" value="'+guarantor+'"/>');
			if($('.assignGurantorHld').length > 0)
				$('.assignGurantorHld').after(sampleObj);
			else
				$('#guarantorBlock_sample').after(sampleObj);
		}
	};
	//build the main promoter list in the select box
	var buildMainPromoter = function(){
		var guarantor,name,optionClone;
		$('.form-horizontal input[name="Guarantors"]').each(function(){
			guarantor = $(this).val();
			if(guarantor == '' || guarantor.toLowerCase() == 'select') return;
			if($(this).is(':checked'))
			{
				name = $(this).parent('label').text();
				if($('#assignGurantor option[value="'+guarantor+'"]').length == 0)
				{
					optionClone = $('#assignGurantor option').first().clone();
					optionClone.attr('value',guarantor);
					optionClone.html(name);
					$('#assignGurantor').append(optionClone);
				}
			}
			else
			{
				$('#assignGurantor option[value="'+guarantor+'"]').remove();
			}
		});
		$('#assignGurantor').siblings('.holder').html($('#assignGurantor option:selected').val());
	};

	//hide and show the guarantor delete button
	var toggleGuarantorDelete = function(){
		var val = $('#assignGurantor').val();
		$('.form-horizontal .assignGurantorHld[data-guarantor] .deleteButton').removeClass('hide');
		$('.form-horizontal .assignGurantorHld[data-guarantor="'+val+'"] .deleteButton').addClass('hide');
	};
	//iterate the guarantor blocks and maintain the unique id and name
	var iterateGuarantorBlock = function(){
		var eleId = 1;
		var splitArr = new Array();
		$('.assignGurantorHld').each(function(){
			$(this).find('input[id],select[id]').each(function(){
				splitArr = $(this).attr('id').split('_');
				$(this).attr('id',splitArr[0]+'_'+eleId);
				//maintaining unique name for radio and checkbox
				if($(this).is(':radio') || $(this).is(':checkbox'))
				{
					if($(this).attr('name') != undefined)
					{
						splitArr = $(this).attr('name').split('_');
						$(this).attr('name',splitArr[0]+'_'+eleId);
					}
				}
			});
			//maintaining unique for attribute for labels
			$(this).find('label[for]').each(function(){
				splitArr = $(this).attr('for').split('_');
				$(this).attr('for',splitArr[0]+'_'+eleId);
			});
			eleId++;
		})
	};
	//hide or show the other div in the guarantor delete confirm popup
	var toggleGuarantorDeletePopUp = function(){
		if($('#reason3').is(":checked"))
			$('#modalWindow_personalBorrowGuarantorDelete .otherDIV').removeClass('hide');
		else
		{
			$('#modalWindow_personalBorrowGuarantorDelete .otherDIV').addClass('hide');
			OLASClear($('#modalWindow_personalBorrowGuarantorDelete .otherDIV'));
		}
	};

	//toggle popUpBank block
	var toggleBorrowingBankBlock = function(){
		if($('#finInstitution').val() == 'Others')
			$('#otherInstitution').parents('.control-group').removeClass('hide');
		else
		{
			$('#otherInstitution').parents('.control-group').addClass('hide');
			OLASClear($('#otherInstitution'));
		}
	};

	//toggle popUpLoan block
	var toggleBorrowingLoanBlock = function(){
		var val = $('#LoanType').val();
		$('#modalWindow_personalBorrow .loanTypeHld').addClass('hide');
		$('#'+val+'_DIV').removeClass('hide');
		try{
			OLASClear($('#modalWindow_personalBorrow .loanTypeHld:not(#'+val+'_DIV'));
		}catch(e){}
	};
	//reset the borrowing popup and toggle the blocks
	var resetBorrowingPopup = function(){
		OLASClear($('#modalWindow_personalBorrow'));
		toggleBorrowingBankBlock();
		toggleBorrowingLoanBlock();
	};

	//block toggler for guarantor popup
	var toggleGuarantorPopUpBlock = function(){
		var val = $('#modalWindow_guarantor input[name="guarantorType"]:checked').attr('id');
		var hide = val == 'corporate' ? 'individual' : 'corporate';
		$('#'+hide+'_div').addClass('hide');
		$('#'+val+'_div').removeClass('hide');
		OLASClear($('#'+hide+'_div'));
	};
	//reset the guarantor popup and toggle the blocks
	var resetGuarantorPopup = function(){
		OLASClear($('#modalWindow_guarantor'));
		toggleGuarantorPopUpBlock();
		toggleGuarantorBtn();
	};
	//hide or show the add more additional guarantor based on the count of added contents
	var toggleGuarantorBtn = function(){
		if($('.additionalGuarantor:not(#additionalGuarantor-sample)').length >= 5) 
			$('#addGuarantor').addClass('hide');
		else
			$('#addGuarantor').removeClass('hide');
	};
	//reset the datas in guarantor delete popup
	var resetGuarantorDeletePopup = function(){
		OLASClear($('#modalWindow_personalBorrowGuarantorDelete'));
		toggleGuarantorDeletePopUp();
	};

	//validating borrowing popup
	var validateBorrowingPopUp = function(){
		var validationObj = new validation();
		var DBSvalidationObj = new DBSvalidation();

		DBSvalidationObj.validate($('#modalWindow_personalBorrow'),validationObj);
		return validationObj.isValid();
	};
	//validate the additional guarantor popup
	var validateGuarantorPopUp = function(){
		var validationObj = new validation();
		var DBSvalidationObj = new DBSvalidation();

		DBSvalidationObj.validate($('#modalWindow_guarantor'),validationObj);
		return validationObj.isValid();
	};
	//validate the form
	var validateGuarantorDetails = function(obj){
		var validationObj = new validation();
		var DBSvalidationObj = new DBSvalidation();
		if(!obj.hasClass('next-btn')) DBSvalidationObj.setCompulsory(false);

		DBSvalidationObj.validate($('.form-horizontal'),validationObj);
		return validationObj.isValid();
	};
});