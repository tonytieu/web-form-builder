var popupEditor = (function() {

    var editObj = '';
    var popUpObj;
    var th = $(this);
    var index = '';
    var editCallBack;

    //hide the popup while pressing the escape button
    $(document).keyup(function(e) {
        if (e.keyCode == 27 && popUpObj != undefined) {
            popUpObj.addClass('hide');
            $('#overlay').css('display', 'none');
            $("html").removeClass("noscroll");
        }
    });

    //hide the popup while pressing outside the popup
    $(document).ready(function() {
        $('#overlay').click(function() {
            popUpObj.addClass('hide');
            $('#overlay').css('display', 'none');
            $("html").removeClass("noscroll");
        });
    });

    //setter function to set the popup create object
    this.setPopUpObj = function(obj) {
        popUpObj = obj;
    };

    //set the index for post value to send to the server
    this.setIndex = function(name) {
        index = name;
    };

    //setter function to set the popup edit object
    this.setEditObj = function(obj) {
        editObj = obj;
    };

    //show the popup
    this.show = function() {
        popUpObj.removeClass('hide');
        $('#overlay').css('display', 'block');
        $("html").addClass("noscroll");
    };

    //hide the popup
    this.hide = function() {
        popUpObj.addClass('hide');
        $('#overlay').css('display', 'none');
        $("html").removeClass("noscroll");
    };

    //set the call back function
    this.setEditCallBack = function(fn) {
        editCallBack = fn;
    };

    //parse the user added contents from the popup and add it to the form
    this.addCnt = function(cntObj) {
        //console.log(cntObj);
        var id = cntObj.attr('id');
        //remove the sample part from the content object id
        cntObj.removeAttr('id');
        cntObj.removeClass('hide');
        var relateVal;
        var val;
        var html;
        var relateObj;
        var facilitytotal = 0;
        var collateraltotal = 0;

        var shareholdingindex = cntObj.attr("data-shareholdingindex");
        console.log(shareholdingindex);

        var borrowingindex = cntObj.attr("data-borrowingindex");
        var otherBankNameSpecifyVal = cntObj.attr("data-otherBankName");
        var otherBankCodeSpecifyVal = cntObj.attr("data-otherBankCode");
        var bankCodeVal = cntObj.attr("data-bankCode");
        var bankName = cntObj.attr("data-bankName");
        var isFacilityOthers = false;
        var isCollateralOthers = false;


        //console.log(otherBankSpecifyVal);

        cntObj.find('.controls[data-relate]').each(function() {
            relateVal = $(this).attr('data-relate');
            relateObj = popUpObj.find('.control-group:visible[data-relate="' + relateVal + '"]');
            var index = $(this).parent().attr("data-index");

            console.log(index);

            //iterate the content object and mapped it with user added contents
            val = '';
            if (relateObj.length > 0) {
                if (relateObj.find('input[type="checkbox"]').length > 0) {
                    val = '';
                    html = '';
                    popUpObj.find('.control-group:visible[data-relate="' + relateVal + '"] input[type="checkbox"]:checked').each(function() {
                        val += val != '' ? ', ' + $(this).val() : $(this).val();
                        html += html != '' ? ', ' + $(this).parent().text() : $(this).parent().text();
                    });
                } else if (relateObj.find('input[type="radio"]').length > 0) {
                    val = popUpObj.find('.control-group:visible[data-relate="' + relateVal + '"] input[type="radio"]:checked').val();
                    html = $(this).parent().text();
                } else if (relateObj.find('select').length > 0) {

                    val = popUpObj.find('.control-group:visible[data-relate="' + relateVal + '"] select option:selected').text();
                    html = popUpObj.find('.control-group:visible[data-relate="' + relateVal + '"] select option:selected').html();

                    if (relateVal == "bankName") {
                        val = popUpObj.find('.control-group:visible[data-relate="' + relateVal + '"] select option:selected').val();
                    }

                    //console.log(val);
                    if (val == 'Others') {
                        var otherVal = popUpObj.find('.control-group:visible[data-relate="' + relateVal + '_other"] input').val();
                        if (otherVal) {
                            val = html = otherVal;
                        } else {
                            val = html = "Others";
                        }
                    }

                    //console.log(val);
                } else if (relateObj.find('input').length > 0) {
                    val = popUpObj.find('.control-group:visible[data-relate="' + relateVal + '"] input').val();

                    //console.log(val.replaceAll(",", ""));
                    if (relateVal.startsWith("facility")) {

                        facilitytotal += parseFloat(val.replaceAll(",", ""));
                    } else if (relateVal.startsWith("collateral")) {

                        collateraltotal += parseFloat(val.replaceAll(",", ""));
                    }


                    html = 'HKD ' + parseFloat(val).toLocaleString();
                    var validationType = popUpObj.find('.control-group:visible[data-relate="' + relateVal + '"] input').attr('data-dbsvalidate');
                    if (validationType == 'price') {
                        html = 'HKD ' + val;
                    } else {

                        if (isNaN(parseFloat(val))) {
                            html = val;
                        } else {
                            html = parseFloat(val).toLocaleString();
                        }

                        //console.log(html);
                    }
                }

                if (relateVal.startsWith("facility") && val.startsWith("Others")) {
                    html = $(".faclityOther").eq(index).val();
                    isFacilityOthers = true;
                } else if (relateVal.startsWith("collateral") && val.startsWith("Others")) {
                    html = $(".collateralOther").eq(index).val();
                    isCollateralOthers = true;
                }

                $(this).find('p').attr('data-val', val);

                $(this).find('p').html(html);
            }
            //remove the non required elements
            else {
                //console.log(relateVal);
                if (relateVal == 'bankCode' && otherBankNameSpecifyVal) {
                    $(this).find('p').attr('data-val', otherBankCodeSpecifyVal);
                    $(this).find('p').html(otherBankCodeSpecifyVal);
                } else {
                    $(this).prev().remove();
                    $(this).remove();
                }
            }


            //add input elements for server side
            //console.log(relateVal);

            if (shareholdingindex != undefined) {
                index = "olafBean.businessDetailsHk.shareholding";
                console.log(index);
                if (cntObj.find('input[name="' + index + '[' + relateVal + ']"]').length == 0)
                    cntObj.append('<input type="hidden" data-relatedName="' + relateVal + '" name="' + index + '[' + shareholdingindex + '].' + relateVal + '"/>');
                cntObj.find('input[name="' + index + '[' + shareholdingindex + '].' + relateVal + '"]').val(val);

            } else if (borrowingindex != undefined) {
                index = "olafBean.businessDetailsHk.companyBorrowing";
                if (relateVal.startsWith("facilityLoanType") || relateVal.startsWith("facilityLimit") ||
                    relateVal.startsWith("collateralLoanType") || relateVal.startsWith("collateralLimit")) {


                    var commonVariable = cntObj.find('[data-relate="' + relateVal + '"]').attr("data-commonVariable");
                    var independentVariable = cntObj.find('[data-relate="' + relateVal + '"]').attr("data-independentVariable");
                    var relateValIndex = relateVal.charAt(relateVal.length - 1);

                    if (isFacilityOthers && relateVal.startsWith("facility")) {
                        val = html;
                        cntObj.append('<input type="hidden" value="true" name="' + index + '[' + borrowingindex + '].' + commonVariable + '[' + relateValIndex + '].' + "ifOtherSpecify" + '"/>');
                    } else if (isCollateralOthers && relateVal.startsWith("collateral")) {
                        val = html;
                        cntObj.append('<input type="hidden" value="true" name="' + index + '[' + borrowingindex + '].' + commonVariable + '[' + relateValIndex + '].' + "ifOtherSpecify" + '"/>');
                    }


                    if (cntObj.find('input[name="' + index + '[' + relateVal + ']"]').length == 0)
                        cntObj.append('<input type="hidden" data-relatedName="' + relateVal + '" name="' + index + '[' + borrowingindex + '].' + commonVariable + '[' + relateValIndex + '].' + independentVariable + '"/>');

                    cntObj.find('input[name="' + index + '[' + borrowingindex + '].' + commonVariable + '[' + relateValIndex + '].' + independentVariable + '"]').val(val);

                    //console.log(cntObj.find(".facility[data-index="+relateValIndex+"]").attr("data-facilitycode"));
                    // facility code - collateral code

                    var facilityCodeName = index + '[' + borrowingindex + '].' + commonVariable + '[' + relateValIndex + '].' + "code";
                    //console.log(facilityCodeName);
                    if (cntObj.find('input[name="' + facilityCodeName + '"]').length == 0) {
                        var value = cntObj.find(".facility[data-index=" + relateValIndex + "]").attr("data-facilitycode");

                        if (relateVal.startsWith("collateralLoanType") || relateVal.startsWith("collateralLimit")) {
                            value = cntObj.find(".collateral[data-index=" + relateValIndex + "]").attr("data-collateralcode");
                        }

                        cntObj.append('<input value="' + value + '" type="hidden" data-relatedName="' + relateVal + '" name="' + facilityCodeName + '"/>');
                    }


                    // cntObj.find('input[name="' + facilityCodeName + '"]')
                    //     .val(cntObj.find(".facility[data-index="+relateValIndex+"]").attr("data-facilitycode"));

                    // 
                } else {
                    if (cntObj.find('input[name="' + index + '[' + relateVal + ']"]').length == 0)
                        cntObj.append('<input type="hidden" data-relatedName="' + relateVal + '" name="' + index + '[' + borrowingindex + '].' + relateVal + '"/>');


                    cntObj.find('input[name="' + index + '[' + borrowingindex + '].' + relateVal + '"]').val(val);



                }
            } else {
                if (cntObj.find('input[name="' + index + '[' + relateVal + ']"]').length == 0)
                    cntObj.append('<input type="hidden" data-relatedName="' + relateVal + '" name="' + index + '[' + relateVal + ']"/>');
                cntObj.find('input[name="' + index + '[' + relateVal + ']"]').val(val);
            }


        });

        if (otherBankNameSpecifyVal) {

            cntObj.append('<input type="hidden" data-relatedName="' + 'ifOtherBankSpecify' + '" name="' + index + '[' + borrowingindex + '].' + 'ifOtherBankSpecify' + '" value="true"/>');
            cntObj.find('input[data-relatedname=bankCode]').val("Others");
        } else {
            cntObj.find('input[data-relatedname=bankCode]').val(bankCodeVal);
            cntObj.find('input[data-relatedname=bankName]').val(bankName);
        }

        if (shareholdingindex != undefined) {
            relateVal = "positionCode";
            index = "olafBean.businessDetailsHk.shareholding";
            //console.log(cntObj);
            cntObj.append('<input type="hidden" data-relatedName="' + relateVal + '" name="' + index + '[' + shareholdingindex + '].' + relateVal + '" value="' + cntObj.attr("data-positionCode") + '" />');

        }


        // total for facility and collateral
        //console.log(cntObj.find("#facility_total"));
        var facilityTotalClone = $("#facility_total").clone();
        $(facilityTotalClone).find(".controls[data-relate=facilityTotal]").html('HKD ' + facilitytotal.toLocaleString());
        cntObj.find("#facility_total").append(facilityTotalClone);
        cntObj.find("[data-relatedname=facilityTotal]").val(facilitytotal);

        var collateralTotalClone = $("#collateral_total").clone();
        $(collateralTotalClone).find(".controls[data-relate=collateralTotal]").html('HKD ' + collateraltotal.toLocaleString());
        cntObj.find("#collateral_total").append(collateralTotalClone);
        cntObj.find("[data-relatedname=collateralTotal]").val(collateraltotal);

        console.log("total facility " + facilitytotal);
        console.log("collateraltotal " + collateraltotal);

        console.log(cntObj.find("table").attr("id"));
        if (cntObj.find("table").attr("id") == "shareholder-data") {

        }

        return cntObj;
    };

    //parse the previously added contents from the form and add it to the popup
    this.editCnt = function() {
        var val;
        var relateObj;
        var valArr;
        var relateVal;
        var otherBankNameSpecifyVal = editObj.attr("data-otherBankName");
        var otherBankCodeSpecifyVal = editObj.attr("data-otherBankCode");
        var isFacilityOthers = false;
        var isCollateralOthers = false;

        //console.log(otherBankNameSpecifyVal);

        editObj.find('.controls[data-relate]').each(function() {
            relateVal = $(this).attr('data-relate');
            relateObj = popUpObj.find('.control-group:visible[data-relate="' + relateVal + '"]');
            val = $(this).find('p').attr('data-val');
            var index = $(this).parent().attr("data-index");
            var facilityCode = $(this).parent().attr("data-facilitycode");
            var collateralCode = $(this).parent().attr("data-collateralcode");

            console.log(index);


            if (relateObj.find('input[type="checkbox"]').length > 0) {
                valArr = val.split(', ');
                for (var i = 0; i < valArr.length; i++) {
                    popUpObj.find('.control-group:visible[data-relate="' + relateVal + '"] input[type="checkbox"][value="' + valArr[i].trim() + '"]').attr('checked', true);
                }
                //popUpObj.find('.control-group:visible[data-relate="'+relateVal+'"] input[type="checkbox"]').trigger('change');
            } else if (relateObj.find('input[type="radio"]').length > 0) {
                popUpObj.find('.control-group:visible[data-relate="' + relateVal + '"] input[type="radio"][value="' + val + '"]').attr('checked', true);
                //popUpObj.find('.control-group:visible[data-relate="'+relateVal+'"] input[type="radio"]').trigger('change');
            } else if (relateObj.find('select').length > 0) {
                var selectObj = popUpObj.find('.control-group:visible[data-relate="' + relateVal + '"] select');

                if (facilityCode) {
                    //console.log(val);
                    if (val.startsWith("Others")) {
                        $(".faclityOther").eq(index).val($(this).find('p').html());
                        $(".faclityOther").eq(index).removeClass("hide");
                    } else {
                        $(".faclityOther").addClass("hide");
                    }
                    val = facilityCode;
                }

                if (collateralCode) {
                    if (val.startsWith("Others")) {
                        $(".collateralOther").eq(index).val($(this).find('p').html());
                        $(".collateralOther").eq(index).removeClass("hide");
                    } else {
                        $(".collateralOther").eq(index).addClass("hide");
                    }
                    val = collateralCode;
                }

                if (popUpObj.find('.control-group:visible[data-relate="' + relateVal + '"] option[value="' + val + '"]').length == 0) {
                    selectObj.val('Others');
                    selectObj.siblings('.holder').html(selectObj.find('option:selected').html());
                    popUpObj.find('.control-group[data-relate="' + relateVal + '_other"] input').val(val);
                } else {
                    selectObj.val(val);


                    selectObj.siblings('.holder').html(selectObj.find('option:selected').html());
                }
                //console.log(editObj);
                if (editObj.hasClass("shareholder-AddOn")) {
                    var selectedValue = $("#position option").filter(function() { return $(this).html() == val }).val();
                    $("#position").val(selectedValue);
                    $("#position").trigger('change');
                }
                //popUpObj.find('.control-group:visible[data-relate="'+relateVal+'"] select').trigger('change');
            } else if (relateObj.find('input').length > 0) {
                popUpObj.find('.control-group:visible[data-relate="' + relateVal + '"] input').val(val);
            }
            if (typeof editCallBack == 'function') {
                editCallBack();
                // eval(editCallBack + '();');
            }
        });

        if (otherBankNameSpecifyVal) {
            popUpObj.find('#otherInstitutionBankCodeInputBox').val(otherBankCodeSpecifyVal);
        }
    };
});