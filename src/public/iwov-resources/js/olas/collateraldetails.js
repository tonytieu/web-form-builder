$(document).ready(function(){
	var collateralDetailObj = new collateralDetail();
	collateralDetailObj.init();

	$("select[name=ED_Category]").on("change", function(){
		//console.log($(this).parents("[data-cloneitem=fd_fixeddeposit]").find("[name=ED_Category_Others_Value]"));
		if($(this).val() == "Others") {
			//console.log($(this).parents("[data-cloneitem=fd_fixeddeposit]").find("#ED_Category_Others_DIV"));
			$(this).parents("[data-cloneitem=fd_fixeddeposit]").find(".otherDivClass").removeClass("hide");
		} else {
			$(this).parents("[data-cloneitem=fd_fixeddeposit]").find(".otherDivClass").addClass("hide");
		}
	});

	$("#VehicleDetails_DIV input[value=Used]").trigger('click');

		// $(".cloneadder").on("click", function(){
		// 	console.log("cloneadder");
		// 	$("input[checked=checked]").trigger('click');
		// });

	if(navigator.userAgent.match(/iPad|iPhone|iPod/i)){
          $("#FD_1").css("width", "30%");
          $(".noMTop").css("clear", "both");
      }
});
var collateralDetail = (function(){
	this.init = function(){
		var cloneObj = new cloner();
		cloneObj.init();
		organisePage();

		//toggler for pledge chk
		$('.form-horizontal').delegate('.fdsection .pledgeChk','change',function(){
			togglePledgorBlock($(this));
		});
		$('.form-horizontal .fdsection .pledgeChk').each(function(){
			togglePledgorBlock($(this));
		});
		
		//bala
		//others text box toggling in refinancing_existingBank 
		$('#refinancing_existingBank').change(function(){
			toggleRFinstitutionBlock();
		});
		toggleRFinstitutionBlock();

		//listener for propertytype radio
		// $('.form-horizontal input[data-name="propertyType"]').change(function(){
		// 	togglePropertyTypeBlock(this);
		// });
		//togglePropertyTypeBlock();
		//listener for type of property selection 
		$('#newProperty_propertyType').change(function(){
			toggleNewPropertyType();
		});
		toggleNewPropertyType();
		//listener for use of property selection 
		$('#newProperty_propertyUse').change(function(){
			toggleNewPropertyUse();
		});
		toggleNewPropertyUse();

		//listener for Refinancing checkbox
		$('#newProperty_refinancing').change(function(){
			toggleRefinancingBlock();
		});
		toggleRefinancingBlock();

		//listener for type of facility Checkbox
		$('#others').change(function(){
			toggleFacilityTypeBlock();
		});
		toggleFacilityTypeBlock();

		//listener for CPF used checkbox
		$('#newProperty_CPFUsed_chk').change(function(){
			toggleCPFUsedBlock();
		});
		toggleCPFUsedBlock();

		//listener for category change in vehicle details and equipment details
		$('#ED_Category,#VD_Category').change(function(){
			toggleCategoryBlock($(this));
		});
		toggleCategoryBlock($('#ED_Category'));
		toggleCategoryBlock($('#VD_Category'));

		//validation
		$('.form-horizontal .buttonHolder button').click(function(){
			var href = $(this).attr('href');
			if($(this).hasClass('prev-btn')) OLASSubmitForm($('.form-horizontal'),href);
			if(validateCollateralDetails($(this)))
				OLASSubmitForm($('.form-horizontal'),href);
			else
				OLASErrorForm($('.form-horizontal'));
		});

		var DBSvalidationObj = new DBSvalidation();
		DBSvalidationObj.init();
	};
	
	//bala
	//show or hide the main operating block
	var toggleRFinstitutionBlock = function(){
		if($('#refinancing_existingBank').val() == 'Others')
			$('.control-group.otherDIV').removeClass('hide');
		else
		{
			$('.control-group.otherDIV').addClass('hide');
			OLASClear($('.control-group.otherDIV'));
		}
	};

	//hide or show the pledgor block
	var togglePledgorBlock = function(obj){
		if(obj.is(':checked'))
			obj.parents('.fdsection').find('.pledgeChkCnt').removeClass('hide');
		else{
			OLASClear(obj.parents('.fdsection').find('.pledgeChkCnt'));
			obj.parents('.fdsection').find('.pledgeChkCnt').addClass('hide');
			$(obj.parents('.fdsection')[0]).find('input[type=text]').trigger('focusout');
		}
		console.log(obj.parents('.fdsection')[0]);
		
	};

	//decide which blocks to show and hide
	var organisePage = function(){
		var block;
		$('.form-horizontal [data-displayblock]').addClass('hide');
		$('.form-horizontal input[type="hidden"][data-display]').each(function(){
			block = $(this).attr('data-display');
			$('.form-horizontal [data-displayblock="'+block+'"]').removeClass('hide');
		});
	};

	//hide or show the land area section in new property block
	var toggleNewPropertyType = function(){
		if($('#newProperty_propertyType option[data-landarea="true"]:selected').length > 0)
			$('#newProperty_LandArea_input').parents('.control-group').removeClass('hide');
		else
		{
			$('#newProperty_LandArea_input').parents('.control-group').addClass('hide');
			OLASClear($('#newProperty_LandArea_input').parents('.control-group'));
		}
	};
	//hide or show the monthly income details section in new property block
	var toggleNewPropertyUse = function(){
		if($('#newProperty_propertyUse option[data-incomedetails="true"]:selected').length > 0)
			$('#newProperty_Income').parents('.control-group').removeClass('hide');
		else
		{
			$('#newProperty_Income').parents('.control-group').addClass('hide');
			OLASClear($('#newProperty_Income').parents('.control-group'));
		}
	};
	//hide or show the Refinancing from another bank block
	var toggleRefinancingBlock = function(){
		if($('#newProperty_refinancing').is(':checked'))
			$('#newProperty_refinancing_DIV').removeClass('hide');
		else
		{
			$('#newProperty_refinancing_DIV').addClass('hide');
			OLASClear($('#newProperty_refinancing_DIV'));
		}
	};
	//hide or show the other section in based on the type of facility selection
	var toggleFacilityTypeBlock = function(){
		if($('#others').is(':checked'))
			$('#Others_DIV').removeClass('hide');
		else
		{
			$('#Others_DIV').addClass('hide');
			OLASClear($('#Others_DIV'));
		}
	};
	//hide or show the CPF used to date block
	var toggleCPFUsedBlock = function(){
		if($('#newProperty_CPFUsed_chk').is(':checked'))
			$('#newProperty_CPFUsed').parents('.control-group').removeClass('hide');
		else
		{
			$('#newProperty_CPFUsed').parents('.control-group').addClass('hide');
			OLASClear($('#newProperty_CPFUsed').parents('.control-group'));
		}
	};

	//hide or show the other input box in category section of equipment details and vehicle details block
	var toggleCategoryBlock = function(obj){
		var id = obj.attr('id');
		if(obj.val() == 'Others')
			$('#'+id+'_Others_DIV').removeClass('hide');
		else
		{
			$('#'+id+'_Others_DIV').addClass('hide');
			OLASClear($('#'+id+'_Others_DIV'));
		}
	};

	//form validation for collateral
	var validateCollateralDetails = function(obj){
		var validationObj = new validation();
		var DBSvalidationObj = new DBSvalidation();
		if(!obj.hasClass('next-btn')) DBSvalidationObj.setCompulsory(false);

		DBSvalidationObj.validate($('.form-horizontal'),validationObj);
		return validationObj.isValid();
	};
	//wrapper function for togglePledgorBlock
	this.wrap_togglePledgorBlock = function(obj){
		togglePledgorBlock(obj);
	};
});

//extending the cloner callback and add the callback register
clonerCallBack.prototype.fd_property = function(obj){
	var collateralDetailObj = new collateralDetail();
	collateralDetailObj.wrap_togglePledgorBlock(obj.find('.pledgeChk'));
};


//toggler for property type
var togglePropertyTypeBlock = function(ele){
	$(ele).parents(".fdsection").find('.propertyTypeDetails').addClass('hide');
	var id = $(ele).parents(".fdsection").find('input[data-name="propertyType"]:checked').attr('data-id');
	
	//console.log(id);

	
	$(ele).parents(".fdsection").find('#'+id+'_DIV').removeClass('hide');
	OLASClear($(ele).parents(".fdsection").find('.propertyTypeDetails:not(#'+id+'_DIV)'));	
};