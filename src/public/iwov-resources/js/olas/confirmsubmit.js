$(document).ready(function() {
    var confirmSubmitObj = new confirmSubmit();
    confirmSubmitObj.init();


    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        $(".LoanRequirements .control-group .span6").css("line-height", "2.6em");
        $(".confirmation-label").css("text-align", "left");
        console.log("mobile");
    }

    $(".span4").each(function(index) {
        var t = $(this).text();
        if (!isNaN(t) && t.length > 0) {
            console.log(t);
            $(this).text(parseFloat(t).toLocaleString());
        }
    });

    $(".confirmation-value, .facility p, .collateral p, #facility_total .controls, #collateral_total .controls").each(function(index) {
        var str = $(this).text();

        var res = str.replace(/\d+/gi, function myFunction(x) { return parseFloat(x).toLocaleString(); });

        $(this).text(res);

    });

});
var confirmSubmit = (function() {
    var olasObj = new olas();
    var queryArr = olasObj.getQueryArr(document.URL);
    if (queryArr['microLoan'] != undefined && queryArr['microLoan'] == 1)
        $('.CollateralDetails').addClass('hide');
    else
        $('.CollateralDetails').removeClass('hide');

    this.init = function() {
        //validation
        $('.form-horizontal button').click(function() {
            if ($(this).parent('.buttonHolder').length == 1)
                OLASSubmitForm($('.form-horizontal'), $(this).attr('href'));
            else
                window.location.href = $(this).attr('href');
        })
    };

    $(".addElement_icon").addClass("hide");

    $(".removeElement_icon").on("click", function() {
        $(this).parent().siblings().addClass("hide");
        $(this).next().removeClass("hide");
        $(this).addClass("hide");
    });

    $(".addElement_icon").on("click", function() {
        $(this).parent().siblings().removeClass("hide");
        $(this).prev().removeClass("hide");
        $(this).addClass("hide");
    });
});