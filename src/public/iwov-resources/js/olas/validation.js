var validation = (function() {
    var isValid = true;

    this.hasCond = function(status) {
        updateValidity(status);
        return status;
    };

    this.isNonEmpty = function(val) {
        var status = val != undefined && val.trim() != '';
        updateValidity(status);
        return status;
    };

    //email validation function which was used earlier
    this.isEmail = function(a) {
        var a = $.trim(a),
            b = /^((([a-z]|\d|[\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i,
            c = b.test(a);
        if (c) {
            var d = a.split("@")[0];
            d.match(/\.{2,}/) && (c = !1);
            var e = a.split("@")[1];
            e.match(/\.{2,}/) && (c = !1);

            if (c) {

                var ss = e.split('.');
                c = !ss.some(function(s) { return s.length == 1; });
            }
        }
        updateValidity(c);
        return c;
    };

    this.maxLength = function(val, length) {
        var status = val.length <= length;
        updateValidity(status);
        return status;
    };

    this.minLength = function(val, length) {
        var status = val.length >= length;
        updateValidity(status);
        return status;
    };

    this.isInteger = function(val) {
        var status = checkPattern(val, /^\d+$/);
        updateValidity(status);
        return status;
    };

    // \u4e00-\u9eff: values for traditional chinese characters
    // \u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF
    this.isAlphabet = function(val) {
        var status = checkPattern(val, /^[a-zA-Z \u4e00-\u9eff]*$/);
        updateValidity(status);
        return status;
    };

    this.isCompanyName = function(val) {
        var status = checkPattern(val, /^[a-zA-Z0-9@!?$&-\(\)',./ \u4e00-\u9eff]*$/);
        updateValidity(status);
        return status;
    };

    this.isAlphaNumeric = function(val) {
        //used from the previous one
        var status = checkPattern(val, /^[0-9a-zA-Z \u4e00-\u9eff]*$/);
        updateValidity(status);
        return status;
    };

    //bala
    this.isAlphaNumericSpecial = function(val) {
        //used from the previous one
        //var status = checkPattern(val, /^[ a-zA-Z0-9 \n\‘\’\“\”\/\-`~!%#+@=&:"(){},|\]\[;*$^_?.']*$/);
        var status = checkPattern(val, /^[A-Z\u4e00-\u9eff]+[ a-zA-Z0-9 -\u4e00-\u9eff]*$/);
        updateValidity(status);
        return status;
    };

    this.isPrice = function(val, decimalLen) {
        var status = checkPattern(val, /^\d{1,3}(,\d{3})*(\.\d+)?$/);
        if (decimalLen != undefined)
            status = decimalPosition(val, decimalLen) && status;
        updateValidity(status);
        return status;
    };

    var checkPattern = function(val, pattern) {
        return pattern.test(val);
    };

    this.isNumber = function(val, decimalLen) {
        var status = !isNaN(val); //check whether it is a number
        if (decimalLen != undefined)
            status = decimalPosition(val, decimalLen) && status;
        updateValidity(status);
        return status;
    };

    this.isFloat = function(val, decimalLen) {
        var status = !isNaN(val); //check whether it is a number
        if (decimalLen == undefined) decimalLen = 1;
        status = decimalPosition(val, decimalLen) && status;
        updateValidity(status);
        return status;

    };
    //TODO: for positive values
    //TODO: for negative values

    var decimalPosition = function(val, decimalLen) {
        var decimalPosition = val.indexOf("."); //check for decimal position
        if (decimalPosition == -1) return true;
        return decimalPosition >= 0 && val.substring(decimalPosition + 1).length <= decimalLen ? true : false;
    };

    this.maxValue = function(val, max) {
        var status = parseInt(val) <= parseInt(max);
        updateValidity(status);
        return status;
    };

    this.minValue = function(val, min) {
        var status = parseInt(val) >= parseInt(min);
        updateValidity(status);
        return status;
    };

    this.inArray = function(val, array) {
        //TODO:
    };

    // this.hasSubStr = function(val, str, start, end){
    // 	if(start == undefined) start = 0;
    // 	if(end == undefined) end = val.length;
    // 	var status = val.substring(start, end) == str;
    // 	updateValidity(status);
    // 	return status;
    // };

    var updateValidity = function(val) {
        isValid = isValid && val;
    };

    this.isValid = function() {
        return isValid;
    };
});