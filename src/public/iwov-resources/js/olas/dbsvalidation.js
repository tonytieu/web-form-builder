var DBSvalidation = (function() {

    this.init = function() {
        $('input[data-dbsvalidate="price"]').priceFormat({
            prefix: '',
            centsSeparator: '',
            centsLimit: -1,
            clearPrefix: true,
            clearSufix: true
        });

        $('input[data-dbsvalidate="pricedecimal"]').priceFormat({
            prefix: '',
            centsLimit: 2,
            clearPrefix: true,
            clearSufix: true
        });

        var formObj = $('.form-horizontal');
        //validation on focus out
        $(formObj).delegate('.control-group input:not([type="file"]):not([type="radio"]):not([type="checkbox"])[data-dbsvalidate],.control-group textarea[data-dbsvalidate]', 'focusout', function() {
            // if ($(this).attr('data-dbsvalidate') == 'date') return; //skip focus out validation for date picker
            //console.log("focusout");
            var validationObj = new validation();
            validateEle(validationObj, $(this));
        });

        $(formObj).delegate('.control-group select[data-dbsvalidate],.control-group:visible input[type="radio"][data-dbsvalidate],.control-group:visible input[type="checkbox"][data-dbsvalidate]', 'change', function() {
            //console.log("change");
            var validationObj = new validation();
            validateEle(validationObj, $(this));
        });

        $('input[type="text"][data-dbsvalidate]').on('focusout', function() {
            //console.log("change1");
            var validationObj = new validation();
            validateEle(validationObj, $(this));
        });
    };

    var isNonCompulsory = false;

    this.setCompulsory = function(flag) {
        isNonCompulsory = !flag;
    };

    //iterate the form object and check the validity status of the control group
    this.validate = function(formObj, validationObj) {
        //validating input boxes
        $(formObj).find('.control-group:visible input:not([type="file"]):not([type="radio"]):not([type="checkbox"])[data-dbsvalidate],.control-group:visible textarea[data-dbsvalidate]').each(function() {
            validateEle(validationObj, $(this));
        });

        //validating select boxes
        $(formObj).find('.control-group:visible select[data-dbsvalidate]').each(function() {
            validateEle(validationObj, $(this));
        });

        //validating radio and checkboxes
        $(formObj).find('.control-group:visible input[type="radio"][data-dbsvalidate],.control-group:visible input[type="checkbox"][data-dbsvalidate]').each(function() {
            validateEle(validationObj, $(this));
        });

    };

    //wrapper function for validate object
    this.validateObj = function(validationObj, obj, statusOnly) {
        return validateEle(validationObj, obj, statusOnly);
    };
    //check the validity of the control group
    //and add the success or error class
    var validateEle = function(validationObj, obj, statusOnly) {
        // return true;
        var isValidEle;
        if (statusOnly == undefined) statusOnly = false;
        if (obj.attr('type') == 'radio')
            isValidEle = validateRadioBox(validationObj, obj);
        else if (obj.attr('type') == 'checkbox')
            isValidEle = validateCheckBox(validationObj, obj);
        else if (obj.is('input') || obj.is('textarea'))
            isValidEle = validateTextBox(validationObj, obj);
        else if (obj.is('select'))
            isValidEle = validateSelectBox(validationObj, obj);

        //if need provide only the status without changing the dom elements
        if (statusOnly == true) return isValidEle;
        if (isValidEle === false) {
            if (obj.attr('type') != 'radio' && obj.attr('type') != 'checkbox') {
                obj.addClass('error').removeClass('success');
                if (obj.is('select'))
                    obj.parent('.select-wrapper').addClass('error').removeClass('success');
            }
            obj.parents('.control-group').addClass('error').removeClass('success').trigger('classChange');
        } else if (isValidEle === true) {
            if (obj.attr('type') != 'radio' && obj.attr('type') != 'checkbox') //skip adding classes for radio boxes
            {
                obj.addClass('success').removeClass('error');
                if (obj.is('select'))
                    obj.parent('.select-wrapper').addClass('success').removeClass('error');
            }
            var adjacentStatus = checkAdjcentStatus(obj);
            if (adjacentStatus === false) //if there is any boxes with error
                obj.parents('.control-group').addClass('error').removeClass('success').trigger('classChange');
            if (adjacentStatus === true) //if there is any boxes with success
                obj.parents('.control-group').addClass('success').removeClass('error').trigger('classChange');
            else if (adjacentStatus === null) //if there is no status boxes
                obj.parents('.control-group').removeClass('success error');
        } else if (isValidEle === null) {
            if (obj.attr('type') != 'radio' && obj.attr('type') != 'checkbox') {
                obj.removeClass('success error');
                if (obj.is('select'))
                    obj.parent('.select-wrapper').removeClass('success error');
            }
            var adjacentStatus = checkAdjcentStatus(obj);
            if (adjacentStatus === false)
                obj.parents('.control-group').addClass('error').removeClass('success');
            else
                obj.parents('.control-group').removeClass('success error');
        }
    };

    //validate the text box based on the dbsvaliate attribute
    var validateTextBox = function(validationObj, obj) {
        var val = obj.val();
        if (val == obj.attr('placeholder')) val = '';
        var isValidEle = true;
        var type = obj.attr('data-dbsvalidate');
        if (obj.parent().children('.select-wrapper').length > 0) {
            var selectwrapper = obj.parent().children('.select-wrapper');
            if ((obj.hasClass('faclityOther') || obj.hasClass('collateralOther')) && selectwrapper.children('select').length > 0 && selectwrapper.children('select').val().toLowerCase().indexOf("others") < 0)
                return true;
        }
        //console.log(type);
        //skip validation for non required if empty
        if ((obj.attr('data-nonrequired') != undefined || isNonCompulsory) && val == '') return null;

        if (!validationObj.isNonEmpty(val)) {
            isValidEle = false;
        } else
        if ((type == 'integer' && !validationObj.isInteger(val)) ||
            (type == 'email' && !validationObj.isEmail(val)) ||
            (type == 'alphabet' && !validationObj.isAlphabet(val)) ||
            (type == 'companyname' && !validationObj.isCompanyName(val)) ||
            (type == 'alphanumeric' && !validationObj.isAlphaNumeric(val)) ||
            (type == 'alphanumericspecial' && !validationObj.isAlphaNumericSpecial(val)) ||
            (type == 'price' && !validationObj.isPrice(val)) ||
            (type == 'pricedecimal' && !validationObj.isPrice(val, 2)) ||
            (type == 'interest' && (!validationObj.isNumber(val, 2) || !validationObj.maxValue(val, 100))) ||
            (type == 'postal' && (!validationObj.isInteger(val) || !validationObj.maxLength(val, 6) || !validationObj.minLength(val, 6))) ||
            (type == 'mobile' && (!validationObj.isInteger(val) || !validationObj.maxLength(val, 8) || !validationObj.minLength(val, 8) ||
                !(validationObj.hasCond(val.substring(0, 1) == 8 || val.substring(0, 1) == 9))))

            ||
            (type == 'office' && (!validationObj.isInteger(val) || !validationObj.maxLength(val, 8) || !validationObj.minLength(val, 8) ||
                !validationObj.hasCond(val.substring(0, 1) == 6))) ||
            (type == 'date' && !isValidDate(val)) ||
            (type == 'phone' && /^[+]?[0-9]*$/.test(val) == false)

        ) {

            isValidEle = false;
        } else if (obj.attr('data-maxvalue') != undefined) //check the max value
        {
            // /alert($(obj).unmask());
            if (type == 'price' || type == 'pricedecimal')
                isValidEle = validationObj.maxValue(unmask(obj), obj.attr('data-maxvalue'));
            else
                isValidEle = validationObj.maxValue(val, obj.attr('data-maxvalue'));
        } else if (obj.attr('data-minvalue') != undefined) //check the min value
        {
            if (type == 'price' || type == 'pricedecimal')
                isValidEle = validationObj.minValue(unmask(obj), obj.attr('data-minvalue'));
            else
                isValidEle = validationObj.minValue(val, obj.attr('data-minvalue'));
        } else if (obj.attr('data-requiredlength') != undefined && obj.attr('data-requiredlength').length > 0) //check the min value
        {

            isValidEle = val.length >= obj.attr('data-requiredlength');
        }
        return isValidEle;
    };
    //validate the select box
    var validateSelectBox = function(validationObj, obj) {
        var val = obj.val();
        if (val && (val.toLowerCase() == 'select' || val.toLowerCase() == 'select type')) val = '';

        if ((obj.attr('data-nonrequired') != undefined || isNonCompulsory) && val == '') return null;

        if (!validationObj.isNonEmpty(val))
            return false;

        return true;
    };
    //validating radio box
    var validateRadioBox = function(validationObj, obj) {
        var name = obj.attr('name');
        if (obj.attr('data-nonrequired') != undefined || isNonCompulsory) return null;

        if (!validationObj.hasCond($('input[type="radio"][name="' + name + '"]:checked:visible').length > 0))
            return false;

        return true;
    };
    //validating check box
    var validateCheckBox = function(validationObj, obj) {
        var name = obj.attr('name');
        if (obj.attr('data-nonrequired') != undefined || isNonCompulsory) return null;

        if (!validationObj.hasCond($('input[type="checkbox"][name="' + name + '"]:checked:visible').length > 0))
            return false;

        return true;
    };

    //check the adjacent element validation status
    var checkAdjcentStatus = function(obj) {
        //TODO: by now skip this for radio and checkboxes
        if (obj.attr('type') == 'radio' || obj.attr('type') == 'checkbox') return true;
        if (obj.parents('.control-group:visible').find('input[data-dbsvalidate]:not([type="radio"]):not([type="file"]):not([type="checkbox"]),select[data-dbsvalidate]').not(obj).length == 0)
            return true; //check whether there is no other elements
        else if (obj.parents('.control-group:visible').find('input[data-dbsvalidate]:not([type="radio"]):not([type="file"]):not([type="checkbox"]).error,select[data-dbsvalidate].error').not(obj).length > 0)
            return false; //check whether there is any other error elements
        else if (obj.parents('.control-group:visible').find('input[data-dbsvalidate]:not([type="radio"]):not([type="file"]):not([type="checkbox"]).success,select[data-dbsvalidate].success').not(obj).length > 0)
            return true; //check whether there is any other success elements
        else
            return null;
    };
    //unmask the price field value
    var unmask = function(obj) {
        return obj.val().replace(/,/g, '');
    };

    var isValidDate = function(dateString) {
        // First check for the pattern
        if (!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(dateString))
            return false;

        // Parse the date parts to integers
        var parts = dateString.split("/");
        var day = parseInt(parts[0], 10);
        var month = parseInt(parts[1], 10);
        var year = parseInt(parts[2], 10);

        // Check the ranges of month and year
        if (year < 1000 || year > 3000 || month == 0 || month > 12)
            return false;

        var monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

        // Adjust for leap years
        if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
            monthLength[1] = 29;

        // Check the range of the day
        return day > 0 && day <= monthLength[month - 1];
    };
});