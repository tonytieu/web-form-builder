
var CustomBrowserDetect = { 

 init: function () {
      if(navigator.userAgent.match(/iPad|iPhone|Android|BlackBerry|webOS|Mobile/i)){
          this.loadjscssfile("iwov-resources/styles/new/customdevices.css", "css", true);
      } else {
          this.loadjscssfile("iwov-resources/styles/new/customdesktop.css", "css", false);
      }
  },
 loadjscssfile: function (filename, filetype, ifMetaTag) {
    var metaTagHand = document.createElement("meta");
   metaTagHand.setAttribute("name", "HandheldFriendly");   
   metaTagHand.setAttribute("content", "True");      
 
   var metaTag = document.createElement("meta");
   metaTag.setAttribute("name", "viewport");   
   metaTag.setAttribute("content", "width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"); 
    
   var fileref=document.createElement("link");
   fileref.setAttribute("rel", "stylesheet");
   fileref.setAttribute("type", "text/css");
   fileref.setAttribute("href", filename);
   //fileref.setAttribute("media", "screen");
	 
     if (typeof fileref!="undefined"){
     if (ifMetaTag){
    document.getElementsByTagName("head")[0].appendChild(metaTagHand);
    document.getElementsByTagName("head")[0].appendChild(metaTag);
  }    
    document.getElementsByTagName("head")[0].appendChild(fileref);
    } 
 } 
};

CustomBrowserDetect.init();
 
