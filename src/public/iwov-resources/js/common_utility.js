

function GetURLParameter(name,href)
{
name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
var regexS = "[\\?&]"+name+"=([^&# ]*)";
var regex = new RegExp(regexS );
var results = regex.exec(href);
if( results == null )
  return "";
else
 return results[1];
}


//function newsdetails(loadUrl){
function newsdetails(){
	var href = $(location).attr('href');
	var checkstr = "FromMonth";
	if(href.indexOf(checkstr) != -1){
	var fromMonth = GetURLParameter('FromMonth',href);
	var fromYear = GetURLParameter('FromYear',href);
	var toMonth = GetURLParameter('ToMonth',href);
	var toYear = GetURLParameter('ToYear',href);
	var country = GetURLParameter('Country',href);
	 $("#countrySelect").val(country);
	 $("#monthFromSelect").val(fromMonth);
	 $("#yearFromSelect").val(fromYear);
	 $("#monthToSelect").val(toMonth);
	 $("#yearToSelect").val(toYear);
	 var backhref = $("#back").attr('href');
	var splitVal = href.split("?");
	backhref=backhref+ "?"+splitVal[1];
	$("#back").attr("href",decodeURIComponent(backhref));
	 

	}
	var currCountry = $("#countrySelect option:selected").val();
	var newsfeedhref = $("#newsfeed").attr('href');
	var newsfeedhrefSplit = newsfeedhref.split("?");
	var newsfeedhrefFinal = newsfeedhrefSplit[0];
	var param="Country="+currCountry;
	$("#newsfeed").attr("href",decodeURIComponent(newsfeedhrefFinal+"?"+param));

}
	

function productInfo(category,loadUrl){
	var highlightCat=null; 
	var description ="";
	if(category===""){
		$(".main-navigation-container ul li").each(function() {		  
		    var css = $(this).attr("class");
		    if(css === 'active'){
		    highlightCat = $(this).children("a").text();
			description =  $(this).find('.scope-note').text();
			highlightCat = highlightCat.replace(description,'');	
		    }
		    //highlightCat= 'Bank';
		});
		ajaxCallInfo(highlightCat,loadUrl);
	}		 
}
function ajaxCallInfo(highlightCat,loadUrl){
	var ajax_load;
	ajax_load="<img src='/iwov-resources/images/ajax-loader.gif' alt='loading'/>";
	$('#productinfo').html(ajax_load);
	$.ajax({
		type: 'POST',
		url: loadUrl,
		cache:false,
		async:true,
		dataType : 'html',
		data: {Category:highlightCat},
		success: function(data) {
			var prdComp = $(data).find('#productinfo').html();
			$('#productinfo').html(prdComp);
			pageLevelComponents.tabs();
		},
		error: function(XMLHttpRequest,textStatus,errorThrown) {
			console.log("error while loading the page "+loadUrl);
		},
		complete: function(XMLHttpRequest,status) {
		}
	}); 
}
//added for product list component -starts
function productList(){
	var currCategory=$('#category_type').val();
	var currSearch =  $('#search_type').val();
	var promotype=$('#promotion_type').val();
	var promoSearch =  $('#search_promo').val();
	var promoCat =  $('#promo_cat').val();
	var type =  $('#type').val();
	var listtype =  $('#listType').val();
	if(type == "promotion"){
		ajaxCallPromotion(promotype,0,1,"",promoSearch,promoCat);
	}else if(type == "product"){	
		ajaxCall(currCategory,0,1,"",currSearch);		
	}else{
		if(listtype == "product"){			
			ajaxCall("",0,1,"",currSearch);
		}else{
			ajaxCallPromotion(listtype,0,1,"",currSearch,"");
		}
	}
	
	$('#productlist').hide();
	$('#pagination').hide();
}

function ajaxCallPromotion(promotype,start,pageNum,clsVal,search,promoCat){
	var ajax_load;
	ajax_load="<img src='/iwov-resources/images/ajax-loader.gif' alt='loading'/>";
	$('#productlist').html(ajax_load);
	var requrl=window.location.href;
	var loadUrl;
	if (requrl.indexOf("/in/") !=-1) {
		loadUrl = "/in";
	}else if(requrl.indexOf("/id/") !=-1){
		loadUrl = "/id";
	}else{
		loadUrl="";
	}
	var prodlistChild =   $('#product_main_page').val();
	loadUrl = loadUrl + prodlistChild;
	var type="promotion";
	$.ajax({
		type: 'POST',
		url: loadUrl,
		cache:false,
		async:true,
		dataType : 'html',
		data: { promotype:promotype,start:start,pageNum:pageNum,clsVal:clsVal,search:search,type:type,pct:promoCat},
		success: function(data) {
			$('#productlist').show();
			$('#pagination').show();
			var prodComp = $(data).find('#product-main').html();
			$('#product-main').html(prodComp);
			var searchHeadline = $(data).find('#productlistHeadline').html();
			//alert("searchHeadline:"+searchHeadline);
			$('#productlistHeadline').html(searchHeadline);
			var paginComp = $(data).find('#pagination').html();
			$('#pagination').html(paginComp);	
			var currPageComp = $(data).find('#hiddendivpage').html();
			$('#hiddendivpage').html(currPageComp);
			
		},
		error: function(XMLHttpRequest,textStatus,errorThrown) {
			console.log("error while loading the page "+loadUrl);
		},
		complete: function(XMLHttpRequest, status) {
		}
	});
}
function ajaxCall(category,start,pageNum,clsVal,search){
	var ajax_load;
	ajax_load="<img src='/iwov-resources/images/ajax-loader.gif' alt='loading'/>";
	$('#productlist').html(ajax_load);
	var requrl=window.location.href;
	var loadUrl;
	if (requrl.indexOf("/in/") !=-1) {
		loadUrl = "/in";
	}else if(requrl.indexOf("/id/") !=-1){
		loadUrl = "/id";
	}else{
		loadUrl="";
	}
	var type="product";
	var prodlistChild =   $('#product_main_page').val();
	loadUrl = loadUrl + prodlistChild;
	$.ajax({
		type: 'POST',
		url: loadUrl,
		cache:false,
		async:true,
		dataType : 'html',
		data: { category:category,start:start,pageNum:pageNum,clsVal:clsVal,search:search,type:type},
		success: function(data) {
			$('#productlist').show();
			$('#pagination').show();
			var prodComp = $(data).find('#product-main').html();
			$('#product-main').html(prodComp);
			var searchHeadline = $(data).find('#productlistHeadline').html();
			$('#productlistHeadline').html(searchHeadline);
			var paginComp = $(data).find('#pagination').html();
			$('#pagination').html(paginComp);	
			var currPageComp = $(data).find('#hiddendivpage').html();
			$('#hiddendivpage').html(currPageComp);
			
		},
		error: function(XMLHttpRequest,textStatus,errorThrown) {
			console.log("error while loading the page "+loadUrl);
		},
		complete: function(XMLHttpRequest, status) {
		}
	});
}
	

	

	function checkClass(){
		var liclsValue = $(this).parent().attr("class");
		if(liclsValue !='disabled'){
			var page = $(this).text();
			var clsValue = $(this).attr("class");
			onPageChange(page,clsValue);
		}
	
	}
	function onPageChange(page,clsValue){
		var listtype =  $('#listType').val();
		var type = $("#type").val();
		var promotype=$('#promotion_type').val();
		var promoCat=$('#promo_cat').val();
		
		var pageNum=1;
		if(page == ""){
			if(clsValue =='prev'){
				pageNum = $("input:hidden[rel = page]").val();

			}
			if(clsValue =='next'){
				pageNum = $("input:hidden[rel = page]").val();		
			}
		}
		var searchPage = $("input:text[id='Search']").val();
		var placeholder =  $("#placeholder").val();
		if((searchPage!="") && (placeholder!=searchPage)){
			if(type == 'promotion'){
				ajaxCallPromotion("",page,pageNum,clsValue,searchPage,"");
				
			}else if(type == 'product'){
				ajaxCall("",page,pageNum,clsValue,searchPage);
			}else{
				if(listtype == "product"){
					ajaxCall("",page,pageNum,clsValue,searchPage);
				}else{
					ajaxCallPromotion(listtype,page,pageNum,clsValue,searchPage,"");
				}
			}
			
		}else{
			currCategory=$('#category_type').val();
			currSearch =  $('#search_type').val();
			var promoSearchCurr = $('#search_promo').val();
			if(type == 'promotion'){
				ajaxCallPromotion(promotype,page,pageNum,clsValue,promoSearchCurr,promoCat);
			}else if(type == 'product'){
				ajaxCall(currCategory,page,pageNum,clsValue,currSearch);
				
			}else{
				if(listtype == "product"){
					ajaxCall("",page,pageNum,clsValue,currSearch);
				}else{
					ajaxCallPromotion(listtype,page,pageNum,clsValue,currSearch,"");
				}
			}
			
		}
	}
	
	function searchTextChange(){
		var listtype =  $('#listType').val();
		var promotype=$('#promotion_type').val();
		var search = $("input:text[id='Search']").val();
		var type = $("#type").val();
		var placeholder =  $("#placeholder").val();
		if((search!="") && (placeholder!=search)){
			if(type=='promotion'){
				ajaxCallPromotion("",0,1,"",search,"");	
				
			}else if(type == 'product'){
				ajaxCall("",0,1,"",search);	
			}else{
				if(listtype == "product"){
					ajaxCall("",0,1,"",search);
				}else{
					ajaxCallPromotion(listtype,0,1,"",search,"");
				}
			}
		
	}
	if(search==""){
		var currSearch =  $('#search_type').val();
		if(listtype == "product"){			
			ajaxCall("",0,1,"",currSearch);
		}else{
			ajaxCallPromotion(listtype,0,1,"",currSearch,"");
		}


	}else{
	if(placeholder==search){
		var currSearch =  $('#search_type').val();
		if(listtype == "product"){			
			ajaxCall("",0,1,"",currSearch);
		}else{
			ajaxCallPromotion(promotype,0,1,"",currSearch,"");
		}
	}
	}
			
		
	}
	 //added for product list component -ends
	
 //added for cards hero component -starts

	
	 //added for cards hero component -ends
	//added for find latest offers
	
function searchOffers(searchPreference,globalsearchpage,promotionlistPage){
	
	var ptype = $("#ptype").val();
	var searchTxt = $("input:text[id='SearchTxt']").val();
	if(searchPreference == 'Global'){
		window.location.href = globalsearchpage+"?Query_String="+searchTxt;
	}else if(searchPreference == 'Contextual'){
		if(searchTxt!=""){
			ajaxCallSearchOffers(searchTxt,0,1,"");
		}
		
	}else{
		if(searchTxt!=""){
			searchTxt = encodeURI(searchTxt);
			window.location.href = promotionlistPage+"?pt="+ptype+"&search="+searchTxt;
		}
		
	}
}
function ajaxCallSearchOffers(searchTxt,start,pageNum,clsVal){
    var moretext =   $('#moretext').val();
	var queryparam =   $('#queryparam').val();
	var offerchild = $("#offerchild").val();
	var ptype = $("#ptype").val();
	var requrl=window.location.href;
	var loadUrl;
	if (requrl.indexOf("/in/") !=-1) {
		loadUrl = "/in";
	}else if(requrl.indexOf("/id/") !=-1){
		loadUrl = "/id";
	}else{
		loadUrl="";
	}
	loadUrl = loadUrl + offerchild;
	//alert(loadUrl);
	$.ajax({
		type: 'POST',
		url: loadUrl,
		cache:false,
		async:true,
		dataType : 'html',
		data: { search:searchTxt,SearchOffers:true,SearchProducts:false,start:start,pageNum:pageNum,clsVal:clsVal,PromotionType:ptype,moretext:moretext,qparam:queryparam},
		success: function(data) {
			$('#paginationOffer').show();
			$('#offersSection').show();			
			var paginComp = $(data).find('#pagination').html();
			$('#paginationOffer').html(paginComp);	
			var currPageComp = $(data).find('#hiddendivpage').html();
			$('#hiddendivpage').html(currPageComp);
			var offerComp = $(data).find('#latestOffers').html();
			$('#latestOffers').html(offerComp);	
			
			var offertype = $(data).find('#offertypediv').html();
			$('#offertypediv').html(offertype);	
			
		},
		error: function(XMLHttpRequest,textStatus,errorThrown) {
			console.log("error while loading the page "+loadUrl);
		},
		complete: function(XMLHttpRequest, status) {
		}
	});
}
function loadCardsDropdown(benefitThree,idString){
	var benefitOne  = $("#subnav").contents('.active').children("a").attr("data-target");



	var currBenTwo = "#benefittwo_"+idString;
	var benefitTwo = $(currBenTwo).val();
	var cardsid = "#benefitcard_"+idString;
	//$(cardsid).empty();
	$(cardsid).find('option:gt(0)').remove();
	ajaxCallSearchCards(benefitOne,benefitTwo,benefitThree,cardsid);
		
}
function loadCardsDropdownMobile(benefitThree,idString){
	var benefitOne  = $("#FindOffersDrp").val();
	var currBenTwo = "#mobbenefittwo_"+idString;
	var benefitTwo = $(currBenTwo).val();
	var cardsid = "#mobbenefitcard_"+idString;
	//$(cardsid).empty();
	$(cardsid).find('option:gt(0)').remove();
	ajaxCallSearchCardsMobile(benefitOne,benefitTwo,benefitThree,cardsid);
		
}
function findOffers(){
	var benefitOne  = $("#subnav").contents('.active').children("a").attr("data-target");
	var pos  = $("#subnav").contents('.active').children("a").attr("rel");




	var currBenTwo = "#benefittwo_"+pos;
	var benefitTwo = $(currBenTwo).val();
	var currBenThree = "#benefitthree_"+pos;
	var benefitThree = $(currBenThree).val();
	var currBenCard = "#benefitcard_"+pos;
	var benefitCard = $(currBenCard).val();
	
   ajaxCallFindOffer(benefitOne,benefitTwo,benefitThree,benefitCard,0,1,"");
	
	
}
function findOffersMobile(){
	var benefitOne  = $("#FindOffersDrp").val();
	var pos  =  $( "#FindOffersDrp option:selected" ).attr("rel");
	var currBenTwo = "#mobbenefittwo_"+pos;
	var benefitTwo = $(currBenTwo).val();
	var currBenThree = "#mobbenefitthree_"+pos;
	var benefitThree = $(currBenThree).val();
	var currBenCard = "#mobbenefitcard_"+pos;
	var benefitCard = $(currBenCard).val();	
   ajaxCallFindOffer(benefitOne,benefitTwo,benefitThree,benefitCard,0,1,"");
	
	
}

function ajaxCallFindOffer(benefitOne,benefitTwo,benefitThree,benefitCard,start,pageNum,clsVal){
    var moretext =   $('#moretext').val();
	var queryparam =   $('#queryparam').val();
	var offerchild = $("#offerchild").val();
	var ptype = $("#ptype").val();
	var requrl=window.location.href;
	var loadUrl;
	if (requrl.indexOf("/in/") !=-1) {
		loadUrl = "/in";
	}else if(requrl.indexOf("/id/") !=-1){
		loadUrl = "/id";
	}else{
		loadUrl="";
	}
	loadUrl = loadUrl + offerchild;
	//alert(loadUrl);
	$.ajax({
		type: 'POST',
		url: loadUrl,
		cache:false,
		async:true,
		dataType : 'html',
		data: { b1:benefitOne,b2:benefitTwo,b3:benefitThree,prodMap:benefitCard,SearchOffers:true,SearchProducts:false,start:start,pageNum:pageNum,clsVal:clsVal,PromotionType:ptype,moretext:moretext,qparam:queryparam},
		success: function(data) {
			$('#paginationOffer').show();
			$('#offersSection').show();			
			var paginComp = $(data).find('#pagination').html();
			$('#paginationOffer').html(paginComp);	
			var currPageComp = $(data).find('#hiddendivpage').html();
			$('#hiddendivpage').html(currPageComp);
			var offerComp = $(data).find('#latestOffers').html();
			$('#latestOffers').html(offerComp);	
			var offertype = $(data).find('#offertypediv').html();
			$('#offertypediv').html(offertype);
			
		},
		error: function(XMLHttpRequest,textStatus,errorThrown) {
			console.log("error while loading the page "+loadUrl);
		},
		complete: function(XMLHttpRequest, status) {
		}
	});
}
function ajaxCallSearchCards(benefitOne,benefitTwo,benefitThree,cardsid){
	var ptype = $("#ptype").val();
	var offerchild = $("#offerchild").val();
	var requrl=window.location.href;
	var loadUrl;
	if (requrl.indexOf("/in/") !=-1) {
		loadUrl = "/in";
	}else if(requrl.indexOf("/id/") !=-1){
		loadUrl = "/id";
	}else{
		loadUrl="";
	}
	loadUrl = loadUrl + offerchild;
	$.ajax({
		type: 'POST',
		url: loadUrl,
		cache:false,
		async:true,
		dataType : 'html',
		data: {SearchProducts:true,SearchOffers:false,b1:benefitOne,b2:benefitTwo,b3:benefitThree,PromotionType:ptype},
		success: function(data) {
			var prodComp = $(data).find('#benefitcard').html();
			$(cardsid).append(prodComp);
			$(cardsid).val($(cardsid).find('option').first().val());
		},
		error: function(XMLHttpRequest,textStatus,errorThrown) {
			console.log("error while loading the page "+loadUrl);
		},
		complete: function(XMLHttpRequest, status) {
		}
	});
}
function ajaxCallSearchCardsMobile(benefitOne,benefitTwo,benefitThree,cardsid){
	var ptype = $("#ptype").val();
	var offerchild = $("#offerchild").val();
	var requrl=window.location.href;
	var loadUrl;
	if (requrl.indexOf("/in/") !=-1) {
		loadUrl = "/in";
	}else if(requrl.indexOf("/id/") !=-1){
		loadUrl = "/id";
	}else{
		loadUrl="";
	}
	loadUrl = loadUrl + offerchild;
	$.ajax({
		type: 'POST',
		url: loadUrl,
		cache:false,
		async:true,
		dataType : 'html',
		data: {SearchProducts:true,SearchOffers:false,b1:benefitOne,b2:benefitTwo,b3:benefitThree,PromotionType:ptype},
		success: function(data) {
			var prodComp = $(data).find('#benefitcard').html();
			$(cardsid).append(prodComp);
			$(cardsid).val($(cardsid).find('option').first().val());
		},
		error: function(XMLHttpRequest,textStatus,errorThrown) {
			console.log("error while loading the page "+loadUrl);
		},
		complete: function(XMLHttpRequest, status) {
		}
	});
}
function checkClassOffer(){
	var liclsValue = $(this).parent().attr("class");
	if(liclsValue !='disabled'){
		var page = $(this).text();
		var clsValue = $(this).attr("class");
		if(navigator.userAgent.match(/iPhone|Android|BlackBerry|Mobile/i)){
			onPageChangeOfferMobile(page,clsValue);
		}else{
			onPageChangeOffer(page,clsValue);
		}
		
	}

}
function onPageChangeOffer(page,clsValue){
	var pageNum=1;
	if(page == ""){
		if(clsValue =='prev'){
			pageNum = $("input:hidden[rel = page]").val();

		}
		if(clsValue =='next'){
			pageNum = $("input:hidden[rel = page]").val();		
		}
	}
	var benefitOne  = $("#subnav").contents('.active').children("a").attr("data-target");
	var pos  = $("#subnav").contents('.active').children("a").attr("rel");




	var currBenTwo = "#benefittwo_"+pos;
	var benefitTwo = $(currBenTwo).val();
	var currBenThree = "#benefitthree_"+pos;
	var benefitThree = $(currBenThree).val();
	var currBenCard = "#benefitcard_"+pos;
	var benefitCard = $(currBenCard).val();
	
	var searchPage = $("input:text[id='SearchTxt']").val();
	var type =  $("#offertype").val();
	if(type =='search'){
		ajaxCallSearchOffers(searchPage,page,pageNum,clsValue);
	}
	
	else{

		   ajaxCallFindOffer(benefitOne,benefitTwo,benefitThree,benefitCard,page,pageNum,clsValue);
	}
}
function onPageChangeOfferMobile(page,clsValue){
	var pageNum=1;
	if(page == ""){
		if(clsValue =='prev'){
			pageNum = $("input:hidden[rel = page]").val();

		}
		if(clsValue =='next'){
			pageNum = $("input:hidden[rel = page]").val();		
		}
	}
	var benefitOne  = $("#FindOffersDrp").val();
	var pos  =  $( "#FindOffersDrp option:selected" ).attr("rel");
	var currBenTwo = "#mobbenefittwo_"+pos;
	var benefitTwo = $(currBenTwo).val();
	var currBenThree = "#mobbenefitthree_"+pos;
	var benefitThree = $(currBenThree).val();
	var currBenCard = "#mobbenefitcard_"+pos;
	var benefitCard = $(currBenCard).val();
	
	var searchPage = $("input:text[id='SearchTxt']").val();
	var type =  $("#offertype").val();
	if(type =='search'){
		ajaxCallSearchOffers(searchPage,page,pageNum,clsValue);
	}else{
		   ajaxCallFindOffer(benefitOne,benefitTwo,benefitThree,benefitCard,page,pageNum,clsValue);
	}
}
function findLatestOffers(){
	var benefitOne  = $("#subnav").contents('.active').children("a").attr("data-target");
	var idStr  = $("#subnav").contents('.active').children("a").attr("rel");





	var cardsid = "#benefitcard_"+idStr;
	var currBenTwo = "#benefittwo_"+idStr;

	var benefitTwo = $(currBenTwo).val();

	//$(cardsid).empty();
	$(cardsid).find('option:gt(0)').remove();
	if(benefitOne != ""){
		if(typeof benefitOne === 'undefined'){
			
		}else{
			ajaxCallSearchCards(benefitOne,benefitTwo,"",cardsid);
				
		}
	}
		
	}
function findLatestOffersMobile(){
	var benefitOne  =$("#FindOffersDrp").val();
	var idStr  = $( "#FindOffersDrp option:selected" ).attr("rel");
	var cardsid = "#mobbenefitcard_"+idStr;
	var currBenTwo = "#mobbenefittwo_"+idStr;
	var benefitTwo = $(currBenTwo).val();
	//$(cardsid).empty();
	$(cardsid).find('option:gt(0)').remove();
	if(benefitOne != ""){
		if(typeof benefitOne === 'undefined'){
			
		}else{
			ajaxCallSearchCardsMobile(benefitOne,benefitTwo,"",cardsid);
				
		}
	}
		
	}
  function findLatestMoreBen(moreBen,idStr){
		

	  if(typeof moreBen === 'undefined'){
			
		}else{
			var currBenThree = "#benefitthree_"+idStr;
			var cardsid = "#benefitcard_"+idStr;
			var benefitTwo =  "#benefittwo_"+idStr;
			$(cardsid).find('option:first').attr('selected', 'selected');
			$(benefitTwo).find('option:first').attr('selected', 'selected');

			$(currBenThree).find('option:first').attr('selected', 'selected');
			ajaxCallSearchCards(moreBen,"","",cardsid);
			
				
		}
  }
  function subnavchange(){
	  	var idStr  = $("#subnav").contents('.active').children("a").attr("rel");



		var currBenThree = "#benefitthree_"+idStr;
		var cardsid = "#benefitcard_"+idStr;
		var benefitTwo =  "#benefittwo_"+idStr;
		$(cardsid).find('option:first').attr('selected', 'selected');
		$(benefitTwo).find('option:first').attr('selected', 'selected');

		$(currBenThree).find('option:first').attr('selected', 'selected');

		findLatestOffers();
  }
  function subnavchangeMobile(){
	   	var idStr  = $( "#FindOffersDrp option:selected" ).attr("rel");
	   	var currBenThree = "#mobbenefitthree_"+idStr;
		var cardsid = "#mobbenefitcard_"+idStr;
		var benefitTwo =  "#mobbenefittwo_"+idStr;
		$(cardsid).find('option:first').attr('selected', 'selected');
		$(benefitTwo).find('option:first').attr('selected', 'selected');

		$(currBenThree).find('option:first').attr('selected', 'selected');

		findLatestOffersMobile();
}	
	

