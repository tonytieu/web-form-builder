    $(document).ready(function(){
        //FIX: change all the customDropDown to customSelect
        $('.customDropdown').each(function(){
            $(this).removeClass('customDropdown').addClass('customSelect');
        });

        $(".customSelect").each(function(){
			var _wrapperWidth;
            _wrapperWidth = $(this).css('width');
            // if(_wrapperWidth == undefined)
            //     _wrapperWidth = $(this).css('min-width');
            //TODO -  Jayaraj: Have to check with team.
			// if($(this).hasClass('customSelectSmall')) _wrapperWidth=102;
			// else if($(this).hasClass('customSelectMedium')) _wrapperWidth=220;
			// else if($(this).hasClass('customSelectLarge')) _wrapperWidth=320;			
			
            $(this).wrap("<span class='select-wrapper' style='width:"+ _wrapperWidth +"'></span>");
            $(this).after("<span class='holder'></span>");
        });
        $("body").delegate(".customSelect","change",function(){
            var selectedOption = $(this).find(":selected").text();
            $(this).next(".holder").text(selectedOption);
        });
        $('.customSelect').trigger("change");
    });